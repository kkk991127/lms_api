package org.bj.academylmsapi.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.enums.MemberGroup;
import org.bj.academylmsapi.model.LoginRequest;
import org.bj.academylmsapi.model.LoginResponse;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.service.LoginService;
import org.bj.academylmsapi.service.ResponseService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/login")
@RequiredArgsConstructor
public class LoginController {
    private final LoginService loginService;

    @PostMapping("/web/admin")
    //@Valid - 유효성 검증
    public SingleResult<LoginResponse> doLoginAdmin(@RequestBody @Valid LoginRequest loginRequest){
        return ResponseService.getSingleResult(loginService.doLogin(MemberGroup.ROLE_MEMBER, loginRequest, "WEB"));
    }

    @PostMapping("/app/admin")
    public SingleResult<LoginResponse> doLoginUser(@RequestBody @Valid LoginRequest loginRequest){
        return ResponseService.getSingleResult(loginService.doLogin(MemberGroup.ROLE_NO_MEMBER,loginRequest,"APP"));
    }


}
