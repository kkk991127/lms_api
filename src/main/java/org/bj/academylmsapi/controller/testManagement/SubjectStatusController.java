package org.bj.academylmsapi.controller.testManagement;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.testManagement.SubjectStatus;
import org.bj.academylmsapi.model.Test.SubjectStatus.SubjectStatusChangeRequest;
import org.bj.academylmsapi.model.Test.SubjectStatus.SubjectStatusItem;
import org.bj.academylmsapi.model.Test.SubjectStatus.SubjectStatusRequest;
import org.bj.academylmsapi.model.Test.SubjectStatus.SubjectStatusResponse;
import org.bj.academylmsapi.model.result.CommonResult;
import org.bj.academylmsapi.model.result.ListResult;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.service.ListConvertService;
import org.bj.academylmsapi.service.ResponseService;
import org.bj.academylmsapi.service.testManagement.SubjectStatusService;
import org.springframework.web.bind.annotation.*;

// 평가관리 - 과목현황 controller

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/subjectStatus")
@Tag(name = "SubjectStatus", description = "[평가관리] 과목현황")
public class SubjectStatusController {
    private final SubjectStatusService subjectStatusService;

    // 과목현황 C
    @PostMapping("/new")
    @Operation(summary = "시험 정보 등록")
    public CommonResult setSubjectStatus(@RequestBody SubjectStatusRequest request){
        subjectStatusService.setSubjectStatus(request);

        return ResponseService.getSuccessResult();
    }

    // 과목현황 복수 R
    @GetMapping("/all/pageNum/{pageNum}")
    @Operation(summary = "시험 정보 리스트 (페이징)")
    public ListResult<SubjectStatusItem> getSubjectStatusItem(@PathVariable int pageNum){
        return ListConvertService.settingListResult(subjectStatusService.getSubjectStatusItem(pageNum));
    }

    // 과목현황 단수 R
    @GetMapping("/detail/subjectStatusId/{subjectStatusId}")
    @Operation(summary = "시험 정보 상세 보기")
    public SingleResult<SubjectStatusResponse> getSubjectStatusResponse(@PathVariable long subjectStatusId){
        return ResponseService.getSingleResult(subjectStatusService.getSubjectStatusResponse(subjectStatusId));
    }

    // 과목현황 U
    @PutMapping("/changeInfo/subjectStatusId/{subjectStatusId}")
    @Operation(summary = "시험 정보 수정")
    public CommonResult putSubjectStatus(@PathVariable long subjectStatusId, @RequestBody SubjectStatusChangeRequest request){
         subjectStatusService.putSubjectStatus(subjectStatusId, request);
         return ResponseService.getSuccessResult();
    }

    // 과목현황 D
    @DeleteMapping("/delete/subjectStatusId/{subjectStatusId}")
    @Operation(summary = "시험 정보 삭제")
    public CommonResult delSubjectStatus(@PathVariable long subjectStatusId){
        subjectStatusService.delSubjectStatus(subjectStatusId);
        return ResponseService.getSuccessResult();
    }
}
