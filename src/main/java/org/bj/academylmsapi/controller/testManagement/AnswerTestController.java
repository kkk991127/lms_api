package org.bj.academylmsapi.controller.testManagement;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.model.Test.Answer.AnswerPutScoreRequest;
import org.bj.academylmsapi.model.Test.Answer.AnswerPutStudentSign;
import org.bj.academylmsapi.model.Test.Answer.AnswerTestRequest;
import org.bj.academylmsapi.model.Test.Answer.AnswerTestResponse;
import org.bj.academylmsapi.model.result.CommonResult;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.service.ResponseService;
import org.bj.academylmsapi.service.testManagement.AnswerTestService;
import org.springframework.web.bind.annotation.*;

// 평가관리 - 시험채점 controller

@RestController
@RequiredArgsConstructor
@Tag(name = "AnswerTest" , description = "[평가관리] 시험 답변")
@RequestMapping("/v1/answer-test")
public class AnswerTestController {
    private final AnswerTestService answerTestService;

    @PostMapping("/new")
    @Operation(summary = "시험등록")
    public CommonResult setAnswer(@RequestBody AnswerTestRequest request){
        answerTestService.setAnswer(request);
        return ResponseService.getSuccessResult();
    }

    @GetMapping("/detail/answerTestId/{answerTestId}")
    @Operation(summary = "답변 보기")
    public SingleResult<AnswerTestResponse> getAnswer(@PathVariable long answerTestId){
        return answerTestService.getAnswer(answerTestId);
    }

    @PutMapping("/putScore/answerTestId/{answerTestId}")
    @Operation(summary = "채점")
    public CommonResult putAnswerScore(@PathVariable long answerTestId, @RequestBody AnswerPutScoreRequest request){
        return answerTestService.putAnswerScore(request, answerTestId);
    }

    @PutMapping("/testSign/answerTestId/{answerTestId}")
    @Operation(summary = "확인 서명")
    public CommonResult putTestCheckSign(@PathVariable long answerTestId, @RequestBody AnswerPutStudentSign request){
        return answerTestService.putTestCheckSign(request, answerTestId);
    }

    @DeleteMapping("delete/answerTestId/{answerTestId}")
    @Operation(summary = "답변 삭제")
    public CommonResult delAnswer(@PathVariable long answerTestId){
        return answerTestService.delAnswer(answerTestId);
    }
}
