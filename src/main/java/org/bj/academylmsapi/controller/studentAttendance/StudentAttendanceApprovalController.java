package org.bj.academylmsapi.controller.studentAttendance;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.model.result.CommonResult;
import org.bj.academylmsapi.model.result.ListResult;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.model.studentAttendance.studentAttendanceApproval.StudentAttendanceApprovalItem;
import org.bj.academylmsapi.model.studentAttendance.studentAttendanceApproval.StudentAttendanceApprovalRequest;
import org.bj.academylmsapi.model.studentAttendance.studentAttendanceApproval.StudentAttendanceApprovalResponse;
import org.bj.academylmsapi.service.ResponseService;
import org.bj.academylmsapi.service.studentAttendance.StudentAttendanceApprovalService;
import org.springframework.web.bind.annotation.*;

// 수강생 출결승인 controller

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/studentAttendanceApproval")
@Tag(name = "StudentAttendanceApproval", description = "수강생 출결 승인")
public class StudentAttendanceApprovalController {
    private final StudentAttendanceApprovalService studentAttendanceApprovalService;

    // 수강생 출결승인 C
    @PostMapping("/new")
    @Operation(summary = "수강생 출결승인 등록")
    public CommonResult setStudentAttendanceApproval(@RequestBody StudentAttendanceApprovalRequest request) {
        studentAttendanceApprovalService.setStudentAttendanceApproval(request);
        return ResponseService.getSuccessResult();
    }

    // 수강생 출결승인 단수 R
    @GetMapping("/detail/{studentAttendanceApprovalId}")
    @Operation(summary = "수강생 출결승인 상세보기")
    public SingleResult<StudentAttendanceApprovalResponse> getStudentAttendanceApproval(@PathVariable long studentAttendanceApprovalId) {
        return ResponseService.getSingleResult(studentAttendanceApprovalService.getStudentAttendanceApproval(studentAttendanceApprovalId));
    }

    // 수강생 출결승인 U
    @PutMapping("/changeInfo/studentAttendanceApprovalId/{studentAttendanceApprovalId}")
    @Operation(summary = "수강생 출결승인 정보 수정")
    public CommonResult putStudentAttendanceApproval(@PathVariable long studentAttendanceApprovalId, @RequestBody StudentAttendanceApprovalRequest request) {
        studentAttendanceApprovalService.putStudentAttendanceApproval(studentAttendanceApprovalId, request);
        return ResponseService.getSuccessResult();
    }

    // 수강생 출결승인 D
    @DeleteMapping("/delete/studentAttendanceApprovalId/{studentAttendanceApprovalId}")
    @Operation(summary = "수강생 출결승인 정보 삭제")
    public CommonResult delStudentAttendanceApproval(@PathVariable long studentAttendanceApprovalId) {
        studentAttendanceApprovalService.delStudentAttendanceApproval(studentAttendanceApprovalId);
        return ResponseService.getSuccessResult();
    }
}
