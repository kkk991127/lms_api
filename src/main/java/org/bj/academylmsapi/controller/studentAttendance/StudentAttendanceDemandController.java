package org.bj.academylmsapi.controller.studentAttendance;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.model.result.CommonResult;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.model.studentAttendance.studentAttendanceDemand.StudentAttendanceDemandRequest;
import org.bj.academylmsapi.model.studentAttendance.studentAttendanceDemand.StudentAttendanceDemandResponse;
import org.bj.academylmsapi.service.ResponseService;
import org.bj.academylmsapi.service.studentAttendance.StudentAttendanceDemandService;
import org.springframework.web.bind.annotation.*;

// 수강생 출결 신청 controller

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/studentAttendanceDemand")
@Tag(name = "StudentAttendanceDemend", description = "수강생 출결 신청")
public class StudentAttendanceDemandController {
    private final StudentAttendanceDemandService studentAttendanceDemandService;

    // 수강생 출결 신청 C
    @PostMapping("/new")
    @Operation(summary = "수강생 출결 신청 등록")
    public CommonResult setStudentAttendanceDemand(@RequestBody StudentAttendanceDemandRequest request) {
        studentAttendanceDemandService.setStudentAttendanceDemand(request);
        return ResponseService.getSuccessResult();
    }

    // 수강생 출결 신청 단수 R
    @GetMapping("/detail/studentAttendanceDemandId/{studentAttendanceDemandId}")
    @Operation(summary = "수강생 출결 신청 상세보기")
    public SingleResult<StudentAttendanceDemandResponse> getStudentAttendanceDemand(@PathVariable long studentAttendanceDemandId) {
        return ResponseService.getSingleResult(studentAttendanceDemandService.getStudentAttendanceDemand(studentAttendanceDemandId));
    }

    // 수강생 출결 신청 U
    @PutMapping("/changeInfo/studentAttendanceDemandId/{studentAttendanceDemandId}")
    @Operation(summary = "수강생 출결 신청 정보 수정")
    public CommonResult putStudentAttendanceDemand(@PathVariable long studentAttendanceDemandId, @RequestBody StudentAttendanceDemandRequest request) {
        studentAttendanceDemandService.putStudentAttendanceDemand(studentAttendanceDemandId, request);
        return ResponseService.getSuccessResult();
    }

    // 수강생 출결 신청 D
    @DeleteMapping("/delete/studentAttendanceDemandId/{studentAttendanceDemandId}")
    @Operation(summary = "수강생 출결 신청 정보 삭제")
    public CommonResult delStudentAttendanceDemand(@PathVariable long studentAttendanceDemandId) {
        studentAttendanceDemandService.delStudentAttendanceDemand(studentAttendanceDemandId);
        return ResponseService.getSuccessResult();
    }
}
