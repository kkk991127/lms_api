package org.bj.academylmsapi.controller.studentAttendance;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.model.result.CommonResult;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.model.studentAttendance.studentAttendanceHistory.StudentAttendanceHistoryRequest;
import org.bj.academylmsapi.model.studentAttendance.studentAttendanceHistory.StudentAttendanceHistoryResponse;
import org.bj.academylmsapi.service.ResponseService;
import org.bj.academylmsapi.service.studentAttendance.StudentAttendanceHistoryService;
import org.springframework.web.bind.annotation.*;

// 수강생 출결 내역 controller

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/studentAttendanceHistory")
@Tag(name = "StudentAttendanceHistory", description = "수강생 출결 내역")
public class StudentAttendanceHistoryController {
    private final StudentAttendanceHistoryService studentAttendanceHistoryService;

    // 수강생 출결 내역 C
    @PostMapping("/new")
    @Operation(summary = "수강생 출결 내역 등록")
    public CommonResult setStudentAttendanceHistory(@RequestBody StudentAttendanceHistoryRequest request) {
        studentAttendanceHistoryService.SetStudentAttendanceHistory(request);
        return ResponseService.getSuccessResult();
    }

    // 수강생 출결 내역 단수 R
    @GetMapping("/detail/studentAttendanceHistoryId/{studentAttendanceHistoryId}")
    @Operation(summary = "수강생 출결 내역 상세보기")
    public SingleResult<StudentAttendanceHistoryResponse> getStudentAttendanceHistory(@PathVariable long studentAttendanceHistoryId) {
        return ResponseService.getSingleResult(studentAttendanceHistoryService.getStudentAttendanceHistory(studentAttendanceHistoryId));
    }

    // 수강생 출결 내역 U
    @PutMapping("/changeInfo/studentAttendanceHistoryId/{studentAttendanceHistoryId}")
    @Operation(summary = "수강생 출결 내역 정보 수정")
    public CommonResult putStudentAttendanceHistory(@PathVariable long studentAttendanceHistoryId, @RequestBody StudentAttendanceHistoryRequest request) {
        studentAttendanceHistoryService.putStudentAttendanceHistory(studentAttendanceHistoryId, request);
        return ResponseService.getSuccessResult();
    }

    // 수강생 출결 내역 D
    @DeleteMapping("/delete/studentAttendanceHistoryId/{studentAttendanceHistoryId}")
    @Operation(summary = "수강생 출결 내역 정보 삭제")
    public CommonResult delStudentAttendanceHistory(@PathVariable long studentAttendanceHistoryId) {
        studentAttendanceHistoryService.delStudentAttendanceHistory(studentAttendanceHistoryId);
        return ResponseService.getSuccessResult();
    }
}
