package org.bj.academylmsapi.controller.trainingManagement;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.studentManagement.Career;
import org.bj.academylmsapi.model.result.CommonResult;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.model.trainingManagement.trainingAttendance.TrainingAttendanceRequest;
import org.bj.academylmsapi.model.trainingManagement.trainingAttendance.TrainingAttendanceResponse;
import org.bj.academylmsapi.service.ResponseService;
import org.bj.academylmsapi.service.trainingManagement.TrainingAttendanceService;
import org.bj.academylmsapi.service.trainingManagement.TrainingRegisterService;
import org.springframework.web.bind.annotation.*;

// 훈련관리 - 훈련일지 출결 controller

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/trainingAttendance")
@Tag(name = "TrainingAttendance", description = "[훈련관리] 훈련일지 출결")
public class TrainingAttendanceController {
    private final TrainingAttendanceService trainingAttendanceService;

    // 훈련일지 출결 C
    @PostMapping("/new")
    @Operation(summary = "훈련일지 출결 등록")
    public CommonResult setTrainingAttendance(@RequestBody TrainingAttendanceRequest request) {
        trainingAttendanceService.setTrainingAttendance(request);
        return ResponseService.getSuccessResult();
    }

    // 훈련일지 출결 단수 R
    @GetMapping("/detail/trainingAttendanceId/{trainingAttendanceId}")
    @Operation(summary = "훈련일지 출결 상세보기")
    public SingleResult<TrainingAttendanceResponse> getTrainingAttendance(@PathVariable long trainingAttendanceId) {
        return ResponseService.getSingleResult(trainingAttendanceService.getTrainingAttendance(trainingAttendanceId));
    }

    // 훈련일지 출결 U
    @PutMapping("/changeInfo/trainingAttendanceId/{trainingAttendanceId}")
    @Operation(summary = "훈련일지 출결 정보 수정")
    public CommonResult putTrainingAttendance(@PathVariable long trainingAttendanceId, @RequestBody TrainingAttendanceRequest request) {
        trainingAttendanceService.putTrainingAttendance(trainingAttendanceId, request);
        return ResponseService.getSuccessResult();
    }

    // 훈련일지 출결 D
    @DeleteMapping("/delete/trainingAttendanceId/{trainingAttendanceId}")
    @Operation(summary = "훈련일지 출결 정보 삭제")
    public CommonResult delTrainingAttendance(@PathVariable long trainingAttendanceId) {
        trainingAttendanceService.delTrainingAttendance(trainingAttendanceId);
        return ResponseService.getSuccessResult();
    }
}
