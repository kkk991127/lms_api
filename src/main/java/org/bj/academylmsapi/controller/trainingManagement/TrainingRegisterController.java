package org.bj.academylmsapi.controller.trainingManagement;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.model.result.CommonResult;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.model.trainingManagement.trainingRegister.TrainingRegisterRequest;
import org.bj.academylmsapi.model.trainingManagement.trainingRegister.TrainingRegisterResponse;
import org.bj.academylmsapi.service.ResponseService;
import org.bj.academylmsapi.service.trainingManagement.TrainingRegisterService;
import org.springframework.web.bind.annotation.*;

// 훈련관리 - 훈련일지 등록 controller

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/trainingRegister")
@Tag(name = "TrainingRegister", description = "[훈련관리] 훈련일지 등록")
public class TrainingRegisterController {
    private final TrainingRegisterService trainingRegisterService;

    // 훈련일지 등록 C
    @PostMapping("/new")
    @Operation(summary = "훈련일지 등록")
    public CommonResult setTrainingRegister(@RequestBody TrainingRegisterRequest request) {
        trainingRegisterService.setTrainingRegister(request);
        return ResponseService.getSuccessResult();
    }

    // 훈련일지 등록 단수 R
    @GetMapping("/detail/trainingRegisterId/{trainingRegisterId}")
    @Operation(summary = "훈련일지 등록 상세보기")
    public SingleResult<TrainingRegisterResponse> getTrainingRegister(@PathVariable long trainingRegisterId) {
        return ResponseService.getSingleResult(trainingRegisterService.getTrainingRegister(trainingRegisterId));
    }

    // 훈련일지 등록 U
    @PutMapping("/changeInfo/trainingRegisterId/{trainingRegisterId}")
    @Operation(summary = "훈련일지 등록 정보 수정")
    public CommonResult putTrainingRegister(@PathVariable long trainingRegisterId, @RequestBody TrainingRegisterRequest request) {
        trainingRegisterService.putTrainingRegister(trainingRegisterId,request);
        return ResponseService.getSuccessResult();
    }

    // 훈련일지 등록 D
    @DeleteMapping("/delete/trainingRegisterId/{trainingRegisterId}")
    @Operation(summary = "훈련일지 등록 정보 삭제")
    public CommonResult delTrainingRegister(@PathVariable long trainingRegisterId) {
        trainingRegisterService.delTrainingRegister(trainingRegisterId);
        return ResponseService.getSuccessResult();
    }
}
