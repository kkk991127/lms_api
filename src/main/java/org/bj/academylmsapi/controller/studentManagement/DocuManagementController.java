package org.bj.academylmsapi.controller.studentManagement;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.model.result.CommonResult;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.model.studentManagement.docuManagement.DocuManagamentResponse;
import org.bj.academylmsapi.model.studentManagement.docuManagement.DocuManagementRequest;
import org.bj.academylmsapi.service.ResponseService;
import org.bj.academylmsapi.service.studentManagement.DocuManagementService;
import org.springframework.web.bind.annotation.*;

// 학생관리 - 서류관리 controller

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/docuManagement")
@Tag(name = "DocuManagement", description = "[학생관리] 서류관리")
public class DocuManagementController {
    private final DocuManagementService docuManagementService;

    // 서류관리 C
    @PostMapping("/new")
    @Operation(summary = "서류관리 등록")
    public CommonResult setDocuManagement(@RequestBody DocuManagementRequest request) {
        docuManagementService.setDocuManagement(request);
        return ResponseService.getSuccessResult();
    }

    // 서류관리 단수 R
    @GetMapping("/detail/docuManagementId/{docuManagementId}")
    @Operation(summary = "서류관리 상세보기")
    public SingleResult<DocuManagamentResponse> getDocuManagement(@PathVariable long docuManagementId) {
        return ResponseService.getSingleResult(docuManagementService.getDocuManagement(docuManagementId));
    }

    // 서류관리 U
    @PutMapping("/changeInfo/docuManagementId/{docuManagementId}")
    @Operation(summary = "서류관리 정보 수정")
    public CommonResult putDocuManagement(@PathVariable long docuManagementId, @RequestBody DocuManagementRequest request) {
        docuManagementService.putDocuManagement(docuManagementId, request);
        return ResponseService.getSuccessResult();
    }

    // 서류관리 D
    @DeleteMapping("/delete/docuManagementId/{docuManagementId}")
    @Operation(summary = "서류관리 정보 삭제")
    public CommonResult delDocuManagement(@PathVariable long docuManagementId) {
        docuManagementService.delDocuManagement(docuManagementId);
        return  ResponseService.getSuccessResult();
    }
}
