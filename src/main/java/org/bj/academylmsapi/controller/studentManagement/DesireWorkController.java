package org.bj.academylmsapi.controller.studentManagement;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.model.result.CommonResult;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.model.studentManagement.desireWork.DesireWorkRequest;
import org.bj.academylmsapi.model.studentManagement.desireWork.DesireWorkResponse;
import org.bj.academylmsapi.service.ResponseService;
import org.bj.academylmsapi.service.studentManagement.DesireWorkService;
import org.springframework.web.bind.annotation.*;

// 학생관리 - 취업관리 - 희망근무조건 controller

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/desireWork")
@Tag(name = "DesireWork", description = "[학생관리] 희망근무조건")
public class DesireWorkController {
    private final DesireWorkService desireWorkService;

    // 희망근무조건 C
    @PostMapping("/new")
    @Operation(summary = "희망근무조건 등록")
    public CommonResult setDesireWork(@RequestBody DesireWorkRequest request) {
        desireWorkService.setDesireWork(request);
        return ResponseService.getSuccessResult();
    }

    // 희망근무조건 단수 R
    @GetMapping("/detail/desireWorkId/{desireWorkId}")
    @Operation(summary = "희망근무조건 상세보기")
    public SingleResult<DesireWorkResponse> getDesireWork(@PathVariable long desireWorkId) {
        return ResponseService.getSingleResult(desireWorkService.getDesireWork(desireWorkId));
    }

    // 희망근무조건 U
    @PutMapping("/changeInfo/desireWorkId/{desireWorkId}")
    @Operation(summary = "희망근무조건 정보 수정")
    public CommonResult putDesireWork(@PathVariable long desireWorkId, @RequestBody DesireWorkRequest request) {
        desireWorkService.putDesireWork(desireWorkId, request);
        return ResponseService.getSuccessResult();
    }

    // 희망근무조건 D
    @DeleteMapping("/delete/desireWorkId/{desireWorkId}")
    @Operation(summary = "희망근무조건 정보 삭제")
    public CommonResult delDesireWork(@PathVariable long desireWorkId) {
        desireWorkService.delDesireWork(desireWorkId);
        return ResponseService.getSuccessResult();
    }
}
