package org.bj.academylmsapi.controller.studentManagement;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.model.result.CommonResult;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.model.studentManagement.career.CareerRequest;
import org.bj.academylmsapi.model.studentManagement.career.CareerResponse;
import org.bj.academylmsapi.service.ResponseService;
import org.bj.academylmsapi.service.studentManagement.CareerService;
import org.springframework.web.bind.annotation.*;

// 학생관리 - 취업관리 - 경력사항 controller

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/career")
@Tag(name = "Career", description = "[학생관리] 경력사항")
public class CareerController {
    private final CareerService careerService;

    // 경력사항 C
    @PostMapping("/new")
    @Operation(summary = "경력사항 등록")
    public CommonResult setCareer(CareerRequest request) {
        careerService.setCareer(request);
        return ResponseService.getSuccessResult();
    }

    // 경력사항 단수 R
    @GetMapping("/detail/careerId/{careerId}")
    @Operation(summary = "경력사항 상세보기")
    public SingleResult<CareerResponse> getCareer(@PathVariable long careerId) {
        return ResponseService.getSingleResult(careerService.getCareer(careerId));
    }

    // 경력사항 U
    @PutMapping("/changeInfo/careerId/{careerId}")
    @Operation(summary = "경력사항 정보 수정")
    public CommonResult putCareer(@PathVariable long careerId, @RequestBody CareerRequest request) {
        careerService.putCareer(careerId, request);
        return ResponseService.getSuccessResult();
    }

    // 경력사항 D
    @DeleteMapping("/delete/careerId/{careerId}")
    @Operation(summary = "경력사항 정보 삭제")
    public CommonResult delCareer(@PathVariable long careerId) {
        careerService.delCareer(careerId);
        return ResponseService.getSuccessResult();
    }
}
