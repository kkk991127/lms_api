package org.bj.academylmsapi.controller.studentManagement;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.model.result.CommonResult;
import org.bj.academylmsapi.model.result.ListResult;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.model.studentManagement.adviceManagement.AdviceManagementItem;
import org.bj.academylmsapi.model.studentManagement.adviceManagement.AdviceManagementRequest;
import org.bj.academylmsapi.model.studentManagement.adviceManagement.AdviceManagementResponse;
import org.bj.academylmsapi.service.ListConvertService;
import org.bj.academylmsapi.service.ResponseService;
import org.bj.academylmsapi.service.studentManagement.AdviceManagementService;
import org.springframework.web.bind.annotation.*;

// 학생관리 - 취업관리 - 상담관리 controller


@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/adviceManagement")
@Tag(name = "AdviceManagement", description = "[학생관리] 상담관리")
public class AdviceManagementController {
    private final AdviceManagementService adviceManagementService;

    // 상담관리 C
    @PostMapping("/new")
    @Operation(summary = "상담관리 등록")
    public CommonResult setAdviceManagement(@RequestBody AdviceManagementRequest request) {
        adviceManagementService.setAdviceManagement(request);
        return ResponseService.getSuccessResult();
    }

    // 상담관리 복수 R
    @GetMapping("/all")
    @Operation(summary = "상담관리 리스트 전체보기")
    public ListResult<AdviceManagementItem> getAdviceManagements() {
        return ListConvertService.settingListResult(adviceManagementService.getAdviceManagements());
    }

    // 상담관리 복수 R 페이징
    @GetMapping("/all/pageNum/{pageNum}")
    @Operation(summary = "상담관리 리스트 (페이징)")
    public ListResult<AdviceManagementItem> getAdviceManagementsPage(@PathVariable int pageNum) {
        return adviceManagementService.getAdviceManagementsPage(pageNum);
    }

    // 상담관리 단수 R
    @GetMapping("/detail/adviceManagementId/{adviceManagementId}")
    @Operation(summary = "상담관리 상세보기")
    public SingleResult<AdviceManagementResponse> getAdviceManagement(@PathVariable long adviceManagementId) {
        return ResponseService.getSingleResult(adviceManagementService.getAdviceManagement(adviceManagementId));
    }

    // 상담관리 U
    @PutMapping("/changeInfo/adviceManagementId/{adviceManagementId}")
    @Operation(summary = "상담관리 정보 수정")
    public CommonResult putAdviceManagement(@PathVariable long adviceManagementId, @RequestBody AdviceManagementRequest request) {
        adviceManagementService.putAdviceManagement(adviceManagementId, request);
        return ResponseService.getSuccessResult();
    }

    // 상담관리 D
    @DeleteMapping("/delete/adviceManagementId/{adviceManagementId}")
    @Operation(summary = "상담관리 정보 삭제")
    public CommonResult delAdviceManagement(@PathVariable long adviceManagementId) {
        adviceManagementService.delAdviceManagement(adviceManagementId);
        return ResponseService.getSuccessResult();
    }
}
