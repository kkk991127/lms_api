package org.bj.academylmsapi.controller.studentManagement;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.model.result.CommonResult;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.model.studentManagement.workManagement.WorkManagementRequest;
import org.bj.academylmsapi.model.studentManagement.workManagement.WorkManagementResponse;
import org.bj.academylmsapi.service.ResponseService;
import org.bj.academylmsapi.service.studentManagement.WorkManagementService;
import org.springframework.web.bind.annotation.*;

// 학생관리 - 취업관리 - 취업관리사항 controller

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/workManagement")
@Tag(name = "WorkManagement", description = "[학생관리] 취업관리사항")
public class WorkManagementController {
    private final WorkManagementService workManagementService;

    // 취업관리사항 C
    @PostMapping("/new")
    @Operation(summary = "취업관리사항 등록")
    public CommonResult setWorkManagement(@RequestBody WorkManagementRequest request) {
        workManagementService.setWorkManagement(request);
        return ResponseService.getSuccessResult();
    }

    // 취업관리사항 단수 R
    @GetMapping("/detail/workManagementId/{workManagementId}")
    @Operation(summary = "취업관리사항 상세보기")
    public SingleResult<WorkManagementResponse> getWorkManagement(@PathVariable long workManagementId) {
        return ResponseService.getSingleResult(workManagementService.getWorkManagement(workManagementId));
    }

    // 취업관리사항 U
    @PutMapping("/changeId/workManagementId/{workManagementId}")
    @Operation(summary = "취업관리사항 정보 수정")
    public CommonResult putWorkManagement(@PathVariable long workManagementId, @RequestBody WorkManagementRequest request) {
        workManagementService.putWorkManagement(workManagementId, request);
        return ResponseService.getSuccessResult();
    }

    // 취업관리사항 D
    @DeleteMapping("/delete/workManagementId/{workManagementId}")
    @Operation(summary = "취업관리사항 정보 삭제")
    public CommonResult delWorkManagement(@PathVariable long workManagementId) {
        workManagementService.delWorkManagement(workManagementId);
        return ResponseService.getSuccessResult();
    }
}
