package org.bj.academylmsapi.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.model.result.CommonResult;
import org.bj.academylmsapi.model.result.ListResult;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.model.subjectChoice.SubjectChoiceItem;
import org.bj.academylmsapi.model.subjectChoice.SubjectChoiceRequest;
import org.bj.academylmsapi.model.subjectChoice.SubjectChoiceResponse;
import org.bj.academylmsapi.service.ListConvertService;
import org.bj.academylmsapi.service.ResponseService;
import org.bj.academylmsapi.service.SubjectChoiceService;
import org.springframework.web.bind.annotation.*;

// 과정 controller

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/subjectchoice")
@Tag(name = "SubjectChoice", description = "과정")
public class SubjectChoiceController {
    private final SubjectChoiceService subjectChoiceService;

    // 과정 C
    @PostMapping("/new")
    @Operation(summary = "과정 등록")
    public CommonResult setSubjectChoice(@RequestBody SubjectChoiceRequest request) {
        subjectChoiceService.setSubjectChoice(request);
        return ResponseService.getSuccessResult();
    }

    // 과정 복수 R
    @GetMapping("/all")
    @Operation(summary = "과정 목록 보기 ")
    public ListResult<SubjectChoiceItem> getSubjectList() {
        return ListConvertService.settingListResult(subjectChoiceService.getSubjectList());
    }

    // 과정 복수 R 페이징
    @GetMapping("/all/pageNum/{pageNum}")
    @Operation(summary = "과정 목록 보기 (페이징)")
    public ListResult<SubjectChoiceItem> getSubjectPageList(@PathVariable int pageNum) {
        return subjectChoiceService.getSubjectPageList(pageNum);
    }

    // 과정 단수 R
    @GetMapping("/detail/subjectChoiceId/{subjectChoiceId}")
    @Operation(summary = "과정 상세 보기 ")
    public SingleResult<SubjectChoiceResponse> getSubjectDetail(@PathVariable long subjectChoiceId) {
        return ResponseService.getSingleResult(subjectChoiceService.getSubjectDetail(subjectChoiceId));
    }

    // 과정 U
    @PutMapping("/changeInfo/subjectChoiceId/{subjectChoiceId}")
    @Operation(summary = "과정 정보 수정")
    public CommonResult putSubjectChoice(@PathVariable long subjectChoiceId, @RequestBody SubjectChoiceRequest subjectRequest) {
        subjectChoiceService.putSubjectChoice(subjectChoiceId, subjectRequest);
        return ResponseService.getSuccessResult();
    }

    // 과정 D
    @DeleteMapping("/delete/subjectChoiceId/{subjectChoiceId}")
    @Operation(summary = "과정 정보 삭제")
    private CommonResult delSubject(@PathVariable long subjectChoiceId) {
        subjectChoiceService.delSubject(subjectChoiceId);
        return ResponseService.getSuccessResult();
    }
}
