package org.bj.academylmsapi.controller;

import org.bj.academylmsapi.exception.CAcademyFullException;
import org.bj.academylmsapi.model.result.CommonResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

// 보안적 문제로 메세지를 던져줘야하는데 그것을 위한 컨트롤러 ( 메세지 통일성을 위한 )
@RestController
@RequestMapping("/exception")
public class ExceptionController {

    //접근 거부 엑세스 거부
    @GetMapping("/access-denied")
    public CommonResult accessDeniedException(){
        throw new CAcademyFullException();
    }
    // 로그인을 했는데 권한이 있는 사람만 접근 할 수 있는
    @GetMapping("/entry-point")
    public CommonResult entryPointException() {
        throw new CAcademyFullException();
    }


}
