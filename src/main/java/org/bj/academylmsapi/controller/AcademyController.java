package org.bj.academylmsapi.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.model.academy.AcademyInfoChangeRequest;
import org.bj.academylmsapi.model.academy.AcademyItem;
import org.bj.academylmsapi.model.academy.AcademyRequest;
import org.bj.academylmsapi.model.academy.AcademyResponse;
import org.bj.academylmsapi.model.result.CommonResult;
import org.bj.academylmsapi.model.result.ListResult;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.service.AcademyService;
import org.bj.academylmsapi.service.ListConvertService;
import org.bj.academylmsapi.service.ResponseService;
import org.springframework.web.bind.annotation.*;

// 학원 controller

@RestController
@RequiredArgsConstructor
@RequestMapping("v1/academy")
@Tag(name = "Academy", description = "학원")
public class AcademyController {
    private final AcademyService academyService;

    // 학원 C
    @PostMapping("/new")
    @Operation(summary = "학원 등록")
    public CommonResult setAcademy(@RequestBody AcademyRequest request) {
        academyService.setAcademy(request);
        return ResponseService.getSuccessResult();
    }

    // 학원 복수 R
    @GetMapping("/all")
    @Operation(summary = "학원 리스트 전체보기")
    public ListResult<AcademyItem> getAcademys() {
        return ListConvertService.settingListResult(academyService.getAcademys());
    }

    // 학원 복수 R 페이징
    @GetMapping("/all/pageNum/{pageNum}")
    @Operation(summary = "학원 리스트 전체보기 (페이징)")
    public ListResult<AcademyItem> getAcademysPage(@PathVariable int pageNum) {
        return academyService.getAcademysPage(pageNum);
    }

    // 학원 단수 R
    @GetMapping("/detail/academyId/{academyId}")
    @Operation(summary = "학원 상세보기")
    public SingleResult<AcademyResponse> getAcademy(@PathVariable long academyId) {
        return ResponseService.getSingleResult(academyService.getAcademy(academyId));
    }

    // 학원 U
    @PutMapping("/changeInfo/academyId/{academyId}")
    @Operation(summary = "학원 정보 변경")
    public CommonResult putAcademy(@PathVariable long academyId, @RequestBody AcademyInfoChangeRequest request) {
        academyService.putAcademy(academyId, request);
        return ResponseService.getSuccessResult();
    }

    // 학원 D
    @DeleteMapping("/delete/academyId/{academyId}")
    @Operation(summary = "학원 정보 삭제")
    public CommonResult delAcademy(@PathVariable long academyId) {
        academyService.delAcademy(academyId);
        return ResponseService.getSuccessResult();
    }
}
