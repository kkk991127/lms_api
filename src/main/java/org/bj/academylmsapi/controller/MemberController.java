package org.bj.academylmsapi.controller;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.model.MemberRequest;
import org.bj.academylmsapi.model.result.CommonResult;
import org.bj.academylmsapi.service.MemberService;
import org.bj.academylmsapi.service.ResponseService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("v1/member")
public class MemberController {
    private final MemberService memberService;

    @PostMapping("/new")
    public CommonResult setMember(@RequestBody MemberRequest request) {
        memberService.setMember(request);
        return ResponseService.getSuccessResult();
    }
}
