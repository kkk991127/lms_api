package org.bj.academylmsapi.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.model.admin.AdminInfoChangeRequest;
import org.bj.academylmsapi.model.admin.AdminItem;
import org.bj.academylmsapi.model.admin.AdminRequest;
import org.bj.academylmsapi.model.admin.AdminResponse;
import org.bj.academylmsapi.model.result.CommonResult;
import org.bj.academylmsapi.model.result.ListResult;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.service.AdminService;
import org.bj.academylmsapi.service.ListConvertService;
import org.bj.academylmsapi.service.ResponseService;
import org.springframework.web.bind.annotation.*;

// 관리자 controller

@RestController
@RequiredArgsConstructor
@RequestMapping("v1/admin")
@Tag(name = "Admin", description = "관리자")
public class AdminController {
    private final AdminService adminService;

    // 관리자 C
    @PostMapping("/new")
    @Operation(summary = "관리자 등록")
    public CommonResult setAdmin(@RequestBody AdminRequest request) {
        adminService.setAdmin(request);
        return ResponseService.getSuccessResult();
    }

    // 관리자 복수 R
    @GetMapping("/all")
    @Operation(summary = "관리자 리스트 전체보기")
    public ListResult<AdminItem> getAdmins() {
        return ListConvertService.settingListResult(adminService.getAdmins());
    }

    // 관리자 복수 R 페이징
    @GetMapping("/all/pageNum/{pageNum}")
    @Operation(summary = "관리자 리스트 (페이징)")
    public ListResult<AdminItem> getAdmins(@PathVariable int pageNum) {
        return adminService.getAdminsPage(pageNum);
    }

    // 관리자 단수 R
    @GetMapping("/detail/adminId/{adminId}")
    @Operation(summary = "관리자 상세보기")
    public SingleResult<AdminResponse> getAdmin(@PathVariable long adminId) {
        return ResponseService.getSingleResult(adminService.getAdmin(adminId));
    }

    // 관리자 U
    @PutMapping("/changeInfo/adminId/{adminId}")
    @Operation(summary = "관리자 정보 수정")
    public CommonResult putAdmin(@PathVariable long adminId, @RequestBody AdminInfoChangeRequest request) {
        adminService.putAdmin(adminId, request);
        return ResponseService.getSuccessResult();
    }

    // 관리자 D
    @DeleteMapping("delete/adminId/{adminId}")
    @Operation(summary = "관리자 정보 삭제")
    public CommonResult delAdmin(@PathVariable long adminId) {
        adminService.delAdmin(adminId);
        return ResponseService.getSuccessResult();
    }
}
