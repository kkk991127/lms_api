package org.bj.academylmsapi.controller.board;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.model.board.board.BoardRequest;
import org.bj.academylmsapi.model.board.board.BoardResponse;
import org.bj.academylmsapi.model.board.reply.ReplyRequest;
import org.bj.academylmsapi.model.board.reply.ReplyResponse;
import org.bj.academylmsapi.model.result.CommonResult;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.service.ResponseService;
import org.bj.academylmsapi.service.board.ReplyService;
import org.springframework.web.bind.annotation.*;

// 게시판 - 게시판 답변 controller

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/reply")
@Tag(name = "Reply", description = "[게시판] 답변")
public class ReplyController {
    private final ReplyService replyService;

    // 답변 C
    @PostMapping("/new")
    @Operation(summary = "답변 등록")
    public CommonResult setBoard(@RequestBody ReplyRequest request) {
        replyService.setReply(request);
        return ResponseService.getSuccessResult();
    }

    // 답변 단수 R
    @GetMapping("/detail/replyId/{replyId}")
    @Operation(summary = "답변 상세보기")
    public SingleResult<ReplyResponse> getBoard(@PathVariable long replyId) {
        return ResponseService.getSingleResult(replyService.getReply(replyId));
    }

    // 답변 U
    @PutMapping("/changeInfo/replyId/{replyId}")
    @Operation(summary = "답변 정보 수정")
    public CommonResult putReply(@PathVariable long replyId, @RequestBody ReplyRequest request) {
        replyService.putReply(replyId, request);
        return ResponseService.getSuccessResult();
    }

    // 답변 D
    @DeleteMapping("/delete/replyId/{replyId}")
    @Operation(summary = "답변 정보 삭제")
    public CommonResult setBoard(@PathVariable long replyId) {
        replyService.delReply(replyId);
        return ResponseService.getSuccessResult();
    }
}
