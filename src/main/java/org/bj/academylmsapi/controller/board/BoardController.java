package org.bj.academylmsapi.controller.board;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.model.board.board.BoardRequest;
import org.bj.academylmsapi.model.board.board.BoardResponse;
import org.bj.academylmsapi.model.result.CommonResult;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.service.ResponseService;
import org.bj.academylmsapi.service.board.BoardService;
import org.springframework.web.bind.annotation.*;

// 게시판 - 게시판 controller

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/board")
@Tag(name = "Board", description = "[게시판] 게시판")
public class BoardController {
    private final BoardService boardService;

    // 게시판 C
    @PostMapping("/new")
    @Operation(summary = "게시판 등록")
    public CommonResult setBoard(@RequestBody BoardRequest request) {
        boardService.setBoard(request);
        return ResponseService.getSuccessResult();
    }

    // 게시판 단수 R
    @GetMapping("/detail/boardId/{boardId}")
    @Operation(summary = "게시판 상세보기")
    public SingleResult<BoardResponse> getBoard(@PathVariable long boardId) {
        return ResponseService.getSingleResult(boardService.getBoard(boardId));
    }

    // 게시판 U
    @PutMapping("/changeInfo/boardId/{boardId}")
    @Operation(summary = "게시판 정보 수정")
    public CommonResult putBoard(@PathVariable long boardId, @RequestBody BoardRequest request) {
        boardService.putBoard(boardId, request);
        return ResponseService.getSuccessResult();
    }

    // 게시판 D
    @DeleteMapping("/delete/boardId/{boardId}")
    @Operation(summary = "게시판 정보 삭제")
    public CommonResult delBoard(@PathVariable long boardId) {
        boardService.delBoard(boardId);
        return ResponseService.getSuccessResult();
    }
}
