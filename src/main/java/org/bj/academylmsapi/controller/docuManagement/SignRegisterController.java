package org.bj.academylmsapi.controller.docuManagement;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.model.docuManagement.signRegister.SignRegisterRequest;
import org.bj.academylmsapi.model.docuManagement.signRegister.SignRegisterResponse;
import org.bj.academylmsapi.model.result.CommonResult;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.service.ResponseService;
import org.bj.academylmsapi.service.docuManagement.SignRegisterService;
import org.springframework.web.bind.annotation.*;

// 서류관리 - 서류등록 controller

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/SignRegister")
@Tag(name = "SignRegister", description = "[서류관리] 서류등록")
public class SignRegisterController {
    private final SignRegisterService signRegisterService;

    // 서류등록 C
    @PostMapping("/new")
    @Operation(summary = "서류등록")
    public CommonResult setSignRegister(@RequestBody SignRegisterRequest request) {
        signRegisterService.setSignRegister(request);
        return ResponseService.getSuccessResult();
    }

    // 서류등록 단수 R
    @GetMapping("/detail/signRegisterId/{signRegisterId}")
    @Operation(summary = "서류등록 상세보기")
    public SingleResult<SignRegisterResponse> getSignRegister(@PathVariable long signRegisterId) {
        return ResponseService.getSingleResult(signRegisterService.getSignRegister(signRegisterId));
    }

    // 서류등록 C
    @PutMapping("/changeInfo/signRegisterId/{signRegisterId}")
    @Operation(summary = "서류등록 정보 수정")
    public CommonResult putSignRegister(@PathVariable long signRegisterId, @RequestBody SignRegisterRequest request) {
        signRegisterService.putSignRegister(signRegisterId, request);
        return ResponseService.getSuccessResult();
    }

    // 서류등록 D
    @DeleteMapping("/delete/signRegisterId/{signRegisterId}")
    @Operation(summary = "서류등록 정보 삭제")
    public CommonResult delSignRegister(@PathVariable long signRegisterId) {
        signRegisterService.delSignRegister(signRegisterId);
        return ResponseService.getSuccessResult();
    }
}
