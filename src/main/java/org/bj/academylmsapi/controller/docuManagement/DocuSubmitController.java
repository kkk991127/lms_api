package org.bj.academylmsapi.controller.docuManagement;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.model.docuManagement.docuSubmit.DocuSubmitRequest;
import org.bj.academylmsapi.model.docuManagement.docuSubmit.DocuSubmitResponse;
import org.bj.academylmsapi.model.result.CommonResult;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.service.ResponseService;
import org.bj.academylmsapi.service.docuManagement.DocuSubmitService;
import org.springframework.web.bind.annotation.*;

// 서류관리 - 서류제출 controller

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/docuSubmit")
@Tag(name = "DocuSubmit", description = "[서류관리] 서류제출")
public class DocuSubmitController {
    private final DocuSubmitService docuSubmitService;

    // 서류제출 C
    @PostMapping("/new")
    @Operation(summary = "서류제출 등록")
    public CommonResult setDocuSubmit(@RequestBody DocuSubmitRequest request) {
        docuSubmitService.setDocuSubmit(request);
        return ResponseService.getSuccessResult();
    }

    // 서류제출 단수 R
    @GetMapping("/detail/docuSubmitId/{docuSubmitId}")
    @Operation(summary = "서류제출 상세보기")
    public SingleResult<DocuSubmitResponse> getDocuSubmit(@PathVariable long docuSubmitId) {
        return ResponseService.getSingleResult(docuSubmitService.getDocuSubmit(docuSubmitId));
    }

    // 서류제출 U
    @PutMapping("/changeInfo/docuSubmitId/{docuSubmitId}")
    @Operation(summary = "서류제출 정보 수정")
    public CommonResult putDocuSubmit(@PathVariable long docuSubmitId, @RequestBody DocuSubmitRequest request) {
        docuSubmitService.putDocuSubmit(docuSubmitId, request);
        return ResponseService.getSuccessResult();
    }

    // 서류제출 D
    @DeleteMapping("/delete/docuSubmitId/{docuSubmitId}")
    @Operation(summary = "서류제출 정보 삭제")
    public CommonResult delDocuSubmit(@PathVariable long docuSubmitId) {
        docuSubmitService.delDocuSubmit(docuSubmitId);
        return ResponseService.getSuccessResult();
    }
}
