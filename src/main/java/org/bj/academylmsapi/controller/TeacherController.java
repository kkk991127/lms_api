package org.bj.academylmsapi.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.Teacher;
import org.bj.academylmsapi.model.result.CommonResult;
import org.bj.academylmsapi.model.result.ListResult;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.model.teacher.*;
import org.bj.academylmsapi.service.ListConvertService;
import org.bj.academylmsapi.service.ResponseService;
import org.bj.academylmsapi.service.TeacherService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

// 강사 controller

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/teacher")
@Tag(name = "Teacher", description = "강사")
public class TeacherController {
    private final TeacherService teacherService;

    // 강사 C
    @PostMapping("/new")
    @Operation(summary = "강사 등록")
    public CommonResult setTeacher(@RequestBody TeacherRequest request) {
        teacherService.setTeacher(request);
        return ResponseService.getSuccessResult();
    }

    // 강사 복수 R
    @GetMapping("/all")
    @Operation(summary = "강사 리스트 전체보기")
    public ListResult<TeacherItem> getTeachers() {
        return ListConvertService.settingListResult(teacherService.getTeachers());
    }

    // 강사 복수 R 페이징
    @GetMapping("/all/pageNum/{pageNum}")
    @Operation(summary = "강사 리스트 (페이징)")
    public ListResult<TeacherItem> getTeachersPage(@PathVariable int pageNum) {
        return teacherService.getTeachersPage(pageNum);
    }

    // 강사 단수 R
    @GetMapping("/detail/teacherId/{teacherId}")
    @Operation(summary = "강사 상세보기")
    public SingleResult<TeacherResponse> getTeacher(@PathVariable long teacherId) {
        return ResponseService.getSingleResult(teacherService.getTeacher(teacherId));
    }

    // 강사 U
    @PutMapping("/changeInfo/teacherId/{teacherId}")
    @Operation(summary = "강사 정보 수정")
    public CommonResult putTeacher(@PathVariable long teacherId, @RequestBody TeacherInfoChangeRequest request) {
        teacherService.putTeacherInfo(teacherId, request);
        return ResponseService.getSuccessResult();
    }

    // 강사 U 관리자 버전
    @PutMapping("/changeInfoAdmin/teacherId/{teacherId}")
    @Operation(summary = "강사 정보 수정 관리자용")
    public CommonResult putTeacherAdmin(@PathVariable long teacherId, @RequestBody TeacherInfoChangeAdminRequest request) {
        teacherService.putTeacherInfo(teacherId, request);
        return ResponseService.getSuccessResult();
    }

    // 강사 D
    @DeleteMapping("/delete/teacherId/{teacherId}")
    @Operation(summary = "강사 정보 삭제")
    public CommonResult delTeacher(@PathVariable long teacherId) {
        teacherService.delTeacher(teacherId);
        return ResponseService.getSuccessResult();
    }
}
