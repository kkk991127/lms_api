package org.bj.academylmsapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

// 성별 enum

@AllArgsConstructor
@Getter
public enum Gender {
    MAN("남자"),
    WOMAN("여자");

    private final String genderName;
}
