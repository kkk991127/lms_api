package org.bj.academylmsapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

// 교육타입 enum

@AllArgsConstructor
@Getter
public enum TrainingType {
    TOMORROWSTUDY("국민내일배움카드(일반)"),
    NATIONALINDUSTRY("국가기간전략산업직종"),
    KDIGITAL("K-디지털트레이닝"),
    SMARTMIX("스마트혼합훈련");

    private final String trainingTypeName;
}
