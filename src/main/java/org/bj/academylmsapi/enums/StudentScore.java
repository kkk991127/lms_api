package org.bj.academylmsapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum StudentScore {
    TEN("100"),
    NINE("90"),
    EIGHT("80"),
    SEVEN("70"),
    SIX("60"),
    FIVE("50"),
    FOUR("40"),
    THREE("30"),
    TWO("20"),
    ONE("10"),
    ZERO("0");

    private final String studentScoreName;
}
