package org.bj.academylmsapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

// 평가유형
@AllArgsConstructor
@Getter
public enum TestType {
    CHECK("평가자체크리스트(N)"),
    PORTFOLIO("포트폴리오(N)");

    private final String testTypeName;
}
