package org.bj.academylmsapi.enums;


import lombok.AllArgsConstructor;
import lombok.Getter;


// 출결유형 Enum

@AllArgsConstructor
@Getter
public enum AttendanceType {

    EARLYLEAVE("조퇴"),
    ABSENT("결석");

    private final String attendanceTypeName;
}
