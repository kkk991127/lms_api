package org.bj.academylmsapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

// 신청유형 Enum

@AllArgsConstructor
@Getter
public enum DemandType {

    LATE("지각"),
    EARLYLEAVE("조퇴"),
    ABSENT("결석");

    private final String demandTypeName;
}
