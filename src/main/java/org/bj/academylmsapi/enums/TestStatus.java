package org.bj.academylmsapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TestStatus {
    NOANSWER("응시 전"),
    PUTANSWER("응시완료 (서명 전)"),
    SIGNOKAY("응시완료 (서명 완료)");

    private final String TestStatus;

}
