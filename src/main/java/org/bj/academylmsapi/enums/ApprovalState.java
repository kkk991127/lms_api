package org.bj.academylmsapi.enums;


import lombok.AllArgsConstructor;
import lombok.Getter;

// 승인상태 Enum

@AllArgsConstructor
@Getter
public enum ApprovalState {
    NOTAPPROVED("미승인"),
    TEACHERAPPROVAL("강사승인"),
    ADMINAPPROVAL("관리자승인");

    private final String approvalStateName;
}
