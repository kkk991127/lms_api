package org.bj.academylmsapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

// 수강상태 enum
@AllArgsConstructor
@Getter
public enum StudentStatus {
    INCLASS("수강"),
    OUTCLASS("수강포기"),
    WAITCLASS("대기중");

    private final String studentStatusName;
}
