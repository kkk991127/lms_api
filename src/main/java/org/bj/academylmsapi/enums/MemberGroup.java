package org.bj.academylmsapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum MemberGroup {
    ROLE_MEMBER("멤버"),
    ROLE_NO_MEMBER("비회원");

    private final String MemberName;

}
