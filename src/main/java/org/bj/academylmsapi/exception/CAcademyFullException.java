package org.bj.academylmsapi.exception;

public class CAcademyFullException extends RuntimeException {
    public CAcademyFullException(String msg, Throwable t) {
        super(msg, t);
    }

    public CAcademyFullException(String msg) {
        super(msg);
    }

    public CAcademyFullException(){

    }
}
