package org.bj.academylmsapi.entity.docuManagement;


import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.SubjectChoice;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.bj.academylmsapi.model.docuManagement.signRegister.SignRegisterRequest;

import java.time.LocalDateTime;

// 서류관리 - 서류등록 entity

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class SignRegister {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 과정명 선택 id FK
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "subjectChoiceId")
    private SubjectChoice subjectChoice;

    // 제목
    @Column(nullable = false)
    private String boardTitle;

    // 작성자
    @Column(nullable = false)
    private String boardWriter;

    // 내용
    @Column(nullable = false)
    private String boardContent;

    // 첨부파일
    @Column(nullable = false)
    private String addFile;

    // 날짜
    @Column(nullable = false)
    private LocalDateTime dateCreate;

    public void putSignRegister(SignRegisterRequest request, SubjectChoice subjectChoice) {
        this.subjectChoice = subjectChoice;
        this.boardTitle = request.getBoardTitle();
        this.boardWriter = request.getBoardWriter();
        this.boardContent = request.getBoardContent();
        this.addFile = request.getAddFile();
        this.dateCreate = LocalDateTime.now();
    }

    private SignRegister(Builder builder) {
        this.subjectChoice = builder.subjectChoice;
        this.boardTitle = builder.boardTitle;
        this.boardWriter = builder.boardWriter;
        this.boardContent = builder.boardContent;
        this.addFile = builder.addFile;
        this.dateCreate = builder.dateCreate;
    }

    public static class Builder implements CommonModelBuilder<SignRegister> {
        private final SubjectChoice subjectChoice;
        private final String boardTitle;
        private final String boardWriter;
        private final String boardContent;
        private final String addFile;
        private final LocalDateTime dateCreate;

        public Builder(SignRegisterRequest request, SubjectChoice subjectChoice) {
            this.subjectChoice = subjectChoice;
            this.boardTitle = request.getBoardTitle();
            this.boardWriter = request.getBoardWriter();
            this.boardContent = request.getBoardContent();
            this.addFile = request.getAddFile();
            this.dateCreate = LocalDateTime.now();
        }

        @Override
        public SignRegister build() {
            return new SignRegister(this);
        }
    }
}
