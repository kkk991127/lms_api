package org.bj.academylmsapi.entity.docuManagement;


import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.bj.academylmsapi.model.docuManagement.docuSubmit.DocuSubmitRequest;

import java.time.LocalDateTime;

// 서류관리 - 서류제출 entity

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class DocuSubmit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 서류등록 id FK
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "signRegisterId")
    private SignRegister signRegister;

    // 수강생id FK
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "studentId")
    private Student student;

    // 서명파일
    private String signFile;

    // 첨부파일
    @Column(nullable = false)
    private String addFile;

    // 서명완료
    @Column(nullable = false)
    private Boolean isSignClear;

    // 날짜
    @Column(nullable = false)
    private LocalDateTime dateCreate;

    public void putDocuSubmit(DocuSubmitRequest request, SignRegister signRegister, Student student) {
        this.signRegister = signRegister;
        this.student = student;
        this.signFile = request.getSignFile();
        this.addFile = request.getAddFile();
        this.isSignClear = request.getIsSignClear();
    }

    private DocuSubmit(Builder builder) {
        this.signRegister = builder.signRegister;
        this.student = builder.student;
        this.signFile = builder.signFile;
        this.addFile = builder.addFile;
        this.isSignClear = builder.isSignClear;
        this.dateCreate = builder.dateCreate;
    }

    public static class Builder implements CommonModelBuilder<DocuSubmit> {
        private final SignRegister signRegister;
        private final Student student;
        private final String signFile;
        private final String addFile;
        private final Boolean isSignClear;
        private final LocalDateTime dateCreate;

        public Builder(DocuSubmitRequest request, SignRegister signRegister, Student student) {
            this.signRegister = signRegister;
            this.student = student;
            this.signFile = request.getSignFile();
            this.addFile = request.getAddFile();
            this.isSignClear = request.getIsSignClear();
            this.dateCreate = LocalDateTime.now();
        }

        @Override
        public DocuSubmit build() {
            return new DocuSubmit(this);
        }
    }
}
