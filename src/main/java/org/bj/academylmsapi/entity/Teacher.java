package org.bj.academylmsapi.entity;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.enums.Gender;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.bj.academylmsapi.model.teacher.TeacherInfoChangeAdminRequest;
import org.bj.academylmsapi.model.teacher.TeacherInfoChangeRequest;
import org.bj.academylmsapi.model.teacher.TeacherRequest;

import java.time.LocalDate;
import java.time.LocalDateTime;

// 강사 entity

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED) // 접근제어자 protected로 준다
public class Teacher {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 사진주소
    @Column(nullable = false)
    private String imgSrc;

    // 이름
    @Column(nullable = false, length = 20)
    private String teacherName;

    // 성별
    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private Gender gender;

    // 생년월일
    @Column(nullable = false)
    private LocalDate dateBirth;

    // 본인휴대폰
    @Column(nullable = false, length = 13)
    private String phoneNumber;

    // 주소
    @Column(nullable = false)
    private String address;

    // 이메일
    @Column(nullable = false)
    private String email;

    // 가입일
    @Column(nullable = false)
    private LocalDateTime dateJoin;

    // 아이디
    @Column(nullable = false)
    private String identity;

    // 비밀번호
    @Column(nullable = false)
    private String password;

    // 강사 U
    public void putTeacher(TeacherInfoChangeRequest request) {
        this.imgSrc = request.getImgSrc();
        this.teacherName = request.getTeacherName();
        this.phoneNumber = request.getPhoneNumber();
        this.address = request.getAddress();
        this.email = request.getEmail();
    }

    // 강사 U 관리자 버전
    public void putTeacherAdmin(TeacherInfoChangeAdminRequest request) {
        this.imgSrc = request.getImgSrc();
        this.teacherName = request.getTeacherName();
        this.gender = request.getGender();
        this.dateBirth = request.getDateBirth();
        this.phoneNumber = request.getPhoneNumber();
        this.address = request.getAddress();
        this.email = request.getEmail();
        this.identity = request.getIdentity();
        this.password = request.getPassword();
    }
    private Teacher(Builder builder) {
        this.imgSrc = builder.imgSrc;
        this.teacherName = builder.teacherName;
        this.gender = builder.gender;
        this.dateBirth = builder.dateBirth;
        this.phoneNumber = builder.phoneNumber;
        this.address = builder.address;
        this.email = builder.email;
        this.dateJoin = builder.dateJoin;
        this.identity = builder.identity;
        this.password = builder.password;
    }

    public static class Builder implements CommonModelBuilder<Teacher> {
        private final String imgSrc;
        private final String teacherName;
        private final Gender gender;
        private final LocalDate dateBirth;
        private final String phoneNumber;
        private final String address;
        private final String email;
        private final LocalDateTime dateJoin;
        private final String identity;
        private final String password;

        public Builder(TeacherRequest request) {
            this.imgSrc = request.getImgSrc();
            this.teacherName = request.getTeacherName();
            this.gender = request.getGender();
            this.dateBirth = request.getDateBirth();
            this.phoneNumber = request.getPhoneNumber();
            this.address = request.getAddress();
            this.email = request.getEmail();
            this.dateJoin = LocalDateTime.now();
            this.identity = request.getIdentity();
            this.password = request.getPassword();
        }

        @Override
        public Teacher build() {
            return new Teacher(this);
        }
    }
}
