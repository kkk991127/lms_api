package org.bj.academylmsapi.entity;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.enums.Gender;
import org.bj.academylmsapi.enums.StudentStatus;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.bj.academylmsapi.model.student.StudentInfoChangeAdminRequest;
import org.bj.academylmsapi.model.student.StudentInfoChangeRequest;
import org.bj.academylmsapi.model.student.StudentRequest;

import java.time.LocalDate;

// 수강생 entity

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 과정명 선택id FK
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "subjectChoiceId")
    private SubjectChoice subjectChoice;

    // 사진주소
    @Column(nullable = false)
    private String imgSrc;

    // 이름
    @Column(nullable = false)
    private String studentName;

    // 성별
    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private Gender gender;

    // 생년월일
    @Column(nullable = false)
    private LocalDate dateBirth;

    // 본인휴대폰
    @Column(nullable = false, length = 13)
    private String phoneNumber;

    // 집전화번호
    @Column(length = 13)
    private String homeNumber;

    // 주민등록번호
    @Column(nullable = false, length = 14)
    private String idNumber;

    // 주소
    @Column(nullable = false)
    private String address;

    // 이메일
    @Column(nullable = false)
    private String email;

    // 최종학력
    @Column(nullable = false)
    private String finalEducation;

    // 전공
    @Column(nullable = false)
    private String major;

    // 수강상태
    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private StudentStatus studentStatus;

    // 상태변경일
    private LocalDate dateStatusChange;

    // 사유 및 날짜
    private String statusWhyAndDate;

    // 아이디
    @Column(nullable = false)
    private String identity;

    // 비밀번호
    @Column(nullable = false)
    private String password;

    // 가입일
    @Column(nullable = false)
    private LocalDate dateJoin;

    public void putStudentInfoAdmin(SubjectChoice subjectChoice,StudentInfoChangeAdminRequest request){
        this.subjectChoice = subjectChoice;
        this.imgSrc = request.getImgSrc();
        this.studentName = request.getStudentName();
        this.gender = request.getGender();
        this.dateBirth = request.getDateBirth();
        this.phoneNumber = request.getPhoneNumber();
        this.homeNumber = request.getHomeNumber();
        this.idNumber = request.getIdNumber();
        this.address = request.getAddress();
        this.email = request.getEmail();
        this.finalEducation = request.getFinalEducation();
        this.major = request.getMajor();
        this.studentStatus = request.getStudentStatus();
        this.dateStatusChange = request.getDateStatusChange();
        this.statusWhyAndDate = request.getStatusWhyAndDate();
        this.identity = request.getIdentity();
        this.password = request.getPassword();
    }

    public void putStudentInfo(StudentInfoChangeRequest request){
        this.imgSrc = request.getImgSrc();
        this.phoneNumber = request.getPhoneNumber();
        this.homeNumber = request.getHomeNumber();
        this.address = request.getAddress();
        this.email = request.getEmail();
        this.finalEducation =request.getFinalEducation();
        this.major =request.getMajor();
        this.identity = request.getIdentity();
        this.password=request.getPassword();
    }

    private Student(Builder builder){
        this.subjectChoice = builder.subjectChoice;
        this.imgSrc = builder.imgSrc;
        this.studentName = builder.studentName;
        this.gender = builder.gender;
        this.dateBirth = builder.dateBirth;
        this.phoneNumber = builder.phoneNumber;
        this.homeNumber = builder.homeNumber;
        this.idNumber = builder.idNumber;
        this.address = builder.address;
        this.email = builder.email;
        this.finalEducation = builder.finalEducation;
        this.major = builder.major;
        this.studentStatus = builder.studentStatus;
        this.dateStatusChange = builder.dateStatusChange;
        this.statusWhyAndDate = builder.statusWhyAndDate;
        this.identity = builder.identity;
        this.password = builder.password;
        this.dateJoin = builder.dateJoin;
    }

    public static class Builder implements CommonModelBuilder<Student>{
        private final  SubjectChoice subjectChoice;
        private final  String imgSrc;
        private final String studentName;
        private final  Gender gender;
        private final  LocalDate dateBirth;
        private final  String phoneNumber;
        private final  String homeNumber;
        private final  String idNumber;
        private final  String address;
        private final  String email;
        private final  String finalEducation;
        private final  String major;
        private final StudentStatus studentStatus;
        private final LocalDate dateStatusChange;
        private final String statusWhyAndDate;
        private final String identity;
        private final String password;
        private final LocalDate dateJoin;

        public Builder(StudentRequest studentRequest , SubjectChoice subjectChoice){
            this.subjectChoice = subjectChoice;
            this.imgSrc = studentRequest.getImgSrc();
            this.studentName = studentRequest.getStudentName();
            this.gender = studentRequest.getGender();
            this.dateBirth = studentRequest.getDateBirth();
            this.phoneNumber = studentRequest.getPhoneNumber();
            this.homeNumber = studentRequest.getHomeNumber();
            this.idNumber = studentRequest.getIdNumber();
            this.address = studentRequest.getAddress();
            this.email = studentRequest.getEmail();
            this.finalEducation = studentRequest.getFinalEducation();
            this.major = studentRequest.getMajor();
            this.studentStatus = StudentStatus.INCLASS;
            this.dateStatusChange = LocalDate.now();
            this.statusWhyAndDate = "-";
            this.identity = studentRequest.getStudentName();
            this.password = subjectChoice.getSubjectName()+studentRequest.getStudentName();
            this.dateJoin = LocalDate.now();
        }
        @Override
        public Student build() {
            return new Student(this);
        }
    }
}
