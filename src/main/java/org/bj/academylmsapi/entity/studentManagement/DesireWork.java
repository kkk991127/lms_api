package org.bj.academylmsapi.entity.studentManagement;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.bj.academylmsapi.model.studentManagement.desireWork.DesireWorkRequest;

// 학생관리 - 취업관리 - 희망근무조건 entity
@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class DesireWork {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 수강생id - FK
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "studentId")
    private Student student;

    // 희망지역
    @Column(nullable = false)
    private String hopeArea;

    // 취업희망분야
    @Column(nullable = false)
    private String hopeWorkField;

    // 희망급여수준
    @Column(nullable = false)
    private String hopePayLevel;

    // 가능고용형태
    @Column(nullable = false)
    private String possibleEmployForm;

    // 가능근무형태
    @Column(nullable = false)
    private String possibleWorkForm;

    // 가능근무시간
    @Column(nullable = false)
    private String possibleWorkTime;

    // 수강생 요청사항
    @Column(nullable = false)
    private String studentRequest;

    public void putDesireWork(DesireWorkRequest request, Student student) {
        this.student = student;
        this.hopeArea = request.getHopeArea();
        this.hopeWorkField = request.getHopeWorkField();
        this.hopePayLevel = request.getHopePayLevel();
        this.possibleEmployForm = request.getPossibleEmployForm();
        this.possibleWorkForm = request.getPossibleWorkForm();
        this.possibleWorkTime = request.getPossibleWorkTime();
        this.studentRequest = request.getStudentRequest();
    }

    private DesireWork(Builder builder) {
        this.student = builder.student;
        this.hopeArea = builder.hopeArea;
        this.hopeWorkField = builder.hopeWorkField;
        this.hopePayLevel = builder.hopePayLevel;
        this.possibleEmployForm = builder.possibleEmployForm;
        this.possibleWorkForm = builder.possibleWorkForm;
        this.possibleWorkTime = builder.possibleWorkTime;
        this.studentRequest = builder.studentRequest;
    }

    public static class Builder implements CommonModelBuilder<DesireWork> {
        private final Student student;
        private final String hopeArea;
        private final String hopeWorkField;
        private final String hopePayLevel;
        private final String possibleEmployForm;
        private final String possibleWorkForm;
        private final String possibleWorkTime;
        private final String studentRequest;

        public Builder(DesireWorkRequest request, Student student) {
            this.student = student;
            this.hopeArea = request.getHopeArea();
            this.hopeWorkField = request.getHopeWorkField();
            this.hopePayLevel = request.getHopePayLevel();
            this.possibleEmployForm = request.getPossibleEmployForm();
            this.possibleWorkForm = request.getPossibleWorkForm();
            this.possibleWorkTime = request.getPossibleWorkTime();
            this.studentRequest = request.getStudentRequest();
        }


        @Override
        public DesireWork build() {
            return new DesireWork(this);
        }
    }
}
