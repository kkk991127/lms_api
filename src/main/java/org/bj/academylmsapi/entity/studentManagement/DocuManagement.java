package org.bj.academylmsapi.entity.studentManagement;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.bj.academylmsapi.model.studentManagement.docuManagement.DocuManagementRequest;

import java.time.LocalDateTime;

// 학생관리 - 서류관리 entity

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class DocuManagement {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 서류명
    @Column(nullable = false)
    private String documentName;

    // 서명 여부
    @Column(nullable = false)
    private Boolean isSign;

    // 등록일
    @Column(nullable = false)
    private LocalDateTime dateRegister;

    public void putDocuManagement(DocuManagementRequest request) {
        this.documentName = request.getDocumentName();
        this.isSign = request.getIsSign();
        this.dateRegister = LocalDateTime.now();
    }

    private DocuManagement(Builder builder) {
        this.documentName = builder.documentName;
        this.isSign = builder.isSign;
        this.dateRegister = builder.dateRegister;
    }

    public static class Builder implements CommonModelBuilder<DocuManagement> {
        private final String documentName;
        private final Boolean isSign;
        private final LocalDateTime dateRegister;

        public Builder(DocuManagementRequest request) {
            this.documentName = request.getDocumentName();
            this.isSign = request.getIsSign();
            this.dateRegister = LocalDateTime.now();
        }

        @Override
        public DocuManagement build() {
            return new DocuManagement(this);
        }
    }
}
