package org.bj.academylmsapi.entity.studentManagement;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.bj.academylmsapi.model.studentManagement.workManagement.WorkManagementRequest;

import java.time.LocalDate;
import java.time.LocalDateTime;

// 학생관리 - 취업관리 - 취업관리사항 entity
@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class WorkManagement {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 수강생id FK
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "studentId")
    private Student student;

    // 업체명
    @Column(nullable = false)
    private String companyName;

    // 사업자등록번호
    @Column(nullable = false)
    private String businessRegisterNumber;

    // 취업확인서
    @Column(nullable = false)
    private String employmentConfirmationForm;

    // 회사주소
    @Column(nullable = false)
    private String companyAddress;

    // 회사연락처
    @Column(nullable = false)
    private String companyCallNumber;

    // 근무직종
    @Column(nullable = false)
    private String workField;

    // 고용보험유무
    @Column(nullable = false)
    private String employmentInsuranceStatus;

    // 취업일
    @Column(nullable = false)
    private LocalDate dateEmployment;

    // 등록일
    @Column(nullable = false)
    private LocalDateTime dateRegister;

    public void putWorkManagement(WorkManagementRequest request, Student student) {
        this.student = student;
        this.companyName = request.getCompanyName();
        this.businessRegisterNumber = request.getBusinessRegisterNumber();
        this.employmentConfirmationForm = request.getEmploymentConfirmationForm();
        this.companyAddress = request.getCompanyAddress();
        this.companyCallNumber = request.getCompanyCallNumber();
        this.workField = request.getWorkField();
        this.employmentInsuranceStatus = request.getEmploymentInsuranceStatus();
        this.dateEmployment = request.getDateEmployment();
        this.dateRegister = LocalDateTime.now();
    }

    private WorkManagement(Builder builder) {
        this.student = builder.student;
        this.companyName = builder.companyName;
        this.businessRegisterNumber = builder.businessRegisterNumber;
        this.employmentConfirmationForm = builder.employmentConfirmationForm;
        this.companyAddress = builder.companyAddress;
        this.companyCallNumber = builder.companyCallNumber;
        this.workField = builder.workField;
        this.employmentInsuranceStatus = builder.employmentInsuranceStatus;
        this.dateEmployment = builder.dateEmployment;
        this.dateRegister = builder.dateRegister;
    }

    public static class Builder implements CommonModelBuilder<WorkManagement> {
        private final Student student;
        private final String companyName;
        private final String businessRegisterNumber;
        private final String employmentConfirmationForm;
        private final String companyAddress;
        private final String companyCallNumber;
        private final String workField;
        private final String employmentInsuranceStatus;
        private final LocalDate dateEmployment;
        private final LocalDateTime dateRegister;

        public Builder(WorkManagementRequest request, Student student) {
            this.student = student;
            this.companyName = request.getCompanyName();
            this.businessRegisterNumber = request.getBusinessRegisterNumber();
            this.employmentConfirmationForm = request.getEmploymentConfirmationForm();
            this.companyAddress = request.getCompanyAddress();
            this.companyCallNumber = request.getCompanyCallNumber();
            this.workField = request.getWorkField();
            this.employmentInsuranceStatus = request.getEmploymentInsuranceStatus();
            this.dateEmployment = request.getDateEmployment();
            this.dateRegister = LocalDateTime.now();
        }

        @Override
        public WorkManagement build() {
            return new WorkManagement(this);
        }
    }
}
