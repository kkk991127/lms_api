package org.bj.academylmsapi.entity.studentManagement;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.entity.Teacher;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.bj.academylmsapi.model.studentManagement.adviceManagement.AdviceManagementRequest;

import java.time.LocalDate;
import java.time.LocalDateTime;

// 학생관리 - 취업관리 - 상담관리 entity
@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AdviceManagement {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 강사id FK
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "teacherId")
    private Teacher teacher;

    // 수강생id FK
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "studentId")
    private Student student;

    // 수강생명
    @Column(nullable = false)
    private String studentName;

    // 상담날짜
    @Column(nullable = false)
    private LocalDate dateAdvice;

    // 상담내용
    @Column(nullable = false)
    private String adviceContent;

    // 등록일
    @Column(nullable = false)
    private LocalDateTime dateRegister;

    // 비고
    @Column(nullable = false)
    private String etc;

    public void putAdviceManagement(AdviceManagementRequest request, Teacher teacher, Student student) {
        this.teacher = teacher;
        this.student = student;
        this.studentName = request.getStudentName();
        this.dateAdvice = request.getDateAdvice();
        this.adviceContent = request.getAdviceContent();
        this.dateRegister = LocalDateTime.now();
        this.etc = request.getEtc();
    }

    private AdviceManagement(Builder builder) {
        this.teacher = builder.teacher;
        this.student = builder.student;
        this.studentName = builder.studentName;
        this.dateAdvice = builder.dateAdvice;
        this.adviceContent = builder.adviceContent;
        this.dateRegister = builder.dateRegister;
        this.etc = builder.etc;
    }

    public static class Builder implements CommonModelBuilder<AdviceManagement> {
        private final Teacher teacher;
        private final Student student;
        private final String studentName;
        private final LocalDate dateAdvice;
        private final String adviceContent;
        private final LocalDateTime dateRegister;
        private final String etc;

        public Builder(AdviceManagementRequest request, Teacher teacher, Student student) {
            this.teacher = teacher;
            this.student = student;
            this.studentName = request.getStudentName();
            this.dateAdvice = request.getDateAdvice();
            this.adviceContent = request.getAdviceContent();
            this.dateRegister = LocalDateTime.now();
            this.etc = request.getEtc();
        }

        @Override
        public AdviceManagement build() {
            return new AdviceManagement(this);
        }
    }
}
