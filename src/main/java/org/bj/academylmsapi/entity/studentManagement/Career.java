package org.bj.academylmsapi.entity.studentManagement;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.bj.academylmsapi.model.studentManagement.career.CareerRequest;

import java.time.LocalDateTime;

// 학생관리 - 취업관리 - 경력사항 entity
@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Career {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 수강생id FK
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "studentId")
    private Student student;

    // 근무처
    @Column(nullable = false)
    private String office;

    // 근무기간
    @Column(nullable = false)
    private String dateWork;

    // 직위
    @Column(nullable = false)
    private String position;

    // 담당업무
    @Column(nullable = false)
    private String whatTask;

    // 급여수준
    @Column(nullable = false)
    private String payLevel;

    // 보유자격증
    @Column(nullable = false)
    private String myLicense;

    // 고용지원금 대상자 여부
    @Column(nullable = false)
    private String employSupport;

    // 소속기관명
    @Column(nullable = false)
    private String belongOrganization;

    // 워크넷 취업활동 여부
    @Column(nullable = false)
    private String workNetJobSearchStatus;

    // 보유기술
    @Column(nullable = false)
    private String haveSkill;

    // 등록일
    @Column(nullable = false)
    private LocalDateTime dateRegister;

    // 기타특이사항
    @Column(nullable = false)
    private String etc;

    public void putCareer(CareerRequest request, Student student) {
        this.student = student;
        this.office = request.getOffice();
        this.dateWork = request.getDateWork();
        this.position = request.getPosition();
        this.whatTask = request.getWhatTask();
        this.payLevel = request.getPayLevel();
        this.myLicense = request.getMyLicense();
        this.employSupport = request.getEmploySupport();
        this.belongOrganization = request.getBelongOrganization();
        this.workNetJobSearchStatus = request.getWorkNetJobSearchStatus();
        this.haveSkill = request.getHaveSkill();
        this.dateRegister = LocalDateTime.now();
        this.etc = request.getEtc();
    }

    private Career(Builder builder) {
        this.student = builder.student;
        this.office = builder.office;
        this.dateWork = builder.dateWork;
        this.position = builder.position;
        this.whatTask = builder.whatTask;
        this.payLevel = builder.payLevel;
        this.myLicense = builder.myLicense;
        this.employSupport = builder.employSupport;
        this.belongOrganization = builder.belongOrganization;
        this.workNetJobSearchStatus = builder.workNetJobSearchStatus;
        this.haveSkill = builder.haveSkill;
        this.dateRegister = builder.dateRegister;
        this.etc = builder.etc;
    }

    public static class Builder implements CommonModelBuilder<Career> {
        private final Student student;
        private final String office;
        private final String dateWork;
        private final String position;
        private final String whatTask;
        private final String payLevel;
        private final String myLicense;
        private final String employSupport;
        private final String belongOrganization;
        private final String workNetJobSearchStatus;
        private final String haveSkill;
        private final LocalDateTime dateRegister;
        private final String etc;

        public Builder(CareerRequest request, Student student) {
            this.student = student;
            this.office = request.getOffice();
            this.dateWork = request.getDateWork();
            this.position = request.getPosition();
            this.whatTask = request.getWhatTask();
            this.payLevel = request.getPayLevel();
            this.myLicense = request.getMyLicense();
            this.employSupport = request.getEmploySupport();
            this.belongOrganization = request.getBelongOrganization();
            this.workNetJobSearchStatus = request.getWorkNetJobSearchStatus();
            this.haveSkill = request.getHaveSkill();
            this.dateRegister = LocalDateTime.now();
            this.etc = request.getEtc();
        }

        @Override
        public Career build() {
            return new Career(this);
        }
    }
}
