package org.bj.academylmsapi.entity.testManagement;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.bj.academylmsapi.model.Test.MakeTest.MakeTestRequest;


import java.time.LocalDateTime;

// 평가관리 - 시험출제
@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MakeTest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 과목현황id FK
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "subjectStatusId", nullable = false)
    private SubjectStatus subjectStatus;

    // 시험 문제
    @Column(nullable = false , columnDefinition = "TEXT")
    private String testProblem;

    // 객관식
    private String testSelect;

    // 등록일
    @Column(nullable = false)
    private LocalDateTime dateRegister;


    public void putMakeTest(SubjectStatus subjectStatus,MakeTestRequest request){
        this.subjectStatus = subjectStatus;
        this.testProblem = request.getTestProblem();
        this.testSelect = request.getTestSelect();

    }

    private MakeTest (Builder builder){

        this.subjectStatus = builder.subjectStatus;
        this.testProblem = builder.testProblem;
        this.testSelect = builder.testSelect;
        this.dateRegister = builder.dateRegister;
    }

    public static class Builder implements CommonModelBuilder<MakeTest>{
        private final SubjectStatus subjectStatus;
        private final String testProblem;
        private final String testSelect;
        private final LocalDateTime dateRegister;

        public Builder(SubjectStatus subjectStatus , MakeTestRequest request){
            this.subjectStatus = subjectStatus;
            this.testProblem = request.getTestProblem();
            this.testSelect = request.getTestSelect();
            this.dateRegister = LocalDateTime.now();
        }

        @Override
        public MakeTest build() {
            return new MakeTest(this);
        }
    }

}
