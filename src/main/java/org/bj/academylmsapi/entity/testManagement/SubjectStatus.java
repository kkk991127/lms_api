package org.bj.academylmsapi.entity.testManagement;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bj.academylmsapi.entity.SubjectChoice;
import org.bj.academylmsapi.entity.Teacher;
import org.bj.academylmsapi.enums.TestType;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.bj.academylmsapi.model.Test.SubjectStatus.SubjectStatusChangeRequest;
import org.bj.academylmsapi.model.Test.SubjectStatus.SubjectStatusRequest;

import java.time.LocalDate;
import java.time.LocalDateTime;

// 평가관리 - 과목현황
@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class SubjectStatus {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 과목명
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(nullable = false)
    private SubjectChoice subjectId;

    // 능력단위명
    @Column(nullable = false)
    private String abilityUnitName;

    // 능력단위요소명
    @Column(nullable = false)
    private String abilityUnitFactorName;

    // NCS
    @Column(nullable = false)
    private String NCS;

    // 강사명
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(nullable = false)
    private Teacher teacherId;

    // 평가명
    @Column(nullable = false)
    private String testName;

    // 평가일
    @Column(nullable = false)
    private LocalDate dateTest;

    // 평가시간(분)
    @Column(nullable = false)
    private Short testTime;

    // 평가유형
    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private TestType testType;

    // 출제여부
    @Column(nullable = false)
    private Boolean isSetTest;

    // 수준
    private Short level;

    // 총배점
    @Column(nullable = false)
    private Short score;

    // 문항수
    @Column(nullable = false)
    private Short problemNum;

    // 등록일
    @Column(nullable = false)
    private LocalDateTime dateRegister;

    public void putSubjectStatusChange(){
        this.isSetTest = true;
    }

    public void putSubjectStatus(SubjectChoice subjectChoice , Teacher teacher , SubjectStatusChangeRequest request){
        this.subjectId = subjectChoice;
        this.abilityUnitName = request.getAbilityUnitName();
        this.abilityUnitFactorName = request.getAbilityUnitFactorName();
        this.NCS = request.getNCS();
        this.teacherId = teacher;
        this.testName = request.getTestName();
        this.dateTest = request.getDateTest();
        this.testTime = request.getTestTime();
        this.testType = request.getTestType();
        this.level = request.getLevel();
        this.score = request.getScore();
        this.problemNum = request.getProblemNum();
        this.dateRegister = LocalDateTime.now();
    }


    private SubjectStatus(Builder builder) {
        this.subjectId = builder.subjectChoice;
        this.abilityUnitName = builder.abilityUnitName;
        this.abilityUnitFactorName = builder.abilityUnitFactorName;
        this.NCS = builder.NCS;
        this.teacherId = builder.teacherId;
        this.testName = builder.testName;
        this.dateTest = builder.dateTest;
        this.testTime = builder.testTime;
        this.testType = builder.testType;
        this.isSetTest = builder.isSetTest;
        this.level = builder.level;
        this.score = builder.score;
        this.problemNum = builder.problemNum;
        this.dateRegister = builder.dateRegister;
    }

    public static class Builder implements CommonModelBuilder<SubjectStatus> {
        private final SubjectChoice subjectChoice;
        private final String abilityUnitName;
        private final String abilityUnitFactorName;
        private final String NCS;
        private final Teacher teacherId;
        private final String testName;
        private final LocalDate dateTest;
        private final Short testTime;
        private final TestType testType;
        private final Boolean isSetTest;
        private final Short level;
        private final Short score;
        private final Short problemNum;
        private final LocalDateTime dateRegister;

        public Builder(SubjectChoice subjectChoice, Teacher teacher, SubjectStatusRequest request) {
            this.subjectChoice = subjectChoice;
            this.abilityUnitName = request.getAbilityUnitName();
            this.abilityUnitFactorName = request.getAbilityUnitFactorName();
            this.NCS = request.getNCS();
            this.teacherId = teacher;
            this.testName = request.getTestName();
            this.dateTest = request.getDateTest();
            this.testTime = request.getTestTime();
            this.testType = request.getTestType();
            this.isSetTest = false;
            this.level = request.getLevel();
            this.score = request.getScore();
            this.problemNum = request.getProblemNum();
            this.dateRegister = LocalDateTime.now();
        }

        @Override
        public SubjectStatus build() {
            return new SubjectStatus(this);
        }
    }
}
