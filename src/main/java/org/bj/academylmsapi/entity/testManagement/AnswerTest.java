package org.bj.academylmsapi.entity.testManagement;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.enums.StudentScore;
import org.bj.academylmsapi.enums.TestStatus;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.bj.academylmsapi.model.Test.Answer.AnswerPutScoreRequest;
import org.bj.academylmsapi.model.Test.Answer.AnswerPutStudentSign;
import org.bj.academylmsapi.model.Test.Answer.AnswerTestRequest;

import java.time.LocalDateTime;

// 평가관리 - 시험채점
@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AnswerTest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 시험출제id FK
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "makeTestId", nullable = false)
    private MakeTest makeTest;

    // 수강생id FK
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "studentId")
    private Student student;

    // 답변
    @Column(nullable = false, columnDefinition = "TEXT")
    private String testAnswer;

    @Column
    private String addFile;

    // 수강생서명
    private String studentSign;

    // 교수자총평
    private String teacherComment;

    // 평가점수
    private Short studentScore;

    //평가현황
    @Enumerated(value = EnumType.STRING)
    private TestStatus testStatus;

    // 성취수준
    private Short achieveLevel;



    // 등록일
    @Column(nullable = false)
    private LocalDateTime dateRegister;

    public void AnswerTestSign(AnswerPutStudentSign request){
        this.studentSign = request.getStudentSign();
        this.testStatus = TestStatus.SIGNOKAY;
    }

    public void getScore(AnswerPutScoreRequest request){
        this.studentScore = request.getStudentScore();
        this.teacherComment = request.getTeacherComment();
        this.achieveLevel = request.getAchieveLevel();
    }


    private AnswerTest(Builder builder) {
        this.makeTest = builder.makeTest;
        this.student = builder.student;
        this.testAnswer = builder.testAnswer;
        this.addFile = builder.addFile;
        this.testStatus = builder.testStatus;
        this.dateRegister = builder.dateRegister;
    }

    public static class Builder implements CommonModelBuilder<AnswerTest> {
        private final MakeTest makeTest;
        private final Student student;
        private final String testAnswer;
        private final String addFile;
        private TestStatus testStatus;
        private final LocalDateTime dateRegister;

        public Builder(MakeTest makeTest, Student student, AnswerTestRequest request) {
            this.makeTest = makeTest;
            this.student = student;
            this.testAnswer = request.getTestAnswer();
            this.addFile = request.getAddFile();
            this.testStatus = TestStatus.PUTANSWER;
            this.dateRegister = LocalDateTime.now();
        }
        @Override
        public AnswerTest build() {
            return new AnswerTest(this);
        }
    }

}
