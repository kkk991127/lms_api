package org.bj.academylmsapi.entity;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bj.academylmsapi.enums.Gender;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.bj.academylmsapi.model.admin.AdminInfoChangeRequest;
import org.bj.academylmsapi.model.admin.AdminRequest;

import java.time.LocalDate;
import java.time.LocalDateTime;

// 관리자 entity

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Admin {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 관리자이름
    @Column(nullable = false, length = 20)
    private String adminName;

    // 성별
    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private Gender gender;

    // 생년월일
    @Column(nullable = false)
    private LocalDate dateBirth;

    // 본인 휴대폰
    @Column(nullable = false, length = 13)
    private String phoneNumber;

    // 집 전화번호
    @Column(nullable = false, length = 13)
    private String homeNumber;

    // 주민등록번호
    @Column(nullable = false, length = 14)
    private String idNumber;

    // 주소
    @Column(nullable = false)
    private String address;

    // 사진주소
    @Column(nullable = false)
    private String imgSrc;

    // 가입일
    @Column(nullable = false)
    private LocalDateTime dateJoin;

    public void putAdmin(AdminInfoChangeRequest request) {
        this.adminName = request.getAdminName();
        this.gender = request.getGender();
        this.dateBirth = request.getDateBirth();
        this.phoneNumber = request.getPhoneNumber();
        this.homeNumber = request.getHomeNumber();
        this.idNumber = request.getIdNumber();
        this.address = request.getAddress();
        this.imgSrc = request.getImgSrc();
    }

    private Admin(Builder builder) {
        this.adminName = builder.adminName;
        this.gender = builder.gender;
        this.dateBirth = builder.dateBirth;
        this.phoneNumber = builder.phoneNumber;
        this.homeNumber = builder.homeNumber;
        this.idNumber = builder.idNumber;
        this.address = builder.address;
        this.imgSrc = builder.imgSrc;
        this.dateJoin = builder.dateJoin;
    }

    public static class Builder implements CommonModelBuilder<Admin> {
        private final String adminName;
        private final Gender gender;
        private final LocalDate dateBirth;
        private final String phoneNumber;
        private final String homeNumber;
        private final String idNumber;
        private final String address;
        private final String imgSrc;
        private final LocalDateTime dateJoin;

        public Builder(AdminRequest request) {
            this.adminName = request.getAdminName();
            this.gender = request.getGender();
            this.dateBirth = request.getDateBirth();
            this.phoneNumber = request.getPhoneNumber();
            this.homeNumber = request.getHomeNumber();
            this.idNumber = request.getIdNumber();
            this.address = request.getAddress();
            this.imgSrc = request.getImgSrc();
            this.dateJoin = request.getDateJoin();
        }

        @Override
        public Admin build() {
            return new Admin(this);
        }
    }
}
