package org.bj.academylmsapi.entity.studentAttendance;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.bj.academylmsapi.model.studentAttendance.studentAttendanceHistory.StudentAttendanceHistoryRequest;

import java.time.LocalDateTime;

// 수강생 출결 내역 entity

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class StudentAttendanceHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 수강생 id FK
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "studentId")
    private Student student;

    // 출석
    @Column(nullable = false)
    private Short studentAttendance;

    // 조퇴
    @Column(nullable = false)
    private Short studentOut;

    // 지각
    @Column(nullable = false)
    private Short studentLate;

    // 무단 결석
    @Column(nullable = false)
    private Short studentAbsent;

    // 병가
    @Column(nullable = false)
    private Short studentSick;

    // 등록일
    @Column(nullable = false)
    private LocalDateTime dateRegister;

    public void putStudentAttendanceHistory(StudentAttendanceHistoryRequest request, Student student) {
        this.student = student;
        this.studentAttendance = request.getStudentAttendance();
        this.studentOut = request.getStudentOut();
        this.studentLate = request.getStudentLate();
        this.studentAbsent = request.getStudentAbsent();
        this.studentSick = request.getStudentSick();
        this.dateRegister = request.getDateRegister();
    }

    private StudentAttendanceHistory(Builder builder) {
        this.student = builder.student;
        this.studentAttendance = builder.studentAttendance;
        this.studentOut = builder.studentOut;
        this.studentLate = builder.studentLate;
        this.studentAbsent = builder.studentAbsent;
        this.studentSick = builder.studentSick;
        this.dateRegister = builder.dateRegister;
    }

    public static class Builder implements CommonModelBuilder<StudentAttendanceHistory> {
        private final Student student;
        private final Short studentAttendance;
        private final Short studentOut;
        private final Short studentLate;
        private final Short studentAbsent;
        private final Short studentSick;
        private final LocalDateTime dateRegister;

        public Builder(StudentAttendanceHistoryRequest request, Student student) {
            this.student = student;
            this.studentAttendance = request.getStudentAttendance();
            this.studentOut = request.getStudentOut();
            this.studentLate = request.getStudentLate();
            this.studentAbsent = request.getStudentAbsent();
            this.studentSick = request.getStudentSick();
            this.dateRegister = LocalDateTime.now();
        }

        @Override
        public StudentAttendanceHistory build() {
            return new StudentAttendanceHistory(this);
        }
    }
}
