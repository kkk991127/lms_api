package org.bj.academylmsapi.entity.studentAttendance;


import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.enums.ApprovalState;
import org.bj.academylmsapi.enums.DemandType;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.bj.academylmsapi.model.studentAttendance.studentAttendanceDemand.StudentAttendanceDemandRequest;

import java.time.LocalDate;
import java.time.LocalDateTime;

//  수강생 출결 신청 entity

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class StudentAttendanceDemand {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 수강생 id FK
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "studentId")
    private Student student;

    // 기준일
    @Column(nullable = false)
    private LocalDate dateThat;

    // 유형
    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private DemandType demandType;

    // 승인 상태
    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private ApprovalState approvalState;

    // 첨부파일 주소
    private String addFile;

    // 비고
    private String etc;

    // 등록일
    @Column(nullable = false)
    private LocalDateTime dateRegister;

    public void putStudentAttendanceDemand(StudentAttendanceDemandRequest request, Student student) {
        this.student = student;
        this.dateThat = request.getDateThat();
        this.demandType = request.getDemandType();
        this.approvalState = request.getApprovalState();
        this.addFile = request.getAddFile();
        this.etc = request.getEtc();
        this.dateRegister = request.getDateRegister();
    }

    private StudentAttendanceDemand(Builder builder) {
        this.student = builder.student;
        this.dateThat = builder.dateThat;
        this.demandType = builder.demandType;
        this.approvalState = builder.approvalState;
        this.addFile = builder.addFile;
        this.etc = builder.etc;
        this.dateRegister = builder.dateRegister;
    }

    public static class Builder implements CommonModelBuilder<StudentAttendanceDemand> {
        private final Student student;
        private final LocalDate dateThat;
        private final DemandType demandType;
        private final ApprovalState approvalState;
        private final String addFile;
        private final String etc;
        private final LocalDateTime dateRegister;

        public Builder(StudentAttendanceDemandRequest request, Student student) {
            this.student = student;
            this.dateThat = request.getDateThat();
            this.demandType = request.getDemandType();
            this.approvalState = request.getApprovalState();
            this.addFile = request.getAddFile();
            this.etc = request.getEtc();
            this.dateRegister = LocalDateTime.now();
        }

        @Override
        public StudentAttendanceDemand build() {
            return new StudentAttendanceDemand(this);
        }
    }
}
