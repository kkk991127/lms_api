package org.bj.academylmsapi.entity.studentAttendance;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.bj.academylmsapi.model.studentAttendance.studentAttendance.StudentAttendanceRequest;

import java.time.LocalDateTime;

// 수강생 출결 entity

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class StudentAttendance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 수강생id FK
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "studentId")
    private Student student;

    // 출석
    @Column(nullable = false)
    private Short studentAttendence;

    // 결석
    @Column(nullable = false)
    private Short studentAbsence;

    // 조퇴
    @Column(nullable = false)
    private Short studentOut;

    // 지각
    @Column(nullable = false)
    private Short studentLate;

    // 무단결석
    @Column(nullable = false)
    private Short studentSelfAbsence;

    // 등록일
    @Column(nullable = false)
    private LocalDateTime dateRegister;

    public void putStudentAttendance(StudentAttendanceRequest request, Student student) {
        this.student = student;
        this.studentAttendence = request.getStudentAttendence();
        this.studentAbsence = request.getStudentAbsence();
        this.studentOut = request.getStudentOut();
        this.studentLate = request.getStudentLate();
        this.studentSelfAbsence = request.getStudentSelfAbsence();
    }

    private StudentAttendance(Builder builder) {
        this.student = builder.student;
        this.studentAttendence = builder.studentAttendence;
        this.studentAbsence = builder.studentAbsence;
        this.studentOut = builder.studentOut;
        this.studentLate = builder.studentLate;
        this.studentSelfAbsence = builder.studentSelfAbsence;
        this.dateRegister = builder.dateRegister;
    }

    public static class Builder implements CommonModelBuilder<StudentAttendance> {
        private final Student student;
        private final Short studentAttendence;
        private final Short studentAbsence;
        private final Short studentOut;
        private final Short studentLate;
        private final Short studentSelfAbsence;
        private final LocalDateTime dateRegister;

        public Builder(StudentAttendanceRequest request, Student student) {
            this.student = student;
            this.studentAttendence = request.getStudentAttendence();
            this.studentAbsence = request.getStudentAbsence();
            this.studentOut = request.getStudentOut();
            this.studentLate = request.getStudentLate();
            this.studentSelfAbsence = request.getStudentSelfAbsence();
            this.dateRegister = LocalDateTime.now();
        }

        @Override
        public StudentAttendance build() {
            return new StudentAttendance(this);
        }
    }
}
