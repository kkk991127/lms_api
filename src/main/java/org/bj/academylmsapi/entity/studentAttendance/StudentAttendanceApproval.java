package org.bj.academylmsapi.entity.studentAttendance;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.enums.ApprovalState;
import org.bj.academylmsapi.enums.AttendanceType;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.bj.academylmsapi.model.studentAttendance.studentAttendanceApproval.StudentAttendanceApprovalRequest;

import java.time.LocalDate;
import java.time.LocalDateTime;

// 수강생 출결 승인 entity

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class StudentAttendanceApproval {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 수강생 id FK
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "studentId")
    private Student student;

    // 출결유형
    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private AttendanceType attendanceType;

    // 사유
    @Column(nullable = false)
    private String reason;

    // 예정일
    @Column(nullable = false)
    private LocalDate datePlan;

    // 승인상태
    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private ApprovalState approvalState;

    // 등록일
    @Column(nullable = false)
    private LocalDateTime dateRegister;

    public void putStudentAttendanceApproval(StudentAttendanceApprovalRequest request, Student student) {
        this.student = student;
        this.attendanceType = request.getAttendanceType();
        this.reason = request.getReason();
        this.datePlan = request.getDatePlan();
        this.approvalState = request.getApprovalState();
        this.dateRegister = request.getDateRegister();
    }

    private StudentAttendanceApproval(Builder builder) {
        this.student = builder.student;
        this.attendanceType = builder.attendanceType;
        this.reason = builder.reason;
        this.datePlan = builder.datePlan;
        this.approvalState = builder.approvalState;
        this.dateRegister = builder.dateRegister;
    }

    public static class Builder implements CommonModelBuilder<StudentAttendanceApproval> {
        private final Student student;
        private final AttendanceType attendanceType;
        private final String reason;
        private final LocalDate datePlan;
        private final ApprovalState approvalState;
        private final LocalDateTime dateRegister;

        public Builder(StudentAttendanceApprovalRequest request, Student student) {
            this.student = student;
            this.attendanceType = request.getAttendanceType();
            this.reason = request.getReason();
            this.datePlan = request.getDatePlan();
            this.approvalState = request.getApprovalState();
            this.dateRegister = LocalDateTime.now();
        }

        @Override
        public StudentAttendanceApproval build() {
            return new StudentAttendanceApproval(this);
        }
    }

}
