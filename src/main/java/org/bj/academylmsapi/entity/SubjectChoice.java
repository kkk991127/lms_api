package org.bj.academylmsapi.entity;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.enums.TrainingType;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.bj.academylmsapi.model.subjectChoice.SubjectChoiceRequest;

import java.time.LocalDate;

// 과정 entity

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class SubjectChoice {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    // 강사id FK
    // 강좌 먼저 등록되고 나중에 강사배치라서 nullable = true
    @ManyToOne(fetch = FetchType.EAGER)
    private Teacher teacher;
    // 과정명
    @Column(nullable = false)
    private String subjectName;
    // 교육 시작일
    @Column(nullable = false)
    private LocalDate dateStart;
    // 교육 종료일
    @Column(nullable = false)
    private LocalDate dateEnd;
    // 훈련유형
    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private TrainingType trainingType;
    // 회차
    @Column(nullable = false)
    private Short classTurn;
    // 재적인원
    @Column(nullable = false)
    private Short registerHuman;
    // 총 인원수
    @Column(nullable = false)
    private Short totalHuman;
    // ncs 수준
    @Column(nullable = false)
    private Short ncsLevel;
    //과정 등록일
    @Column(nullable = false)
    private LocalDate dateRegister;

    public void putSubject(Teacher teacher, SubjectChoiceRequest request){
        this.teacher = teacher;
        this.subjectName = request.getSubjectName();
        this.dateStart = request.getDateStart();
        this.dateEnd = request.getDateEnd();
        this.trainingType = request.getTrainingType();
        this.classTurn = request.getClassTurn();
        this.registerHuman = request.getRegisterHuman();
        this.totalHuman = request.getTotalHuman();
        this.ncsLevel = request.getNcsLevel();
        this.dateRegister = LocalDate.now();
    }

    private SubjectChoice(Builder builder) {
        this.teacher = builder.teacher;
        this.subjectName = builder.subjectName;
        this.dateStart = builder.dateStart;
        this.dateEnd = builder.dateEnd;
        this.trainingType = builder.trainingType;
        this.classTurn = builder.classTurn;
        this.registerHuman = builder.registerHuman;
        this.totalHuman = builder.totalHuman;
        this.ncsLevel = builder.ncsLevel;
        this.dateRegister = builder.dateRegister;
    }

    public static class Builder implements CommonModelBuilder<SubjectChoice> {
        private final Teacher teacher;
        private final String subjectName;
        private final LocalDate dateStart;
        private final LocalDate dateEnd;
        private final TrainingType trainingType;
        private final Short classTurn;
        private final Short registerHuman;
        private final Short totalHuman;
        private final Short ncsLevel;
        private final LocalDate dateRegister;

        public Builder(SubjectChoiceRequest request, Teacher teacher) {
            this.teacher = teacher;
            this.subjectName = request.getSubjectName();
            this.dateStart = request.getDateStart();
            this.dateEnd = request.getDateEnd();
            this.trainingType = request.getTrainingType();
            this.classTurn = request.getClassTurn();
            this.registerHuman = request.getRegisterHuman();
            this.totalHuman = request.getTotalHuman();
            this.ncsLevel = request.getNcsLevel();
            this.dateRegister = LocalDate.now();
        }

        @Override
        public SubjectChoice build() {
            return new SubjectChoice(this);
        }
    }

}
