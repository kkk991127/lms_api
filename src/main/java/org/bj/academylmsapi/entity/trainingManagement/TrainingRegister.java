package org.bj.academylmsapi.entity.trainingManagement;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.entity.Teacher;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.bj.academylmsapi.model.trainingManagement.trainingRegister.TrainingRegisterRequest;
import org.springframework.data.domain.PageRequest;

import java.time.LocalDateTime;

// 훈련관리 - 훈련일지 등록 entity

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class TrainingRegister {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 수강생id FK
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "studentId")
    private Student student;

    // 강사id FK
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "teacherId")
    private Teacher teacher;

    // 훈련날짜
    @Column(nullable = false)
    private LocalDateTime dateTraining;

    // 교시
    private Short period;

    // 재적
    @Column(nullable = false)
    private Short registrationNum;

    // 출석
    private Short attendanceNum;

    // 결석
    private String absentNum;

    // 지각
    private String lateNum;

    // 조퇴
    private String earlyLeave;

    // 훈련과목
    private String trainingSubject;

    // 담당교사
    @Column(nullable = false)
    private String mainTeacher;

    // 훈련내용
    private String trainingContent;

    // 등록일
    @Column(nullable = false)
    private LocalDateTime dateRegister;

    public void putTrainingRegister(TrainingRegisterRequest request, Student student, Teacher teacher) {
        this.student = student;
        this.teacher = teacher;
        this.dateTraining = request.getDateTraining();
        this.period = request.getPeriod();
        this.registrationNum = request.getRegistrationNum();
        this.attendanceNum = request.getAttendanceNum();
        this.absentNum = request.getAbsentNum();
        this.lateNum = request.getLateNum();
        this.earlyLeave = request.getEarlyLeave();
        this.trainingSubject = request.getTrainingSubject();
        this.mainTeacher = request.getMainTeacher();
        this.trainingContent = request.getTrainingContent();
    }

    private TrainingRegister(Builder builder) {
        this.student = builder.student;
        this.teacher = builder.teacher;
        this.dateTraining = builder.dateTraining;
        this.period = builder.period;
        this.registrationNum = builder.registrationNum;
        this.attendanceNum = builder.attendanceNum;
        this.absentNum = builder.absentNum;
        this.lateNum = builder.lateNum;
        this.earlyLeave = builder.earlyLeave;
        this.trainingSubject = builder.trainingSubject;
        this.mainTeacher = builder.mainTeacher;
        this.trainingContent = builder.trainingContent;
        this.dateRegister = builder.dateRegister;
    }

    public static class Builder implements CommonModelBuilder<TrainingRegister> {
        private final Student student;
        private final Teacher teacher;
        private final LocalDateTime dateTraining;
        private final Short period;
        private final Short registrationNum;
        private final Short attendanceNum;
        private final String absentNum;
        private final String lateNum;
        private final String earlyLeave;
        private final String trainingSubject;
        private final String mainTeacher;
        private final String trainingContent;
        private final LocalDateTime dateRegister;

        public Builder(TrainingRegisterRequest request, Student student, Teacher teacher) {
            this.student = student;
            this.teacher = teacher;
            this.dateTraining = request.getDateTraining();
            this.period = request.getPeriod();
            this.registrationNum = request.getRegistrationNum();
            this.attendanceNum = request.getAttendanceNum();
            this.absentNum = request.getAbsentNum();
            this.lateNum = request.getLateNum();
            this.earlyLeave = request.getEarlyLeave();
            this.trainingSubject = request.getTrainingSubject();
            this.mainTeacher = request.getMainTeacher();
            this.trainingContent = request.getTrainingContent();
            this.dateRegister = LocalDateTime.now();
        }

        @Override
        public TrainingRegister build() {
            return new TrainingRegister(this);
        }
    }
}
