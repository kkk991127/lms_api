package org.bj.academylmsapi.entity.trainingManagement;


import ch.qos.logback.core.pattern.color.BoldYellowCompositeConverter;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.bj.academylmsapi.model.trainingManagement.trainingAttendance.TrainingAttendanceRequest;

import java.time.LocalDate;
import java.time.LocalDateTime;

// 훈련관리 - 훈련일지 출결

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class TrainingAttendance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 이론
    @Column(nullable = false)
    private Short theory;

    // 실습
    @Column(nullable = false)
    private Short practice;

    // 작성일
    @Column(nullable = false)
    private LocalDate dateAbsent;

    // 결석자
    private String whoAbsent;

    // 지각자
    private String whoLate;

    // 조퇴자
    private String whoLeave;

    // 기타사항
    private String etc;

    // 등록일
    @Column(nullable = false)
    private LocalDateTime dateRegister;

    public void putTrainingAttendance(TrainingAttendanceRequest request) {
        this.theory = request.getTheory();
        this.practice = request.getPractice();
        this.dateAbsent = request.getDateAbsent();
        this.whoAbsent = request.getWhoAbsent();
        this.whoLate = request.getWhoLate();
        this.whoLeave = request.getWhoLeave();
        this.etc = request.getEtc();
    }

    private TrainingAttendance(Builder builder) {
        this.theory = builder.theory;
        this.practice = builder.practice;
        this.dateAbsent = builder.dateAbsent;
        this.whoAbsent = builder.whoAbsent;
        this.whoLate = builder.whoLate;
        this.whoLeave = builder.whoLeave;
        this.etc = builder.etc;
        this.dateRegister = builder.dateRegister;
    }

    public static class Builder implements CommonModelBuilder<TrainingAttendance> {
        private final Short theory;
        private final Short practice;
        private final LocalDate dateAbsent;
        private final String whoAbsent;
        private final String whoLate;
        private final String whoLeave;
        private final String etc;
        private final LocalDateTime dateRegister;

        public Builder(TrainingAttendanceRequest request) {
            this.theory = request.getTheory();
            this.practice = request.getPractice();
            this.dateAbsent = request.getDateAbsent();
            this.whoAbsent = request.getWhoAbsent();
            this.whoLate = request.getWhoLate();
            this.whoLeave = request.getWhoLeave();
            this.etc = request.getEtc();
            this.dateRegister = LocalDateTime.now();
        }

        @Override
        public TrainingAttendance build() {
            return new TrainingAttendance(this);
        }
    }
}
