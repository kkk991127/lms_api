package org.bj.academylmsapi.entity;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.enums.MemberGroup;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.bj.academylmsapi.model.academy.AcademyInfoChangeRequest;
import org.bj.academylmsapi.model.academy.AcademyRequest;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Collections;

// 학원 entity

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
// implements - 구현하다 .
public class Academy implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 학원명
    @Column(nullable = false)
    private String academyName;

    @Column(nullable = false)
    private String username;

    @Column(nullable = false)
    private String password;

    // 학원주소
    @Column(nullable = false)
    private String address;

    // 업종
    @Column(nullable = false)
    private String businessType;

    // 사업내용
    @Column(nullable = false)
    private String businessContent;

    // 대표자명
    @Column(nullable = false)
    private String representativeName;

    // 대표전화번호
    @Column(nullable = false)
    private String academyCallNumber;

    // 등록일
    @Column(nullable = false)
    private LocalDateTime dateRegister;

    @Enumerated
    @Column(nullable = false)
    private MemberGroup memberGroup;



    public void putAcademy(AcademyInfoChangeRequest request) {
        this.academyName = request.getAcademyName();
        this.username = request.getUserName();
        this.address = request.getAddress();
        this.businessType = request.getBusinessType();
        this.businessContent = request.getBusinessContent();
        this.representativeName = request.getRepresentativeName();
        this.academyCallNumber = request.getAcademyCallNumber();
    }

    private Academy(Builder builder) {
        this.academyName = builder.academyName;
        this.address = builder.address;
        this.businessType = builder.businessType;
        this.businessContent = builder.businessContent;
        this.representativeName = builder.representativeName;
        this.academyCallNumber = builder.academyCallNumber;
        this.dateRegister = builder.dateRegister;
        this.username = builder.username;
        this.password = builder.password;
        this.memberGroup = builder.memberGroup;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singleton(new SimpleGrantedAuthority(memberGroup.toString()));
    } // memberGroup을 String 으로 바꿔줌 ..

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    } // 자격증명이 안된

    @Override
    public boolean isEnabled() {
        return true;
    }

    // 여기가 먼
    public static class Builder implements CommonModelBuilder<Academy> {
        private final String academyName;
        private final String address;
        private final String businessType;
        private final String businessContent;
        private final String representativeName;
        private final String academyCallNumber;
        private final LocalDateTime dateRegister;
        private final String username;
        private final String password;
        private final MemberGroup memberGroup;

        public Builder(AcademyRequest request) {
            this.academyName = request.getAcademyName();
            this.address = request.getAddress();
            this.businessType = request.getBusinessType();
            this.businessContent = request.getBusinessContent();
            this.representativeName = request.getRepresentativeName();
            this.academyCallNumber = request.getAcademyCallNumber();
            this.dateRegister = LocalDateTime.now();
            this.username = request.getUsername();
            this.password = request.getPassword();
            this.memberGroup = request.getMemberGroup();
        }

        @Override
        public Academy build() {
            return new Academy(this);
        }
    }
}
