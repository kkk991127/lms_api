package org.bj.academylmsapi.entity;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.enums.MemberGroup;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.bj.academylmsapi.model.MemberRequest;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Member implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Enumerated
    @Column(nullable = false)
    private MemberGroup memberGroup;

    @Column(nullable = false)
    private String username;

    @Column(nullable = false)
    private String password;

    private Member(Builder builder){
        this.memberGroup = builder.memberGroup;
        this.username = builder.username;
        this.password = builder.password;
    }
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singleton(new SimpleGrantedAuthority(memberGroup.toString()));
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public String getUsername() {
        return null;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public static class Builder implements CommonModelBuilder<Member> {
        private final MemberGroup memberGroup;

        private final String username;

        private final String password;

        public Builder(MemberRequest request) {
            this.memberGroup = request.getMemberGroup();
            this.username = request.getUsername();
            this.password = request.getPassword();
        }


        @Override
        public Member build() {
            return new Member(this);
        }
    }
}
