package org.bj.academylmsapi.entity.board;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.entity.Teacher;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import org.bj.academylmsapi.model.board.reply.ReplyRequest;

import java.time.LocalDate;
import java.time.LocalDateTime;

// 게시판 - 답변 entity
@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Reply {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 게시판id FK
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "boardId")
    private Board board;

    // 강사id FK
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "teacherId")
    private Teacher teacher;

    // 수강생id FK
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "studentId")
    private Student student;

    // 제목
    @Column(nullable = false)
    private String boardTitle;

    // 답변 제목
    @Column(nullable = false)
    private String boardReTitle;

    // 작성자
    @Column(nullable = false)
    private String boardWriter;

    // 내용
    @Column(nullable = false)
    private String boardContent;

    // 날짜
    @Column(nullable = false)
    private LocalDateTime dateCreate;

    public void putReply(ReplyRequest request, Board board, Teacher teacher, Student student) {
        this.board = board;
        this.teacher = teacher;
        this.student = student;
        this.boardTitle = request.getBoardTitle();
        this.boardReTitle = request.getBoardReTitle();
        this.boardWriter = request.getBoardWriter();
        this.boardContent = request.getBoardContent();
    }

    private Reply(Builder builder) {
        this.board = builder.board;
        this.teacher = builder.teacher;
        this.student = builder.student;
        this.boardTitle = builder.boardTitle;
        this.boardReTitle = builder.boardReTitle;
        this.boardWriter = builder.boardWriter;
        this.boardContent = builder.boardContent;
        this.dateCreate = builder.dateCreate;
    }

    public static class Builder implements CommonModelBuilder<Reply> {
        private final Board board;
        private final Teacher teacher;
        private final Student student;
        private final String boardTitle;
        private final String boardReTitle;
        private final String boardWriter;
        private final String boardContent;
        private final LocalDateTime dateCreate;

        public Builder(ReplyRequest request, Board board, Teacher teacher, Student student) {
            this.board = board;
            this.teacher = teacher;
            this.student = student;
            this.boardTitle = request.getBoardTitle();
            this.boardReTitle = request.getBoardReTitle();
            this.boardWriter = request.getBoardWriter();
            this.boardContent = request.getBoardContent();
            this.dateCreate = LocalDateTime.now();
        }

        @Override
        public Reply build() {
            return new Reply(this);
        }
    }
}
