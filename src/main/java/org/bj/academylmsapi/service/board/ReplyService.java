package org.bj.academylmsapi.service.board;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.entity.Teacher;
import org.bj.academylmsapi.entity.board.Board;
import org.bj.academylmsapi.entity.board.Reply;
import org.bj.academylmsapi.enums.ResultCode;
import org.bj.academylmsapi.model.board.reply.ReplyRequest;
import org.bj.academylmsapi.model.board.reply.ReplyResponse;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.repository.StudentRepository;
import org.bj.academylmsapi.repository.TeacherRepository;
import org.bj.academylmsapi.repository.board.BoardRepository;
import org.bj.academylmsapi.repository.board.ReplyRepository;
import org.springframework.stereotype.Service;

// 게시판 - 답변 service

@Service
@RequiredArgsConstructor
public class ReplyService {
    private final ReplyRepository replyRepository;
    private final BoardRepository boardRepository;
    private final TeacherRepository teacherRepository;
    private final StudentRepository studentRepository;

    // 답변 C
    public void setReply(ReplyRequest request) {
        Board boardData = boardRepository.findById(request.getBoardId()).orElseThrow();
        Teacher teacherData = teacherRepository.findById(request.getTeacherId()).orElseThrow();
        Student studentData = studentRepository.findById(request.getStudentId()).orElseThrow();
        replyRepository.save(new Reply.Builder(request, boardData, teacherData, studentData).build());
    }

    // 답변 단수 R
    public ReplyResponse getReply(long id) {
        Reply originData = replyRepository.findById(id).orElseThrow();
        return new ReplyResponse.Builder(originData).build();
    }

    // 답변 U
    public void putReply(long id, ReplyRequest request) {
        Reply originData = replyRepository.findById(id).orElseThrow();
        Board boardData = boardRepository.findById(request.getBoardId()).orElseThrow();
        Teacher teacherData = teacherRepository.findById(request.getTeacherId()).orElseThrow();
        Student studentData = studentRepository.findById(request.getStudentId()).orElseThrow();
        originData.putReply(request, boardData, teacherData, studentData);
        replyRepository.save(originData);
    }

    // 답변 D
    public void delReply(long id) {
        replyRepository.deleteById(id);
    }
}
