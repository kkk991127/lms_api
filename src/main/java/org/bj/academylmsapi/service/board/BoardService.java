package org.bj.academylmsapi.service.board;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.entity.Teacher;
import org.bj.academylmsapi.entity.board.Board;
import org.bj.academylmsapi.enums.ResultCode;
import org.bj.academylmsapi.model.board.board.BoardRequest;
import org.bj.academylmsapi.model.board.board.BoardResponse;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.repository.StudentRepository;
import org.bj.academylmsapi.repository.TeacherRepository;
import org.bj.academylmsapi.repository.board.BoardRepository;
import org.springframework.stereotype.Service;

// 게시판 - 게시판 service

@Service
@RequiredArgsConstructor
public class BoardService {
    private final BoardRepository boardRepository;
    private final TeacherRepository teacherRepository;
    private final StudentRepository studentRepository;

    // 게시판 C
    public void setBoard(BoardRequest request) {
        Teacher teacherData = teacherRepository.findById(request.getTeacherId()).orElseThrow();
        Student studentData = studentRepository.findById(request.getStudentId()).orElseThrow();
        boardRepository.save(new Board.Builder(request, teacherData, studentData).build());
    }

    // 게시판 단수 R
    public BoardResponse getBoard(long id) {
        Board originData = boardRepository.findById(id).orElseThrow();
        return new BoardResponse.Builder(originData).build();
    }

    // 게시판 U
    public void putBoard(long id, BoardRequest request) {
        Board originData = boardRepository.findById(id).orElseThrow();
        Teacher teacherData = teacherRepository.findById(request.getTeacherId()).orElseThrow();
        Student studentData = studentRepository.findById(request.getStudentId()).orElseThrow();
        originData.putBoard(request, teacherData, studentData);
        boardRepository.save(originData);
    }

    // 게시판 D
    public void delBoard(long id) {
        boardRepository.deleteById(id);
    }
}
