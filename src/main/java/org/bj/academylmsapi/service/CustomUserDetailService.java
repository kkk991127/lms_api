package org.bj.academylmsapi.service;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.Academy;
import org.bj.academylmsapi.exception.CAcademyFullException;
import org.bj.academylmsapi.repository.AcademyRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CustomUserDetailService implements UserDetailsService {
    private  final AcademyRepository academyRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Academy academy =academyRepository.findByUsername(username).orElseThrow(CAcademyFullException::new);

        return academy;
    }
}
