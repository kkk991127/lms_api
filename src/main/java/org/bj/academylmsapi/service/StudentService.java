package org.bj.academylmsapi.service;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.entity.SubjectChoice;
import org.bj.academylmsapi.enums.ResultCode;
import org.bj.academylmsapi.model.result.CommonResult;
import org.bj.academylmsapi.model.result.ListResult;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.model.student.*;
import org.bj.academylmsapi.repository.StudentRepository;
import org.bj.academylmsapi.repository.SubjectChoiceRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

// 수강생 service

@Service
@RequiredArgsConstructor
public class StudentService {
    private final StudentRepository studentRepository;
    private final SubjectChoiceRepository subjectChoiceRepository;

    // id 찾아오기
    public Student getData(long id) {
        return studentRepository.findById(id).orElseThrow();
    }

    // 수강생 C
    public void setStudent(StudentRequest request) {
        SubjectChoice subjectChoiceData = subjectChoiceRepository.findById(request.getSubjectChoiceId()).orElseThrow();
        studentRepository.save(new Student.Builder(request, subjectChoiceData).build());
    }

    // 수강생 복수 R
    public List<StudentItem> getStudents() {
        List<Student> originData = studentRepository.findAllByOrderByIdAsc();
        List<StudentItem> result = new LinkedList<>();
        for (Student student : originData) result.add(new StudentItem.Builder(student).build());
        return result;
    }

    // 수강생 복수 R 페이징
    public ListResult<StudentItem> getStudentsPage(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Student> studentPage = studentRepository.findAll(pageRequest);
        List<StudentItem> result = new LinkedList<>();
        for (Student student : studentPage) result.add(new StudentItem.Builder(student).build());
        return ListConvertService.settingListResult(result, studentPage.getTotalElements(), studentPage.getTotalPages(), studentPage.getPageable().getPageNumber());
    }

    // 수강생 단수 R
    public StudentResponse getStudent(long id) {
        Student originData = studentRepository.findById(id).orElseThrow();
        return new StudentResponse.Builder(originData).build();
    }

    // 수강생 U
    public void putStudent(long id, StudentInfoChangeRequest studentInfoChangeRequest) {
        Student originData = studentRepository.findById(id).orElseThrow();
        originData.putStudentInfo(studentInfoChangeRequest);
        studentRepository.save(originData);
    }

    // 수강생 U 관리자용
    public void putStudentAdmin(long id, StudentInfoChangeAdminRequest studentInfoChangeAdminRequest) {
        Student originData = studentRepository.findById(id).orElseThrow();
        SubjectChoice subjectChoiceData = subjectChoiceRepository.findById(studentInfoChangeAdminRequest.getSubjectChoiceId()).orElseThrow();
        originData.putStudentInfoAdmin(subjectChoiceData, studentInfoChangeAdminRequest);
        studentRepository.save(originData);

    }

    // 수강생 D
    public void delStudent(long id) {
        studentRepository.deleteById(id);
    }
}
