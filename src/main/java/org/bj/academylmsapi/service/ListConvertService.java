package org.bj.academylmsapi.service;

import org.bj.academylmsapi.model.result.ListResult;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ListConvertService {
    public static PageRequest getPageable(int pageNum){
        return PageRequest.of(pageNum - 1,10);
    }

    public static PageRequest getPageable(int pageNum , int pageSize){
        return PageRequest.of(pageNum - 1, pageSize);
    }


    // 페이지 없는 거
    public static <T> ListResult<T> settingListResult(List<T> list) {
        ListResult<T> result = new ListResult<>();
        result.setList(list);
        result.setTotalCount((long) list.size());
        result.setTotalPage(1);
        result.setCurrentPage(1);
        return result;
    }

    // 페이징
    public static <T> ListResult<T> settingListResult(List<T> list , long totalItemCount, int totalPage, int currentPage ) {
        ListResult<T> result = new ListResult<>();
        result.setList(list);
        result.setTotalCount(totalItemCount);
        result.setTotalPage(totalPage == 0 ? 1 : totalPage);
        result.setCurrentPage(currentPage + 1);
        return result;
    }
}
