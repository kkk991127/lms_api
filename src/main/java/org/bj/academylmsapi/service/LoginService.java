package org.bj.academylmsapi.service;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.configure.JwtTokenProvider;
import org.bj.academylmsapi.entity.Academy;
import org.bj.academylmsapi.enums.MemberGroup;
import org.bj.academylmsapi.exception.CAcademyFullException;
import org.bj.academylmsapi.model.LoginRequest;
import org.bj.academylmsapi.model.LoginResponse;
import org.bj.academylmsapi.repository.AcademyRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LoginService {
    private final AcademyRepository academyRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenProvider jwtTokenProvider;

    public LoginResponse doLogin(MemberGroup memberGroup, LoginRequest loginRequest, String loginType) {
        Academy academy = academyRepository.findByUsername(loginRequest.getUsername()).orElseThrow(CAcademyFullException::new);
        // 회원정보가 없습니다 던지기
        // academy레포한테 username을 찾아달라고한다 ( loginrequest 에서 가져온 이름을 ) 아니라면 던져라 회원정보가 없습니다라고 .

        if(!academy.getMemberGroup().equals(memberGroup)) throw new CAcademyFullException();
        // 일반회원이 최고 관리자용으로 로그인 하려거나 이런 경우이므로 메세지는 회원정보가 없습니다 라고 일단 던짐
        if(!passwordEncoder.matches(loginRequest.getPassword(), academy.getPassword())) throw new CAcademyFullException();
        // 비밀번호가 일치되지 않습니다 던짐

        String token = jwtTokenProvider.createToken(String.valueOf(academy.getUsername()), academy.getMemberGroup().toString(),loginType);

        return new LoginResponse.Builder(token, academy.getUsername()).build();


    }


}
