package org.bj.academylmsapi.service.studentManagement;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.entity.Teacher;
import org.bj.academylmsapi.entity.studentManagement.AdviceManagement;
import org.bj.academylmsapi.enums.ResultCode;
import org.bj.academylmsapi.model.result.ListResult;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.model.studentManagement.adviceManagement.AdviceManagementItem;
import org.bj.academylmsapi.model.studentManagement.adviceManagement.AdviceManagementRequest;
import org.bj.academylmsapi.model.studentManagement.adviceManagement.AdviceManagementResponse;
import org.bj.academylmsapi.repository.StudentRepository;
import org.bj.academylmsapi.repository.TeacherRepository;
import org.bj.academylmsapi.repository.studentManagement.AdviceManagementRepository;
import org.bj.academylmsapi.service.ListConvertService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

// 학생관리 - 취업관리 - 상담관리 service

@Service
@RequiredArgsConstructor
public class AdviceManagementService {
    private final AdviceManagementRepository adviceManagementRepository;
    private final TeacherRepository teacherRepository;
    private final StudentRepository studentRepository;

    // 상담관리 C
    public void setAdviceManagement(AdviceManagementRequest request) {
        Teacher teacherData = teacherRepository.findById(request.getTeacherId()).orElseThrow();
        Student studentData = studentRepository.findById(request.getStudentId()).orElseThrow();
        adviceManagementRepository.save(new AdviceManagement.Builder(request, teacherData, studentData).build());
    }

    // 상담관리 복수 R
    public List<AdviceManagementItem> getAdviceManagements() {
        List<AdviceManagement> originData = adviceManagementRepository.findAll();
        List<AdviceManagementItem> result = new LinkedList<>();
        for (AdviceManagement adviceManagement : originData) result.add(new AdviceManagementItem.Builder(adviceManagement).build());
        return result;
    }

    // 상담관리 복수 R 페이징
    public ListResult<AdviceManagementItem> getAdviceManagementsPage(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<AdviceManagement> adviceManagementPage = adviceManagementRepository.findAll(pageRequest);
        List<AdviceManagementItem> listResult = new LinkedList<>();
        for (AdviceManagement adviceManagement : adviceManagementPage) listResult.add(new AdviceManagementItem.Builder(adviceManagement).build());
        return ListConvertService.settingListResult(listResult, adviceManagementPage.getTotalElements(), adviceManagementPage.getTotalPages(), adviceManagementPage.getPageable().getPageNumber());
    }

    // 상담관리 단수 R
    public AdviceManagementResponse getAdviceManagement(long id) {
        AdviceManagement originData = adviceManagementRepository.findById(id).orElseThrow();
        return new AdviceManagementResponse.Builder(originData).build();
    }

    // 상담관리 U
    public void putAdviceManagement(long id, AdviceManagementRequest request) {
        Teacher teacherData = teacherRepository.findById(request.getTeacherId()).orElseThrow();
        Student studentData = studentRepository.findById(request.getStudentId()).orElseThrow();
        AdviceManagement originData = adviceManagementRepository.findById(id).orElseThrow();
        originData.putAdviceManagement(request, teacherData, studentData);
        adviceManagementRepository.save(originData);
    }

    // 상담관리 D
    public void delAdviceManagement(long id) {
        adviceManagementRepository.deleteById(id);
    }
}
