package org.bj.academylmsapi.service.studentManagement;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.entity.studentManagement.WorkManagement;
import org.bj.academylmsapi.enums.ResultCode;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.model.studentManagement.workManagement.WorkManagementRequest;
import org.bj.academylmsapi.model.studentManagement.workManagement.WorkManagementResponse;
import org.bj.academylmsapi.repository.StudentRepository;
import org.bj.academylmsapi.repository.studentManagement.WorkManagementRepository;
import org.springframework.stereotype.Service;

// 학생관리 - 취업관리 - 취업관리사항 service

@Service
@RequiredArgsConstructor
public class WorkManagementService {
    private final WorkManagementRepository workManagementRepository;
    private final StudentRepository studentRepository;

    // 취업관리사항 C
    public void setWorkManagement(WorkManagementRequest request) {
        Student studentData = studentRepository.findById(request.getStudentId()).orElseThrow();
        workManagementRepository.save(new WorkManagement.Builder(request, studentData).build());
    }

    // 취업관리사항 단수 R
    public WorkManagementResponse getWorkManagement(long id) {
        WorkManagement originData = workManagementRepository.findById(id).orElseThrow();
        return new WorkManagementResponse.Builder(originData).build();
    }

    // 취업관리사항 U
    public void putWorkManagement(long id, WorkManagementRequest request) {
        Student studentData = studentRepository.findById(request.getStudentId()).orElseThrow();
        WorkManagement originData = workManagementRepository.findById(id).orElseThrow();
        originData.putWorkManagement(request, studentData);
        workManagementRepository.save(originData);
    }

    // 취업관리사항 D
    public void delWorkManagement(long id) {
        workManagementRepository.deleteById(id);
    }
}
