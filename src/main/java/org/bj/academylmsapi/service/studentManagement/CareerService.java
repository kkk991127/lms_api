package org.bj.academylmsapi.service.studentManagement;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.entity.studentManagement.Career;
import org.bj.academylmsapi.entity.studentManagement.DocuManagement;
import org.bj.academylmsapi.enums.ResultCode;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.model.studentManagement.career.CareerRequest;
import org.bj.academylmsapi.model.studentManagement.career.CareerResponse;
import org.bj.academylmsapi.repository.StudentRepository;
import org.bj.academylmsapi.repository.studentManagement.CareerRepository;
import org.springframework.stereotype.Service;

// 학생관리 - 취업관리 - 경력사항 service

@Service
@RequiredArgsConstructor
public class CareerService {
    private final CareerRepository careerRepository;
    private final StudentRepository studentRepository;

    // 경력사항 C
    public void setCareer(CareerRequest request) {
        Student studentData = studentRepository.findById(request.getStudentId()).orElseThrow();
        careerRepository.save(new Career.Builder(request, studentData).build());
    }

    // 경력사항 단수 R
    public CareerResponse getCareer(long id) {
        Career originData = careerRepository.findById(id).orElseThrow();
        return new CareerResponse.Builder(originData).build();
    }

    // 경력사항 정보 수정
    public void putCareer(long id, CareerRequest request) {
        Student studentData = studentRepository.findById(request.getStudentId()).orElseThrow();
        Career originData = new Career.Builder(request, studentData).build();
        originData.putCareer(request,studentData);
        careerRepository.save(originData);
    }

    // 경력사항 정보 삭제
    public void delCareer(long id) {
        careerRepository.deleteById(id);
    }
}
