package org.bj.academylmsapi.service.studentManagement;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.studentManagement.DocuManagement;
import org.bj.academylmsapi.enums.ResultCode;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.model.studentManagement.docuManagement.DocuManagamentResponse;
import org.bj.academylmsapi.model.studentManagement.docuManagement.DocuManagementRequest;
import org.bj.academylmsapi.repository.studentManagement.DocuManagementRepository;
import org.springframework.stereotype.Service;

// 학생관리 - 서류관리 service

@Service
@RequiredArgsConstructor
public class DocuManagementService {
    private final DocuManagementRepository docuManagementRepository;

    // 서류관리 C
    public void setDocuManagement(DocuManagementRequest request) {
        docuManagementRepository.save(new DocuManagement.Builder(request).build());
    }

    // 서류관리 단수 R
    public DocuManagamentResponse getDocuManagement(long id) {
        DocuManagement originData = docuManagementRepository.findById(id).orElseThrow();
        return new DocuManagamentResponse.Builder(originData).build();
    }

    // 서류관리 U
    public void putDocuManagement(long id, DocuManagementRequest request) {
        DocuManagement originData = docuManagementRepository.findById(id).orElseThrow();
        originData.putDocuManagement(request);
        docuManagementRepository.save(originData);
    }

    // 서류관리 D
    public void delDocuManagement(long id) {
        docuManagementRepository.deleteById(id);
    }
}
