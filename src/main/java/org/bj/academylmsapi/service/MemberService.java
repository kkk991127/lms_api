package org.bj.academylmsapi.service;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.Member;
import org.bj.academylmsapi.enums.MemberGroup;
import org.bj.academylmsapi.exception.CAcademyFullException;
import org.bj.academylmsapi.lib.CommonCheck;
import org.bj.academylmsapi.model.MemberRequest;
import org.bj.academylmsapi.repository.MemberRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor

public class MemberService {
    private final MemberRepository memberRepository;
    private final PasswordEncoder passwordEncoder;
        public  void setMember(MemberGroup memberGroup, MemberRequest request){
            if(!CommonCheck.checkUsername(request.getUsername())) throw new CAcademyFullException();
            if(!request.getPassword().equals(request.getPassword())) throw new CAcademyFullException();



        }


    public void setMember(MemberRequest request) {memberRepository.save(new Member.Builder(request).build());}
}
