package org.bj.academylmsapi.service;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.SubjectChoice;
import org.bj.academylmsapi.entity.Teacher;
import org.bj.academylmsapi.enums.ResultCode;
import org.bj.academylmsapi.model.result.CommonResult;
import org.bj.academylmsapi.model.result.ListResult;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.model.subjectChoice.SubjectChoiceItem;
import org.bj.academylmsapi.model.subjectChoice.SubjectChoiceRequest;
import org.bj.academylmsapi.model.subjectChoice.SubjectChoiceResponse;
import org.bj.academylmsapi.repository.SubjectChoiceRepository;
import org.bj.academylmsapi.repository.TeacherRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;


import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

// 과정 service

@Service
@RequiredArgsConstructor
public class SubjectChoiceService {
    private final SubjectChoiceRepository subjectChoiceRepository;
    private final TeacherRepository teacherRepository;

    // 과정 C
    public void setSubjectChoice(SubjectChoiceRequest request){
        Teacher teacherData = teacherRepository.findById(request.getTeacherId()).orElseThrow();
        subjectChoiceRepository.save(new SubjectChoice.Builder(request,teacherData).build());
    }

    // 과정 복수 R
    public List<SubjectChoiceItem> getSubjectList(){
        List<SubjectChoice> originData = subjectChoiceRepository.findAll();
        List<SubjectChoiceItem> result = new LinkedList<>();
        for (SubjectChoice subjectChoice : originData) result.add(new SubjectChoiceItem.Builder(subjectChoice).build());
        return result;
    }

    // 과정 복수 R 페이징
    public ListResult<SubjectChoiceItem> getSubjectPageList(int pageNum){
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<SubjectChoice> subjectChoicePage = subjectChoiceRepository.findAll(pageRequest);
        List<SubjectChoiceItem> result = new LinkedList<>();
        for (SubjectChoice subjectChoice : subjectChoicePage) result.add(new SubjectChoiceItem.Builder(subjectChoice).build());
        return ListConvertService.settingListResult(result, subjectChoicePage.getTotalElements(), subjectChoicePage.getTotalPages(), subjectChoicePage.getPageable().getPageNumber());
    }

    // 과정 단수 R
    public SubjectChoiceResponse getSubjectDetail(long id){
        SubjectChoice originData = subjectChoiceRepository.findById(id).orElseThrow();
        return new SubjectChoiceResponse.Builder(originData).build();
    }

    // 과정 U
    public void putSubjectChoice(long id , SubjectChoiceRequest request){
        SubjectChoice subjectChoice = subjectChoiceRepository.findById(id).orElseThrow();
        Teacher teacherData = teacherRepository.findById(request.getTeacherId()).orElseThrow();

        subjectChoice.putSubject(teacherData, request);
        subjectChoiceRepository.save(subjectChoice);
    }

    // 과정 D
    public void delSubject(long id){
        subjectChoiceRepository.deleteById(id);
    }
}
