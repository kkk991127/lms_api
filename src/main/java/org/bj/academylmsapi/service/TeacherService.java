package org.bj.academylmsapi.service;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.Teacher;
import org.bj.academylmsapi.enums.ResultCode;
import org.bj.academylmsapi.model.result.CommonResult;
import org.bj.academylmsapi.model.result.ListResult;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.model.teacher.*;
import org.bj.academylmsapi.repository.TeacherRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

// 강사 service

@Service
@RequiredArgsConstructor
public class TeacherService {
    private final TeacherRepository teacherRepository;

    // 강사 C
    public void setTeacher(TeacherRequest request) {
        teacherRepository.save(new Teacher.Builder(request).build());
    }

    // 강사 복수 R
    public List<TeacherItem> getTeachers() {
        List<Teacher> originList = teacherRepository.findAll();
        List<TeacherItem> result = new LinkedList<>();
        for (Teacher teacher : originList) result.add(new TeacherItem.Builder(teacher).build());
        return result;
    }


    // 강사 복수 R 페이징
    public ListResult<TeacherItem> getTeachersPage(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Teacher> teachersPage = teacherRepository.findAll(pageRequest);
        List<TeacherItem> result = new LinkedList<>();
        for (Teacher teacher : teachersPage) result.add(new TeacherItem.Builder(teacher).build());
        return ListConvertService.settingListResult(result, teachersPage.getTotalElements(), teachersPage.getTotalPages(), teachersPage.getPageable().getPageNumber());
    }

    // 강사 단수 R
    public TeacherResponse getTeacher(long id) {
        Teacher originData = teacherRepository.findById(id).orElseThrow();
        return new TeacherResponse.Builder(originData).build();
    }

    // 강사 U
    public void putTeacherInfo(long id, TeacherInfoChangeRequest request) {
        Teacher originData = teacherRepository.findById(id).orElseThrow();
        originData.putTeacher(request);
        teacherRepository.save(originData);
    }

    // 강사 U 관리자 버전
    public void putTeacherInfo(long id, TeacherInfoChangeAdminRequest request) {
        Teacher originData = teacherRepository.findById(id).orElseThrow();
        originData.putTeacherAdmin(request);
        teacherRepository.save(originData);
    }

    // 강사 D
    public void delTeacher(long id) {
        teacherRepository.deleteById(id);
    }
}
