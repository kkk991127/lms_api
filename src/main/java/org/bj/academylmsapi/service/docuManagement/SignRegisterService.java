package org.bj.academylmsapi.service.docuManagement;


import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.SubjectChoice;
import org.bj.academylmsapi.entity.docuManagement.SignRegister;
import org.bj.academylmsapi.enums.ResultCode;
import org.bj.academylmsapi.model.docuManagement.signRegister.SignRegisterRequest;
import org.bj.academylmsapi.model.docuManagement.signRegister.SignRegisterResponse;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.repository.SubjectChoiceRepository;
import org.bj.academylmsapi.repository.docuManagement.SignRegisterRepository;
import org.springframework.stereotype.Service;

// 서류관리 - 서류등록 service

@Service
@RequiredArgsConstructor
public class SignRegisterService {
    private final SignRegisterRepository signRegisterRepository;
    private final SubjectChoiceRepository subjectChoiceRepository;

    // 서류등록 C
    public void setSignRegister(SignRegisterRequest request) {
        SubjectChoice subjectChoiceData = subjectChoiceRepository.findById(request.getSubjectChoiceId()).orElseThrow();
        signRegisterRepository.save(new SignRegister.Builder(request, subjectChoiceData).build());
    }

    // 서류등록 단수 R
    public SignRegisterResponse getSignRegister(long id) {
        SignRegister originData = signRegisterRepository.findById(id).orElseThrow();
        return new SignRegisterResponse.Builder(originData).build();
    }

    // 서류등록 U
    public void putSignRegister(long id, SignRegisterRequest request) {
        SignRegister originData= signRegisterRepository.findById(id).orElseThrow();
        SubjectChoice subjectChoiceData = subjectChoiceRepository.findById(request.getSubjectChoiceId()).orElseThrow();
        originData.putSignRegister(request, subjectChoiceData);
        signRegisterRepository.save(originData);
    }

    // 서류등록 D
    public void delSignRegister(long id) {
        signRegisterRepository.deleteById(id);
    }
}
