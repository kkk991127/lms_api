package org.bj.academylmsapi.service;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.Academy;
import org.bj.academylmsapi.enums.MemberGroup;
import org.bj.academylmsapi.enums.ResultCode;
import org.bj.academylmsapi.exception.CAcademyFullException;
import org.bj.academylmsapi.lib.CommonCheck;
import org.bj.academylmsapi.model.academy.AcademyInfoChangeRequest;
import org.bj.academylmsapi.model.academy.AcademyItem;
import org.bj.academylmsapi.model.academy.AcademyRequest;
import org.bj.academylmsapi.model.academy.AcademyResponse;
import org.bj.academylmsapi.model.result.ListResult;
import org.bj.academylmsapi.repository.AcademyRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

// 학원 service

@Service
@RequiredArgsConstructor
public class AcademyService {
    private final AcademyRepository academyRepository;
    private final PasswordEncoder passwordEncoder;

     //   public void setMember(MemberGroup memberGroup, AcademyRequest request){
    //    if (!CommonCheck.checkUsername(request.getUsername())) throw new CAcademyFullException();
    //   // if (!request.getPassword().equals(request.getPassword()) throw new CAcademyFullException();
     //   if (!isNewUsername(request.getUsername())) throw new CAcademyFullException();


  //      request.setPassword(passwordEncoder.encode(request.getPassword()));
   //     Academy academy = new Academy.Builder(memberGroup, request).build();
  //      academyRepository.save(academy);

    //}

  //  private boolean isNewUsername(String username) {
  //      long dupCount = academyRepository.countByUsername(username);
   //     return dupCount <= 0;
   // }

    // 학원 C
    public void setAcademy(AcademyRequest request) {
        academyRepository.save(new Academy.Builder(request).build());
    }

    // 학원 복수 R
    public List<AcademyItem> getAcademys() {
        List<Academy> originData = academyRepository.findAllByOrderByIdAsc();
        List<AcademyItem> result = new LinkedList<>();
        for (Academy academy : originData) result.add(new AcademyItem.Builder(academy).build());
        return result;
    }

    // 학원 복수 R 페이징
    public ListResult<AcademyItem> getAcademysPage(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Academy> academyPage = academyRepository.findAll(pageRequest);
        List<AcademyItem> result = new LinkedList<>();
        for (Academy academy : academyPage) result.add(new AcademyItem.Builder(academy).build());
        return ListConvertService.settingListResult(result, academyPage.getTotalElements(), academyPage.getTotalPages(), academyPage.getPageable().getPageNumber());
    }

    // 학원 단수 R
    public AcademyResponse getAcademy(long id) {
        Academy originData = academyRepository.findById(id).orElseThrow();
        return new AcademyResponse.Builder(originData).build();

    }

    // 학원 U
    public void putAcademy(long id, AcademyInfoChangeRequest request) {
        Academy originData = academyRepository.findById(id).orElseThrow();
        originData.putAcademy(request);
        academyRepository.save(originData);
    }

    // 학원 D
    public void delAcademy(long id) {
        academyRepository.deleteById(id);
    }
}
