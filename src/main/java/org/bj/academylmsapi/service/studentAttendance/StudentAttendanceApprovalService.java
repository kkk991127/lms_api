package org.bj.academylmsapi.service.studentAttendance;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.entity.studentAttendance.StudentAttendanceApproval;
import org.bj.academylmsapi.enums.ResultCode;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.model.studentAttendance.studentAttendanceApproval.StudentAttendanceApprovalRequest;
import org.bj.academylmsapi.model.studentAttendance.studentAttendanceApproval.StudentAttendanceApprovalResponse;
import org.bj.academylmsapi.repository.StudentRepository;
import org.bj.academylmsapi.repository.studentAttendance.StudentAttendanceApprovalRepository;
import org.springframework.stereotype.Service;


// 수강생 출결 승인 service

@Service
@RequiredArgsConstructor
public class StudentAttendanceApprovalService {
    private final StudentAttendanceApprovalRepository studentAttendanceApprovalRepository;
    private final StudentRepository studentRepository;

    // 수강생 출결승인 C
    public void setStudentAttendanceApproval(StudentAttendanceApprovalRequest request) {
        Student studentData = studentRepository.findById(request.getStudentId()).orElseThrow();
        studentAttendanceApprovalRepository.save(new StudentAttendanceApproval.Builder(request, studentData).build());
    }

    // 수강생 출결승인 단수 R
    public StudentAttendanceApprovalResponse getStudentAttendanceApproval(long id) {
        StudentAttendanceApproval originData = studentAttendanceApprovalRepository.findById(id).orElseThrow();
        return new StudentAttendanceApprovalResponse.Builder(originData).build();
    }

    // 수강생 출결승인 U
    public void putStudentAttendanceApproval(long id, StudentAttendanceApprovalRequest request) {
        Student studentData = studentRepository.findById(request.getStudentId()).orElseThrow();
        StudentAttendanceApproval originData = studentAttendanceApprovalRepository.findById(id).orElseThrow();
        originData.putStudentAttendanceApproval(request,studentData);
        studentAttendanceApprovalRepository.save(originData);
    }

    // 수강생 출결승인 D
    public void delStudentAttendanceApproval(long id) {
        studentAttendanceApprovalRepository.deleteById(id);
    }
}
