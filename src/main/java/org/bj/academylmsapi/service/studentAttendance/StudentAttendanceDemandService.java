package org.bj.academylmsapi.service.studentAttendance;


import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.entity.studentAttendance.StudentAttendanceDemand;
import org.bj.academylmsapi.enums.ResultCode;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.model.studentAttendance.studentAttendanceDemand.StudentAttendanceDemandRequest;
import org.bj.academylmsapi.model.studentAttendance.studentAttendanceDemand.StudentAttendanceDemandResponse;
import org.bj.academylmsapi.repository.StudentRepository;
import org.bj.academylmsapi.repository.studentAttendance.StudentAttendanceDemandRepository;
import org.springframework.stereotype.Service;

// 수강생 출결 신청 service

@Service
@RequiredArgsConstructor
public class StudentAttendanceDemandService {
    private final StudentAttendanceDemandRepository studentAttendanceDemandRepository;
    private final StudentRepository studentRepository;

    // 수강생 출결 신청 C
    public void setStudentAttendanceDemand(StudentAttendanceDemandRequest request) {
        Student studentData = studentRepository.findById(request.getStudentId()).orElseThrow();
        studentAttendanceDemandRepository.save(new StudentAttendanceDemand.Builder(request, studentData).build());
    }

    // 수강생 출결 신청 단수 R
    public StudentAttendanceDemandResponse getStudentAttendanceDemand(long id) {
        StudentAttendanceDemand originData = studentAttendanceDemandRepository.findById(id).orElseThrow();
        return new StudentAttendanceDemandResponse.Builder(originData).build();
    }

    // 수강생 출결 신청 U
    public void putStudentAttendanceDemand(long id, StudentAttendanceDemandRequest request) {
        Student studentData = studentRepository.findById(request.getStudentId()).orElseThrow();
        StudentAttendanceDemand originData = studentAttendanceDemandRepository.findById(id).orElseThrow();
        originData.putStudentAttendanceDemand(request, studentData);
        studentAttendanceDemandRepository.save(originData);
    }

    // 수강생 출결 신청 D
    public void delStudentAttendanceDemand(long id) {
        studentAttendanceDemandRepository.deleteById(id);
    }
}
