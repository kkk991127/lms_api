package org.bj.academylmsapi.service.studentAttendance;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.entity.studentAttendance.StudentAttendance;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.model.studentAttendance.studentAttendance.StudentAttendanceRequest;
import org.bj.academylmsapi.model.studentAttendance.studentAttendance.StudentAttendanceResponse;
import org.bj.academylmsapi.repository.studentAttendance.StudentAttendanceRepository;
import org.bj.academylmsapi.repository.StudentRepository;
import org.bj.academylmsapi.service.ResponseService;
import org.springframework.stereotype.Service;

//  수강생 출결 service

@Service
@RequiredArgsConstructor
public class StudentAttendanceService {
    private final StudentAttendanceRepository studentAttendanceRepository;
    private final StudentRepository studentRepository;

    // 수강생 출결 C
    public void SetStudentAttendance(StudentAttendanceRequest request) {
        Student studentData = studentRepository.findById(request.getStudentId()).orElseThrow();
        studentAttendanceRepository.save(new StudentAttendance.Builder(request, studentData).build());
    }

    // 수강생 출결 단수 R
    public StudentAttendanceResponse getStudentAttendance(long id) {
        StudentAttendance originData = studentAttendanceRepository.findById(id).orElseThrow();
        return new StudentAttendanceResponse.Builder(originData).build();
    }

    // 수강생 U
    public void putStudentAttendance(long id, StudentAttendanceRequest request) {
        Student studentData = studentRepository.findById(request.getStudentId()).orElseThrow();
        StudentAttendance origindata = studentAttendanceRepository.findById(id).orElseThrow();
        origindata.putStudentAttendance(request, studentData);
        studentAttendanceRepository.save(origindata);
    }

    // 수강생 D
    public void delStudentAttendance(long id) {
        studentAttendanceRepository.deleteById(id);
    }
}
