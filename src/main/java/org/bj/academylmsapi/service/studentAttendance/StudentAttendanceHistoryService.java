package org.bj.academylmsapi.service.studentAttendance;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.entity.studentAttendance.StudentAttendance;
import org.bj.academylmsapi.entity.studentAttendance.StudentAttendanceHistory;
import org.bj.academylmsapi.enums.ResultCode;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.model.studentAttendance.studentAttendanceHistory.StudentAttendanceHistoryRequest;
import org.bj.academylmsapi.model.studentAttendance.studentAttendanceHistory.StudentAttendanceHistoryResponse;
import org.bj.academylmsapi.repository.StudentRepository;
import org.bj.academylmsapi.repository.studentAttendance.StudentAttendanceHistoryRepository;
import org.springframework.stereotype.Service;

// 수강생 출결 내역 service

@Service
@RequiredArgsConstructor
public class StudentAttendanceHistoryService {
    private final StudentAttendanceHistoryRepository studentAttendanceHistoryRepository;
    private final StudentRepository studentRepository;

    // 수강생 출결 내역 C
    public void SetStudentAttendanceHistory(StudentAttendanceHistoryRequest request) {
        Student studentData = studentRepository.findById(request.getStudentId()).orElseThrow();
        studentAttendanceHistoryRepository.save(new StudentAttendanceHistory.Builder(request, studentData).build());
    }

    // 수강생 출결 내역 단수 R
    public StudentAttendanceHistoryResponse getStudentAttendanceHistory(long id) {
        StudentAttendanceHistory originData = studentAttendanceHistoryRepository.findById(id).orElseThrow();
        return new StudentAttendanceHistoryResponse.Builder(originData).build();
    }

    // 수강생 출결 내역 U
    public void putStudentAttendanceHistory(long id, StudentAttendanceHistoryRequest request) {
        Student studentData = studentRepository.findById(request.getStudentId()).orElseThrow();
        StudentAttendanceHistory originData = studentAttendanceHistoryRepository.findById(id).orElseThrow();
        originData.putStudentAttendanceHistory(request, studentData);
        studentAttendanceHistoryRepository.save(originData);
    }

    // 수강생 출결 내역 D
    public void delStudentAttendanceHistory(long id) {
        studentAttendanceHistoryRepository.deleteById(id);
    }
}
