package org.bj.academylmsapi.service.testManagement;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.entity.testManagement.AnswerTest;
import org.bj.academylmsapi.entity.testManagement.MakeTest;
import org.bj.academylmsapi.model.Test.Answer.AnswerPutScoreRequest;
import org.bj.academylmsapi.model.Test.Answer.AnswerPutStudentSign;
import org.bj.academylmsapi.model.Test.Answer.AnswerTestRequest;
import org.bj.academylmsapi.model.Test.Answer.AnswerTestResponse;
import org.bj.academylmsapi.model.result.CommonResult;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.repository.StudentRepository;
import org.bj.academylmsapi.repository.testManagement.AnswerTestRepository;
import org.bj.academylmsapi.repository.testManagement.MakeTestRepository;
import org.bj.academylmsapi.repository.testManagement.SubjectStatusRepository;
import org.bj.academylmsapi.service.ResponseService;
import org.springframework.stereotype.Service;

import javax.print.DocFlavor;

// 평가관리 - 시험출제 service

@Service
@RequiredArgsConstructor
public class AnswerTestService {
    private final AnswerTestRepository answerTestRepository;
    private final StudentRepository studentRepository;
    private final MakeTestRepository makeTestRepository;

    public void setAnswer(AnswerTestRequest request){
        MakeTest makeTest = makeTestRepository.findById(request.getMakeTest()).orElseThrow();
        Student student = studentRepository.findById(request.getStudent()).orElseThrow();
        AnswerTest answerTest = new AnswerTest.Builder(makeTest, student, request).build();
        answerTestRepository.save(answerTest);
    }
    public SingleResult<AnswerTestResponse> getAnswer(long id){
        AnswerTest answerTest = answerTestRepository.findById(id).orElseThrow();
        AnswerTestResponse answerTestResponse = new AnswerTestResponse.Builder(answerTest).build();

        return ResponseService.getSingleResult(answerTestResponse);
    }

    public CommonResult putAnswerScore(AnswerPutScoreRequest request, long id){
        AnswerTest answerTest = answerTestRepository.findById(id).orElseThrow();
        answerTest.getScore(request);
        answerTestRepository.save(answerTest);
        return ResponseService.getSuccessResult();
    }

    public CommonResult putTestCheckSign(AnswerPutStudentSign request , long id){
        AnswerTest answerTest = answerTestRepository.findById(id).orElseThrow();
        answerTest.AnswerTestSign(request);
        answerTestRepository.save(answerTest);

        return ResponseService.getSuccessResult();
    }


    public CommonResult delAnswer(long id){
        answerTestRepository.findById(id);
        return ResponseService.getSuccessResult();
    }
}
