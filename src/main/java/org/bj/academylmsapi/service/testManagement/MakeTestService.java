package org.bj.academylmsapi.service.testManagement;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.testManagement.MakeTest;
import org.bj.academylmsapi.entity.testManagement.SubjectStatus;
import org.bj.academylmsapi.model.Test.MakeTest.MakeTestRequest;
import org.bj.academylmsapi.model.Test.MakeTest.MakeTestResponse;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.repository.testManagement.MakeTestRepository;
import org.bj.academylmsapi.repository.testManagement.SubjectStatusRepository;
import org.bj.academylmsapi.service.ResponseService;
import org.springframework.stereotype.Service;

// 평가관리 - 시험출제 service

@Service
@RequiredArgsConstructor
public class MakeTestService {
    private final MakeTestRepository makeTestRepository;
    private final SubjectStatusRepository subjectStatusRepository;


    public void setMakeTest(MakeTestRequest request){
        SubjectStatus subjectStatus = subjectStatusRepository.findById(request.getSubjectStatusId()).orElseThrow();
        subjectStatus.putSubjectStatusChange();
        makeTestRepository.save(new MakeTest.Builder(subjectStatus,request).build());
        subjectStatusRepository.save(subjectStatus);
    }

    public MakeTestResponse getMakeTests(long id){
        MakeTest makeTest = makeTestRepository.findById(id).orElseThrow();
        return new MakeTestResponse.Builder(makeTest).build();
    }

    public void putMakeTest(MakeTestRequest request,long id){
        SubjectStatus subjectStatus = subjectStatusRepository.findById(request.getSubjectStatusId()).orElseThrow();
        MakeTest OriginData = makeTestRepository.findById(id).orElseThrow();
        OriginData.putMakeTest(subjectStatus,request);
        makeTestRepository.save(OriginData);
    }

    public void delMakeTest(long id){
        makeTestRepository.deleteById(id);
    }
}
