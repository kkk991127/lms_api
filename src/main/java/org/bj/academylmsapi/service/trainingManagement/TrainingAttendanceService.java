package org.bj.academylmsapi.service.trainingManagement;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.trainingManagement.TrainingAttendance;
import org.bj.academylmsapi.enums.ResultCode;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.model.trainingManagement.trainingAttendance.TrainingAttendanceRequest;
import org.bj.academylmsapi.model.trainingManagement.trainingAttendance.TrainingAttendanceResponse;
import org.bj.academylmsapi.repository.trainingManagement.TrainingAttendanceRepository;
import org.bj.academylmsapi.service.ResponseService;
import org.springframework.stereotype.Service;

// 훈련관리 - 훈련일지 출결 service

@Service
@RequiredArgsConstructor
public class TrainingAttendanceService {
    private final TrainingAttendanceRepository trainingAttendanceRepository;

    // 훈련일지 출결 C
    public void setTrainingAttendance(TrainingAttendanceRequest request) {
        trainingAttendanceRepository.save(new TrainingAttendance.Builder(request).build());
    }

    // 훈련일지 출결 단수 R
    public TrainingAttendanceResponse getTrainingAttendance(long id) {
        TrainingAttendance originData = trainingAttendanceRepository.findById(id).orElseThrow();
        return new TrainingAttendanceResponse.Builder(originData).build();
    }

    // 훈련일지 출결 U
    public void putTrainingAttendance(long id, TrainingAttendanceRequest request) {
        TrainingAttendance originData = trainingAttendanceRepository.findById(id).orElseThrow();
        originData.putTrainingAttendance(request);
        trainingAttendanceRepository.save(originData);
    }

    // 훈련일지 출결 D
    public void delTrainingAttendance(long id) {
        trainingAttendanceRepository.deleteById(id);
    }
}
