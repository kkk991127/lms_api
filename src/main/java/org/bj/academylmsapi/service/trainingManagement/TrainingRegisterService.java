package org.bj.academylmsapi.service.trainingManagement;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.entity.Teacher;
import org.bj.academylmsapi.entity.trainingManagement.TrainingRegister;
import org.bj.academylmsapi.enums.ResultCode;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.model.trainingManagement.trainingRegister.TrainingRegisterRequest;
import org.bj.academylmsapi.model.trainingManagement.trainingRegister.TrainingRegisterResponse;
import org.bj.academylmsapi.repository.StudentRepository;
import org.bj.academylmsapi.repository.TeacherRepository;
import org.bj.academylmsapi.repository.trainingManagement.TrainingRegisterRepository;
import org.springframework.stereotype.Service;

// 훈련관리 - 훈련일지 등록 service

@Service
@RequiredArgsConstructor
public class TrainingRegisterService {
    private final TrainingRegisterRepository trainingRegisterRepository;
    private final StudentRepository studentRepository;
    private final TeacherRepository teacherRepository;

    // 훈련일지 등록 C
    public void setTrainingRegister(TrainingRegisterRequest request) {
        Student studentData = studentRepository.findById(request.getStudentId()).orElseThrow();
        Teacher teacherData = teacherRepository.findById(request.getTeacherId()).orElseThrow();
        trainingRegisterRepository.save(new TrainingRegister.Builder(request, studentData, teacherData).build());
    }

    // 훈련일지 등록 단수 R
    public TrainingRegisterResponse getTrainingRegister(long id) {
        TrainingRegister originData = trainingRegisterRepository.findById(id).orElseThrow();
        return new TrainingRegisterResponse.Builder(originData).build();
    }

    // 훈련일지 등록 U
    public void putTrainingRegister(long id, TrainingRegisterRequest request) {
        TrainingRegister originData = trainingRegisterRepository.findById(id).orElseThrow();
        Student studentData = studentRepository.findById(request.getStudentId()).orElseThrow();
        Teacher teacherData = teacherRepository.findById(request.getTeacherId()).orElseThrow();
        originData.putTrainingRegister(request, studentData, teacherData);
        trainingRegisterRepository.save(originData);
    }

    // 훈련일지 등록 D
    public void delTrainingRegister(long id) {
        trainingRegisterRepository.deleteById(id);
    }
}
