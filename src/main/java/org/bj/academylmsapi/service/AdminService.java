package org.bj.academylmsapi.service;

import lombok.RequiredArgsConstructor;
import org.bj.academylmsapi.entity.Admin;
import org.bj.academylmsapi.enums.ResultCode;
import org.bj.academylmsapi.model.admin.AdminInfoChangeRequest;
import org.bj.academylmsapi.model.admin.AdminItem;
import org.bj.academylmsapi.model.admin.AdminRequest;
import org.bj.academylmsapi.model.admin.AdminResponse;
import org.bj.academylmsapi.model.result.CommonResult;
import org.bj.academylmsapi.model.result.ListResult;
import org.bj.academylmsapi.model.result.SingleResult;
import org.bj.academylmsapi.repository.AdminRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

// 관리자 service

@Service
@RequiredArgsConstructor
public class AdminService {
    private final AdminRepository adminRepository;

    // 관리자 C
    public void setAdmin(AdminRequest request) {
        adminRepository.save(new Admin.Builder(request).build());
    }

    // 관리자 복수 R
    public List<AdminItem> getAdmins() {
        List<Admin> originList = adminRepository.findAllByOrderByIdAsc();
        List<AdminItem> result = new LinkedList<>();
        for (Admin admin : originList) result.add(new AdminItem.Builder(admin).build());
        return result;
    }

    // 관리자 복수 R 페이징
    public ListResult<AdminItem> getAdminsPage(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Admin> adminPage = adminRepository.findAll(pageRequest);
        List<AdminItem> result = new LinkedList<>();
        for (Admin admin : adminPage) result.add(new AdminItem.Builder(admin).build());
        return ListConvertService.settingListResult(result, adminPage.getTotalElements(), adminPage.getTotalPages(), adminPage.getPageable().getPageNumber());
    }

    // 관리자 단수 R
    public AdminResponse getAdmin(long id) {
        Admin originData = adminRepository.findById(id).orElseThrow();
        return new AdminResponse.Builder(originData).build();
    }

    // 관리자 U
    public void putAdmin(long id, AdminInfoChangeRequest request) {
        Admin originData = adminRepository.findById(id).orElseThrow();
        originData.putAdmin(request);
        adminRepository.save(originData);
    }


    // 관리자 D
    public void delAdmin(long id) {
        adminRepository.deleteById(id);
    }
}
