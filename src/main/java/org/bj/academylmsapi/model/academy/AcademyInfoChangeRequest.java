package org.bj.academylmsapi.model.academy;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

// 학원 U model

@Getter
@Setter
public class AcademyInfoChangeRequest {
    private String academyName;
    private String address;
    private String businessType;
    private String businessContent;
    private String representativeName;
    private String academyCallNumber;
    private String userName;
}
