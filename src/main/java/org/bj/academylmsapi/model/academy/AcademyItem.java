package org.bj.academylmsapi.model.academy;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.Academy;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

import java.util.Locale;

// 학원 복수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AcademyItem {
    private Long id;
    private String academyName;
    private String businessType;
    private String representativeName;
    private String academyCallNumber;
    private String username;
    private String password;


    private AcademyItem(Builder builder) {
        this.id = builder.id;
        this.academyName = builder.academyName;
        this.businessType = builder.businessType;
        this.representativeName = builder.representativeName;
        this.academyCallNumber = builder.academyCallNumber;
        this.username = builder.username;
        this.password = builder.password;


    }

    public static class Builder implements CommonModelBuilder<AcademyItem> {
        private final Long id;
        private final String academyName;
        private final String businessType;
        private final String representativeName;
        private final String academyCallNumber;
        private final String username;
        private final String password;



        public Builder(Academy academy) {
            this.id = academy.getId();
            this.academyName = academy.getAcademyName();
            this.businessType = academy.getBusinessType();
            this.representativeName = academy.getRepresentativeName();
            this.academyCallNumber = academy.getAcademyCallNumber();
            this.username = academy.getUsername();
            this.password = academy.getPassword();
        }

        @Override
        public AcademyItem build() {
            return new AcademyItem(this);
        }
    }
}
