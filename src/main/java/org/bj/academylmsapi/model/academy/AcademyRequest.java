package org.bj.academylmsapi.model.academy;

import lombok.Getter;
import lombok.Setter;
import org.bj.academylmsapi.enums.MemberGroup;

import java.time.LocalDateTime;

// 학원 C model

@Getter
@Setter
public class AcademyRequest {
    private String academyName;
    private String address;
    private String businessType;
    private String businessContent;
    private String representativeName;
    private String academyCallNumber;
    private String username;
    private String password;
    private MemberGroup memberGroup;
}
