package org.bj.academylmsapi.model.academy;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.Academy;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

import java.time.LocalDateTime;

// 학원 단수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AcademyResponse {
    private Long id;
    private String academyName;
    private String address;
    private String businessType;
    private String businessContent;
    private String representativeName;
    private String academyCallNumber;
    private LocalDateTime dateRegister;


    private AcademyResponse(Builder builder) {
        this.id = builder.id;
        this.academyName = builder.academyName;
        this.address = builder.address;
        this.businessType = builder.businessType;
        this.businessContent = builder.businessContent;
        this.representativeName = builder.representativeName;
        this.academyCallNumber = builder.academyCallNumber;
        this.dateRegister = builder.dateRegister;

    }
    public static class Builder implements CommonModelBuilder<AcademyResponse> {
        private final Long id;
        private final String academyName;
        private final String address;
        private final String businessType;
        private final String businessContent;
        private final String representativeName;
        private final String academyCallNumber;
        private final LocalDateTime dateRegister;




        public Builder(Academy academy) {
            this.id = academy.getId();
            this.academyName = academy.getAcademyName();
            this.address = academy.getAddress();
            this.businessType = academy.getBusinessType();
            this.businessContent = academy.getBusinessContent();
            this.representativeName = academy.getRepresentativeName();
            this.academyCallNumber = academy.getAcademyCallNumber();
            this.dateRegister = academy.getDateRegister();
        }

        @Override
        public AcademyResponse build() {
            return new AcademyResponse(this);
        }
    }
}
