package org.bj.academylmsapi.model.trainingManagement.trainingRegister;

import lombok.Getter;
import lombok.Setter;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.entity.Teacher;

import java.time.LocalDateTime;

// 훈련관리 - 훈련관리 등록 C model

@Getter
@Setter
public class TrainingRegisterRequest {
    private Long studentId;
    private Long teacherId;
    private LocalDateTime dateTraining;
    private Short period;
    private Short registrationNum;
    private Short attendanceNum;
    private String absentNum;
    private String lateNum;
    private String earlyLeave;
    private String trainingSubject;
    private String mainTeacher;
    private String trainingContent;
}
