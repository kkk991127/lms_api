package org.bj.academylmsapi.model.trainingManagement.trainingAttendance;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.trainingManagement.TrainingAttendance;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

import java.time.LocalDate;
import java.time.LocalDateTime;

// 훈련일지 - 훈련일지 출결 단수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class TrainingAttendanceResponse {
    private Long id;
    private Short theory;
    private Short practice;
    private LocalDate dateAbsent;
    private String whoAbsent;
    private String whoLate;
    private String whoLeave;
    private String etc;
    private LocalDateTime dateRegister;

    private TrainingAttendanceResponse(Builder builder) {
        this.id = builder.id;
        this.theory = builder.theory;
        this.practice = builder.practice;
        this.dateAbsent = builder.dateAbsent;
        this.whoAbsent = builder.whoAbsent;
        this.whoLate = builder.whoLate;
        this.whoLeave = builder.whoLeave;
        this.etc = builder.etc;
        this.dateRegister = builder.dateRegister;
    }

    public static class Builder implements CommonModelBuilder<TrainingAttendanceResponse> {
        private final Long id;
        private final Short theory;
        private final Short practice;
        private final LocalDate dateAbsent;
        private final String whoAbsent;
        private final String whoLate;
        private final String whoLeave;
        private final String etc;
        private final LocalDateTime dateRegister;

        public Builder(TrainingAttendance trainingAttendance) {
            this.id = trainingAttendance.getId();
            this.theory = trainingAttendance.getTheory();
            this.practice = trainingAttendance.getPractice();
            this.dateAbsent = trainingAttendance.getDateAbsent();
            this.whoAbsent = trainingAttendance.getWhoAbsent();
            this.whoLate = trainingAttendance.getWhoLate();
            this.whoLeave = trainingAttendance.getWhoLeave();
            this.etc = trainingAttendance.getEtc();
            this.dateRegister = trainingAttendance.getDateRegister();
        }

        @Override
        public TrainingAttendanceResponse build() {
            return new TrainingAttendanceResponse(this);
        }
    }
}
