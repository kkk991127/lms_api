package org.bj.academylmsapi.model.trainingManagement.trainingAttendance;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

// 훈련일지 - 훈련일지 출결 C model

@Getter
@Setter
public class TrainingAttendanceRequest {
    private Short theory;
    private Short practice;
    private LocalDate dateAbsent;
    private String whoAbsent;
    private String whoLate;
    private String whoLeave;
    private String etc;
}
