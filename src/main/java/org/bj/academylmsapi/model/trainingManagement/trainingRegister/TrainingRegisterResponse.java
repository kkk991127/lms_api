package org.bj.academylmsapi.model.trainingManagement.trainingRegister;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.trainingManagement.TrainingRegister;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

import java.time.LocalDateTime;

// 훈련관리 - 훈련관리 등록 단수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class TrainingRegisterResponse {
    private Long id;
    private Long studentId;
    private Long teacherId;
    private LocalDateTime dateTraining;
    private Short period;
    private Short registrationNum;
    private Short attendanceNum;
    private String absentNum;
    private String lateNum;
    private String earlyLeave;
    private String trainingSubject;
    private String mainTeacher;
    private String trainingContent;
    private LocalDateTime dateRegister;

    private TrainingRegisterResponse(Builder builder) {
        this.id = builder.id;
        this.studentId = builder.studentId;
        this.teacherId = builder.teacherId;
        this.dateTraining = builder.dateTraining;
        this.period = builder.period;
        this.registrationNum = builder.registrationNum;
        this.attendanceNum = builder.attendanceNum;
        this.absentNum = builder.absentNum;
        this.lateNum = builder.lateNum;
        this.earlyLeave = builder.earlyLeave;
        this.trainingSubject = builder.trainingSubject;
        this.mainTeacher = builder.mainTeacher;
        this.trainingContent = builder.trainingContent;
        this.dateRegister = builder.dateRegister;
    }

    public static class Builder implements CommonModelBuilder<TrainingRegisterResponse> {
        private final Long id;
        private final Long studentId;
        private final Long teacherId;
        private final LocalDateTime dateTraining;
        private final Short period;
        private final Short registrationNum;
        private final Short attendanceNum;
        private final String absentNum;
        private final String lateNum;
        private final String earlyLeave;
        private final String trainingSubject;
        private final String mainTeacher;
        private final String trainingContent;
        private final LocalDateTime dateRegister;

        public Builder(TrainingRegister trainingRegister) {
            this.id = trainingRegister.getId();
            this.studentId = trainingRegister.getStudent().getId();
            this.teacherId = trainingRegister.getTeacher().getId();
            this.dateTraining = trainingRegister.getDateTraining();
            this.period = trainingRegister.getPeriod();
            this.registrationNum = trainingRegister.getRegistrationNum();
            this.attendanceNum = trainingRegister.getAttendanceNum();
            this.absentNum = trainingRegister.getAbsentNum();
            this.lateNum = trainingRegister.getLateNum();
            this.earlyLeave = trainingRegister.getEarlyLeave();
            this.trainingSubject = trainingRegister.getTrainingSubject();
            this.mainTeacher = trainingRegister.getMainTeacher();
            this.trainingContent = trainingRegister.getTrainingContent();
            this.dateRegister = trainingRegister.getDateRegister();
        }

        @Override
        public TrainingRegisterResponse build() {
            return new TrainingRegisterResponse(this);
        }
    }
}
