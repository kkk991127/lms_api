package org.bj.academylmsapi.model;

import lombok.Getter;
import lombok.Setter;
import org.bj.academylmsapi.enums.MemberGroup;
@Getter
@Setter
public class MemberRequest {
    private MemberGroup memberGroup;
    private String username;
    private String password;
}
