package org.bj.academylmsapi.model.studentAttendance.studentAttendanceHistory;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.studentAttendance.StudentAttendanceHistory;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

import java.time.LocalDateTime;

// 수강생 출결 내역 단수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class StudentAttendanceHistoryResponse {
    private Long id;
    private Long studentId;
    private Short studentAttendance;
    private Short studentOut;
    private Short studentLate;
    private Short studentAbsent;
    private Short studentSick;
    private LocalDateTime dateRegister;

    private StudentAttendanceHistoryResponse(Builder builder) {
        this.id = builder.id;
        this.studentId = builder.studentId;
        this.studentAttendance = builder.studentAttendance;
        this.studentOut = builder.studentOut;
        this.studentLate = builder.studentLate;
        this.studentAbsent = builder.studentAbsent;
        this.studentSick = builder.studentSick;
        this.dateRegister = builder.dateRegister;
    }

    public static class Builder implements CommonModelBuilder<StudentAttendanceHistoryResponse> {
        private final Long id;
        private final Long studentId;
        private final Short studentAttendance;
        private final Short studentOut;
        private final Short studentLate;
        private final Short studentAbsent;
        private final Short studentSick;
        private final LocalDateTime dateRegister;

        public Builder(StudentAttendanceHistory studentAttendanceHistory) {
            this.id = studentAttendanceHistory.getId();
            this.studentId = studentAttendanceHistory.getStudent().getId();
            this.studentAttendance = studentAttendanceHistory.getStudentAttendance();
            this.studentOut = studentAttendanceHistory.getStudentOut();
            this.studentLate = studentAttendanceHistory.getStudentLate();
            this.studentAbsent = studentAttendanceHistory.getStudentAbsent();
            this.studentSick = studentAttendanceHistory.getStudentSick();
            this.dateRegister = studentAttendanceHistory.getDateRegister();
        }

        @Override
        public StudentAttendanceHistoryResponse build() {
            return new StudentAttendanceHistoryResponse(this);
        }
    }
}
