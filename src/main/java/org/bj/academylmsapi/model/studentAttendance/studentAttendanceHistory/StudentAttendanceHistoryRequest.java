package org.bj.academylmsapi.model.studentAttendance.studentAttendanceHistory;


import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

// 수강생 출결 내역 C model

@Getter
@Setter
public class StudentAttendanceHistoryRequest {
    private Long studentId;
    private Short studentAttendance;
    private Short studentOut;
    private Short studentLate;
    private Short studentAbsent;
    private Short studentSick;
    private LocalDateTime dateRegister;
}
