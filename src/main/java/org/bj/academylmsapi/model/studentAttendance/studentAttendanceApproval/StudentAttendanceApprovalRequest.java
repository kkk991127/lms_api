package org.bj.academylmsapi.model.studentAttendance.studentAttendanceApproval;

import lombok.Getter;
import lombok.Setter;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.enums.ApprovalState;
import org.bj.academylmsapi.enums.AttendanceType;

import java.time.LocalDate;
import java.time.LocalDateTime;

// 수강생 출결 승인 C model

@Getter
@Setter
public class StudentAttendanceApprovalRequest {
    private Long studentId;
    private AttendanceType attendanceType;
    private String reason;
    private LocalDate datePlan;
    private ApprovalState approvalState;
    private LocalDateTime dateRegister;
}
