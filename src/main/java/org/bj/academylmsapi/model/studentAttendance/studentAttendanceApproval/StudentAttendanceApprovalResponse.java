package org.bj.academylmsapi.model.studentAttendance.studentAttendanceApproval;


import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.studentAttendance.StudentAttendanceApproval;
import org.bj.academylmsapi.enums.ApprovalState;
import org.bj.academylmsapi.enums.AttendanceType;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

import java.time.LocalDate;
import java.time.LocalDateTime;

// 수강생 출결 승인 단수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class StudentAttendanceApprovalResponse {
    private Long id;
    private Long studentId;
    private AttendanceType attendanceType;
    private String reason;
    private LocalDate datePlan;
    private ApprovalState approvalState;
    private LocalDateTime dateRegister;

    private StudentAttendanceApprovalResponse(Builder builder) {
        this.id = builder.id;
        this.studentId = builder.studentId;
        this.attendanceType = builder.attendanceType;
        this.reason = builder.reason;
        this.datePlan = builder.datePlan;
        this.approvalState = builder.approvalState;
        this.dateRegister = builder.dateRegister;
    }

    public static class Builder implements CommonModelBuilder<StudentAttendanceApprovalResponse> {
        private final Long id;
        private final Long studentId;
        private final AttendanceType attendanceType;
        private final String reason;
        private final LocalDate datePlan;
        private final ApprovalState approvalState;
        private final LocalDateTime dateRegister;

        public Builder(StudentAttendanceApproval studentAttendanceApproval) {
            this.id = studentAttendanceApproval.getId();
            this.studentId = studentAttendanceApproval.getStudent().getId();
            this.attendanceType = studentAttendanceApproval.getAttendanceType();
            this.reason = studentAttendanceApproval.getReason();
            this.datePlan = studentAttendanceApproval.getDatePlan();
            this.approvalState = studentAttendanceApproval.getApprovalState();
            this.dateRegister = studentAttendanceApproval.getDateRegister();
        }

        @Override
        public StudentAttendanceApprovalResponse build() {
            return new StudentAttendanceApprovalResponse(this);
        }
    }
}
