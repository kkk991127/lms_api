package org.bj.academylmsapi.model.studentAttendance.studentAttendanceDemand;

import lombok.Getter;
import lombok.Setter;
import org.bj.academylmsapi.enums.ApprovalState;
import org.bj.academylmsapi.enums.DemandType;

import java.time.LocalDate;
import java.time.LocalDateTime;

// 수강생 출결 신청 C model

@Getter
@Setter
public class StudentAttendanceDemandRequest {
    private Long studentId;
    private LocalDate dateThat;
    private DemandType demandType;
    private ApprovalState approvalState;
    private String addFile;
    private String etc;
    private LocalDateTime dateRegister;
}
