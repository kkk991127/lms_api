package org.bj.academylmsapi.model.studentAttendance.studentAttendanceApproval;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.studentAttendance.StudentAttendanceApproval;
import org.bj.academylmsapi.enums.ApprovalState;
import org.bj.academylmsapi.enums.AttendanceType;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

import java.time.LocalDate;
import java.time.LocalDateTime;

// 수강생 출결 승인 복수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class StudentAttendanceApprovalItem {
    private Long id;
    private Long studentId;
    private AttendanceType attendanceType;
    private LocalDate datePlan;
    private ApprovalState approvalState;

    private StudentAttendanceApprovalItem(Builder builder) {
        this.id = builder.id;
        this.studentId = builder.studentId;
        this.attendanceType = builder.attendanceType;
        this.datePlan = builder.datePlan;
        this.approvalState = builder.approvalState;
    }

    public static class Builder implements CommonModelBuilder<StudentAttendanceApprovalItem> {
        private final Long id;
        private final Long studentId;
        private final AttendanceType attendanceType;
        private final LocalDate datePlan;
        private final ApprovalState approvalState;

        public Builder(StudentAttendanceApproval studentAttendanceApproval) {
            this.id = studentAttendanceApproval.getId();
            this.studentId = studentAttendanceApproval.getStudent().getId();
            this.attendanceType = studentAttendanceApproval.getAttendanceType();
            this.datePlan = studentAttendanceApproval.getDatePlan();
            this.approvalState = studentAttendanceApproval.getApprovalState();
        }

        @Override
        public StudentAttendanceApprovalItem build() {
            return new StudentAttendanceApprovalItem(this);
        }
    }
}
