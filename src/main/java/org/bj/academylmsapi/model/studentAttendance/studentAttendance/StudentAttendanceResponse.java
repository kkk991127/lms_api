package org.bj.academylmsapi.model.studentAttendance.studentAttendance;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.studentAttendance.StudentAttendance;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

// 수강생 출결 단수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class StudentAttendanceResponse {
    private Long id;
    private Short studentAttendence;
    private Short studentAbsence;
    private Short studentOut;
    private Short studentLate;
    private Short studentSelfAbsence;

    private StudentAttendanceResponse(Builder builder) {
        this.id = builder.id;
        this.studentAttendence = builder.studentAttendence;
        this.studentAbsence = builder.studentAbsence;
        this.studentOut = builder.studentOut;
        this.studentLate = builder.studentLate;
        this.studentSelfAbsence = builder.studentSelfAbsence;
    }

    public static class Builder implements CommonModelBuilder<StudentAttendanceResponse> {
        private final Long id;
        private final Short studentAttendence;
        private final Short studentAbsence;
        private final Short studentOut;
        private final Short studentLate;
        private final Short studentSelfAbsence;

        public Builder(StudentAttendance studentAttendance) {
            this.id = studentAttendance.getId();
            this.studentAttendence = studentAttendance.getStudentAttendence();
            this.studentAbsence = studentAttendance.getStudentAbsence();
            this.studentOut = studentAttendance.getStudentOut();
            this.studentLate = studentAttendance.getStudentLate();
            this.studentSelfAbsence = studentAttendance.getStudentSelfAbsence();
        }

        @Override
        public StudentAttendanceResponse build() {
            return new StudentAttendanceResponse(this);
        }
    }
}
