package org.bj.academylmsapi.model.studentAttendance.studentAttendanceDemand;


import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.studentAttendance.StudentAttendanceDemand;
import org.bj.academylmsapi.enums.ApprovalState;
import org.bj.academylmsapi.enums.DemandType;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

import java.time.LocalDate;
import java.time.LocalDateTime;

// 수강생 출결 신청 단수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class StudentAttendanceDemandResponse {
    private Long id;
    private Long studentId;
    private LocalDate dateThat;
    private DemandType demandType;
    private ApprovalState approvalState;
    private String addFile;
    private String etc;
    private LocalDateTime dateRegister;

    private StudentAttendanceDemandResponse(Builder builder) {
        this.id = builder.id;
        this.studentId = builder.studentId;
        this.dateThat = builder.dateThat;
        this.demandType = builder.demandType;
        this.approvalState = builder.approvalState;
        this.addFile = builder.addFile;
        this.etc = builder.etc;
        this.dateRegister = builder.dateRegister;
    }

    public static class Builder implements CommonModelBuilder<StudentAttendanceDemandResponse> {
        private final Long id;
        private final Long studentId;
        private final LocalDate dateThat;
        private final DemandType demandType;
        private final ApprovalState approvalState;
        private final String addFile;
        private final String etc;
        private final LocalDateTime dateRegister;

        public Builder(StudentAttendanceDemand studentAttendanceDemand) {
            this.id = studentAttendanceDemand.getId();
            this.studentId = studentAttendanceDemand.getStudent().getId();
            this.dateThat = studentAttendanceDemand.getDateThat();
            this.demandType = studentAttendanceDemand.getDemandType();
            this.approvalState = studentAttendanceDemand.getApprovalState();
            this.addFile = studentAttendanceDemand.getAddFile();
            this.etc = studentAttendanceDemand.getEtc();
            this.dateRegister = studentAttendanceDemand.getDateRegister();
        }

        @Override
        public StudentAttendanceDemandResponse build() {
            return new StudentAttendanceDemandResponse(this);
        }
    }
}
