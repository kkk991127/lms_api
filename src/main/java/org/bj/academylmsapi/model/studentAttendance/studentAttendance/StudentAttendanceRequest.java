package org.bj.academylmsapi.model.studentAttendance.studentAttendance;

import lombok.Getter;
import lombok.Setter;
import org.bj.academylmsapi.entity.Student;

import java.time.LocalDateTime;

// 수강생 출결 C model

@Getter
@Setter
public class StudentAttendanceRequest {
    private Long studentId;
    private Short studentAttendence;
    private Short studentAbsence;
    private Short studentOut;
    private Short studentLate;
    private Short studentSelfAbsence;
}
