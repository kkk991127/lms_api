package org.bj.academylmsapi.model.studentManagement.desireWork;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.studentManagement.DesireWork;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

// 학생관리 - 취업관리 - 희망근무조건 단수 R model

@Getter
@NoArgsConstructor
public class DesireWorkResponse {
    private Long id;
    private Long studentId;
    private String hopeArea;
    private String hopeWorkField;
    private String hopePayLevel;
    private String possibleEmployForm;
    private String possibleWorkForm;
    private String possibleWorkTime;
    private String studentRequest;

    private DesireWorkResponse(Builder builder) {
        this.id = builder.id;
        this.studentId = builder.studentId;
        this.hopeArea = builder.hopeArea;
        this.hopeWorkField = builder.hopeWorkField;
        this.hopePayLevel = builder.hopePayLevel;
        this.possibleEmployForm = builder.possibleEmployForm;
        this.possibleWorkForm = builder.possibleWorkForm;
        this.possibleWorkTime = builder.possibleWorkTime;
        this.studentRequest = builder.studentRequest;
    }

    public static class Builder implements CommonModelBuilder<DesireWorkResponse> {
        private final Long id;
        private final Long studentId;
        private final String hopeArea;
        private final String hopeWorkField;
        private final String hopePayLevel;
        private final String possibleEmployForm;
        private final String possibleWorkForm;
        private final String possibleWorkTime;
        private final String studentRequest;

        public Builder(DesireWork desireWork) {
            this.id = desireWork.getId();
            this.studentId = desireWork.getStudent().getId();
            this.hopeArea = desireWork.getHopeArea();
            this.hopeWorkField = desireWork.getHopeWorkField();
            this.hopePayLevel = desireWork.getHopePayLevel();
            this.possibleEmployForm = desireWork.getPossibleEmployForm();
            this.possibleWorkForm = desireWork.getPossibleWorkForm();
            this.possibleWorkTime = desireWork.getPossibleWorkTime();
            this.studentRequest = desireWork.getStudentRequest();
        }

        @Override
        public DesireWorkResponse build() {
            return new DesireWorkResponse(this);
        }
    }
}
