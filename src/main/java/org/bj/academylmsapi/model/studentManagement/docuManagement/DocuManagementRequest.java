package org.bj.academylmsapi.model.studentManagement.docuManagement;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

// 학생관리 - 서류관리 C model

@Getter
@Setter
public class DocuManagementRequest {
    private String documentName;
    private Boolean isSign;
    private LocalDateTime dateRegister;
}
