package org.bj.academylmsapi.model.studentManagement.adviceManagement;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.studentManagement.AdviceManagement;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

import java.time.LocalDate;
import java.time.LocalDateTime;

// 학생관리 - 취업관리 - 상담관리 단수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AdviceManagementResponse {
    private Long id;
    private Long teacherId;
    private Long studentId;
    private String studentName;
    private LocalDate dateAdvice;
    private String adviceContent;
    private LocalDateTime dateRegister;
    private String etc;

    private AdviceManagementResponse(Builder builder) {
        this.id = builder.id;
        this.teacherId = builder.teacherId;
        this.studentId = builder.studentId;
        this.studentName = builder.studentName;
        this.dateAdvice = builder.dateAdvice;
        this.adviceContent = builder.adviceContent;
        this.dateRegister = builder.dateRegister;
        this.etc = builder.etc;
    }

    public static class Builder implements CommonModelBuilder<AdviceManagementResponse> {
        private final Long id;
        private final Long teacherId;
        private final Long studentId;
        private final String studentName;
        private final LocalDate dateAdvice;
        private final String adviceContent;
        private final LocalDateTime dateRegister;
        private final String etc;

        public Builder(AdviceManagement adviceManagement) {
            this.id = adviceManagement.getId();
            this.teacherId = adviceManagement.getTeacher().getId();
            this.studentId = adviceManagement.getStudent().getId();
            this.studentName = adviceManagement.getStudentName();
            this.dateAdvice = adviceManagement.getDateAdvice();
            this.adviceContent = adviceManagement.getAdviceContent();
            this.dateRegister = adviceManagement.getDateRegister();
            this.etc = adviceManagement.getEtc();
        }

        @Override
        public AdviceManagementResponse build() {
            return new AdviceManagementResponse(this);
        }
    }
}
