package org.bj.academylmsapi.model.studentManagement.desireWork;

import lombok.Getter;
import lombok.Setter;
import org.bj.academylmsapi.entity.Student;

// 학생관리 - 취업관리 - 희망근무조건 C model

@Getter
@Setter
public class DesireWorkRequest {
    private Long studentId;
    private String hopeArea;
    private String hopeWorkField;
    private String hopePayLevel;
    private String possibleEmployForm;
    private String possibleWorkForm;
    private String possibleWorkTime;
    private String studentRequest;
}
