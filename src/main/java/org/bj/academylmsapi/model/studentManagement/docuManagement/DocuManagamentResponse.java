package org.bj.academylmsapi.model.studentManagement.docuManagement;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.studentManagement.DocuManagement;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

import java.time.LocalDateTime;

// 학생관리 - 서류관리 단수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class DocuManagamentResponse {
    private Long id;
    private String documentName;
    private Boolean isSign;
    private LocalDateTime dateRegister;

    private DocuManagamentResponse(Builder builder) {
        this.id = builder.id;
        this.documentName = builder.documentName;
        this.isSign = builder.isSign;
        this.dateRegister = builder.dateRegister;
    }

    public static class Builder implements CommonModelBuilder<DocuManagamentResponse> {
        private final Long id;
        private final String documentName;
        private final Boolean isSign;
        private final LocalDateTime dateRegister;

        public Builder(DocuManagement docuManagement) {
            this.id = docuManagement.getId();
            this.documentName = docuManagement.getDocumentName();
            this.isSign = docuManagement.getIsSign();
            this.dateRegister = docuManagement.getDateRegister();
        }

        @Override
        public DocuManagamentResponse build() {
            return new DocuManagamentResponse(this);
        }
    }
}
