package org.bj.academylmsapi.model.studentManagement.workManagement;

import lombok.Getter;
import lombok.Setter;
import org.bj.academylmsapi.entity.Student;

import java.time.LocalDate;
import java.time.LocalDateTime;

// 학생관리 - 취업관리 - 취업관리사항 C model

@Getter
@Setter
public class WorkManagementRequest {
    private Long studentId;
    private String companyName;
    private String businessRegisterNumber;
    private String employmentConfirmationForm;
    private String companyAddress;
    private String companyCallNumber;
    private String workField;
    private String employmentInsuranceStatus;
    private LocalDate dateEmployment;
    private LocalDateTime dateRegister;
}
