package org.bj.academylmsapi.model.studentManagement.adviceManagement;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

// 학생관리 - 취업관리 - 상담관리 C model

@Getter
@Setter
public class AdviceManagementRequest {
    private Long teacherId;
    private Long studentId;
    private String studentName;
    private LocalDate dateAdvice;
    private String adviceContent;
    private LocalDateTime dateRegister;
    private String etc;
}
