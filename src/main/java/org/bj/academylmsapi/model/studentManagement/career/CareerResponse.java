package org.bj.academylmsapi.model.studentManagement.career;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.studentManagement.Career;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

import java.time.LocalDateTime;

// 학생관리 - 취업관리 - 경력사항 단수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CareerResponse {
    private Long id;
    private Long studentId;
    private String office;
    private String dateWork;
    private String position;
    private String whatTask;
    private String payLevel;
    private String myLicense;
    private String employSupport;
    private String belongOrganization;
    private String workNetJobSearchStatus;
    private String haveSkill;
    private LocalDateTime dateRegister;
    private String etc;

    private CareerResponse(Builder builder) {
        this.id = builder.id;
        this.studentId = builder.studentId;
        this.office = builder.office;
        this.dateWork = builder.dateWork;
        this.position = builder.position;
        this.whatTask = builder.whatTask;
        this.payLevel = builder.payLevel;
        this.myLicense = builder.myLicense;
        this.employSupport = builder.employSupport;
        this.belongOrganization = builder.belongOrganization;
        this.workNetJobSearchStatus = builder.workNetJobSearchStatus;
        this.haveSkill = builder.haveSkill;
        this.dateRegister = builder.dateRegister;
        this.etc = builder.etc;
    }

    public static class Builder implements CommonModelBuilder<CareerResponse> {
        private final Long id;
        private final Long studentId;
        private final String office;
        private final String dateWork;
        private final String position;
        private final String whatTask;
        private final String payLevel;
        private final String myLicense;
        private final String employSupport;
        private final String belongOrganization;
        private final String workNetJobSearchStatus;
        private final String haveSkill;
        private final LocalDateTime dateRegister;
        private final String etc;

        public Builder(Career career) {
            this.id = career.getId();
            this.studentId = career.getStudent().getId();
            this.office = career.getOffice();
            this.dateWork = career.getDateWork();
            this.position = career.getPosition();
            this.whatTask = career.getWhatTask();
            this.payLevel = career.getPayLevel();
            this.myLicense = career.getMyLicense();
            this.employSupport = career.getEmploySupport();
            this.belongOrganization = career.getBelongOrganization();
            this.workNetJobSearchStatus = career.getWorkNetJobSearchStatus();
            this.haveSkill = career.getHaveSkill();
            this.dateRegister = career.getDateRegister();
            this.etc = career.getEtc();
        }
        @Override
        public CareerResponse build() {
            return new CareerResponse(this);
        }
    }
}
