package org.bj.academylmsapi.model.studentManagement.adviceManagement;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.studentManagement.AdviceManagement;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

import java.time.LocalDate;


// 학생관리 - 취업관리 - 상담관리 복수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AdviceManagementItem {
    private Long id;
    private Long teacherId;
    private Long studentId;
    private String studentName;
    private LocalDate dateAdvice;

    private AdviceManagementItem(Builder builder) {
        this.id = builder.id;
        this.teacherId = builder.teacherId;
        this.studentId = builder.studentId;
        this.studentName = builder.studentName;
        this.dateAdvice = builder.dateAdvice;
    }

    public static class Builder implements CommonModelBuilder<AdviceManagementItem> {
        private final Long id;
        private final Long teacherId;
        private final Long studentId;
        private final String studentName;
        private final LocalDate dateAdvice;

        public Builder(AdviceManagement adviceManagement) {
            this.id = adviceManagement.getId();
            this.teacherId = adviceManagement.getTeacher().getId();
            this.studentId = adviceManagement.getStudent().getId();
            this.studentName = adviceManagement.getStudentName();
            this.dateAdvice = adviceManagement.getDateAdvice();
        }

        @Override
        public AdviceManagementItem build() {
            return new AdviceManagementItem(this);
        }
    }
}
