package org.bj.academylmsapi.model.studentManagement.workManagement;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.studentManagement.WorkManagement;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

import java.time.LocalDate;
import java.time.LocalDateTime;

// 학생관리 - 취업관리 - 취업관리사항 복수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class WorkManagementResponse {
    private Long id;
    private Long studentId;
    private String companyName;
    private String businessRegisterNumber;
    private String employmentConfirmationForm;
    private String companyAddress;
    private String companyCallNumber;
    private String workField;
    private String employmentInsuranceStatus;
    private LocalDate dateEmployment;
    private LocalDateTime dateRegister;

    private WorkManagementResponse(Builder builder) {
        this.id = builder.id;
        this.studentId = builder.studentId;
        this.companyName = builder.companyName;
        this.businessRegisterNumber = builder.businessRegisterNumber;
        this.employmentConfirmationForm = builder.employmentConfirmationForm;
        this.companyAddress = builder.companyAddress;
        this.companyCallNumber = builder.companyCallNumber;
        this.workField = builder.workField;
        this.employmentInsuranceStatus = builder.employmentInsuranceStatus;
        this.dateEmployment = builder.dateEmployment;
        this.dateRegister = builder.dateRegister;
    }

    public static class Builder implements CommonModelBuilder<WorkManagementResponse> {
        private final Long id;
        private final Long studentId;
        private final String companyName;
        private final String businessRegisterNumber;
        private final String employmentConfirmationForm;
        private final String companyAddress;
        private final String companyCallNumber;
        private final String workField;
        private final String employmentInsuranceStatus;
        private final LocalDate dateEmployment;
        private final LocalDateTime dateRegister;

        public Builder(WorkManagement workManagement) {
            this.id = workManagement.getId();
            this.studentId = workManagement.getStudent().getId();
            this.companyName = workManagement.getCompanyName();
            this.businessRegisterNumber = workManagement.getBusinessRegisterNumber();
            this.employmentConfirmationForm = workManagement.getEmploymentConfirmationForm();
            this.companyAddress = workManagement.getCompanyAddress();
            this.companyCallNumber = workManagement.getCompanyCallNumber();
            this.workField = workManagement.getWorkField();
            this.employmentInsuranceStatus = workManagement.getEmploymentInsuranceStatus();
            this.dateEmployment = workManagement.getDateEmployment();
            this.dateRegister = workManagement.getDateRegister();
        }
        @Override
        public WorkManagementResponse build() {
            return new WorkManagementResponse(this);
        }
    }
}
