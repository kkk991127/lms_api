package org.bj.academylmsapi.model.studentManagement.career;

import lombok.Getter;
import lombok.Setter;
import org.bj.academylmsapi.entity.Student;

import java.time.LocalDateTime;

// 학생관리 - 취업관리 - 경력사항 C model

@Getter
@Setter
public class CareerRequest {
    private Long studentId;
    private String office;
    private String dateWork;
    private String position;
    private String whatTask;
    private String payLevel;
    private String myLicense;
    private String employSupport;
    private String belongOrganization;
    private String workNetJobSearchStatus;
    private String haveSkill;
    private String etc;
}
