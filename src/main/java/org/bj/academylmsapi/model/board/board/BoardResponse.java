package org.bj.academylmsapi.model.board.board;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.board.Board;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

import java.time.LocalDateTime;

// 게시판 - 게시판 단수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BoardResponse {
    private Long id;
    private Long teacherId;
    private Long studentId;
    private String boardTitle;
    private String boardWriter;
    private String boardContent;
    private LocalDateTime dateCreate;

    private BoardResponse(Builder builder) {
        this.id = builder.id;
        this.teacherId = builder.teacherId;
        this.studentId = builder.studentId;
        this.boardTitle = builder.boardTitle;
        this.boardWriter = builder.boardWriter;
        this.boardContent = builder.boardContent;
        this.dateCreate = builder.dateCreate;
    }

    public static class Builder implements CommonModelBuilder<BoardResponse> {
        private final Long id;
        private final Long teacherId;
        private final Long studentId;
        private final String boardTitle;
        private final String boardWriter;
        private final String boardContent;
        private final LocalDateTime dateCreate;

        public Builder(Board board) {
            this.id = board.getId();
            this.teacherId = board.getTeacher().getId();
            this.studentId = board.getStudent().getId();
            this.boardTitle = board.getBoardTitle();
            this.boardWriter = board.getBoardWriter();
            this.boardContent = board.getBoardContent();
            this.dateCreate = board.getDateCreate();
        }

        @Override
        public BoardResponse build() {
            return new BoardResponse(this);
        }
    }
}
