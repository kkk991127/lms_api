package org.bj.academylmsapi.model.board.reply;

import lombok.Getter;
import lombok.Setter;

// 게시판 - 답변 C model

@Getter
@Setter
public class ReplyRequest {
    private Long boardId;
    private Long teacherId;
    private Long studentId;
    private String boardTitle;
    private String boardReTitle;
    private String boardWriter;
    private String boardContent;
}
