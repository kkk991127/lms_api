package org.bj.academylmsapi.model.board.board;

import lombok.Getter;
import lombok.Setter;

// 게시판 - 게시판 C model

@Getter
@Setter
public class BoardRequest {
    private Long teacherId;
    private Long studentId;
    private String boardTitle;
    private String boardWriter;
    private String boardContent;
}
