package org.bj.academylmsapi.model.board.reply;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.entity.Teacher;
import org.bj.academylmsapi.entity.board.Board;
import org.bj.academylmsapi.entity.board.Reply;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

import java.time.LocalDateTime;

// 게시판 - 답변 단수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ReplyResponse {
    private Long id;
    private Long boardId;
    private Long teacherId;
    private Long studentId;
    private String boardTitle;
    private String boardReTitle;
    private String boardWriter;
    private String boardContent;
    private LocalDateTime dateCreate;

    private ReplyResponse(Builder builder) {
        this.id = builder.id;
        this.boardId = builder.boardId;
        this.teacherId = builder.teacherId;
        this.studentId = builder.studentId;
        this.boardTitle = builder.boardTitle;
        this.boardReTitle = builder.boardReTitle;
        this.boardWriter = builder.boardWriter;
        this.boardContent = builder.boardContent;
        this.dateCreate = builder.dateCreate;
    }

    public static class Builder implements CommonModelBuilder<ReplyResponse> {
        private final Long id;
        private final Long boardId;
        private final Long teacherId;
        private final Long studentId;
        private final String boardTitle;
        private final String boardReTitle;
        private final String boardWriter;
        private final String boardContent;
        private final LocalDateTime dateCreate;

        public Builder(Reply reply) {
            this.id = reply.getId();
            this.boardId = reply.getBoard().getId();
            this.teacherId = reply.getTeacher().getId();
            this.studentId = reply.getStudent().getId();
            this.boardTitle = reply.getBoardTitle();
            this.boardReTitle = reply.getBoardReTitle();
            this.boardWriter = reply.getBoardWriter();
            this.boardContent = reply.getBoardContent();
            this.dateCreate = reply.getDateCreate();
        }

        @Override
        public ReplyResponse build() {
            return new ReplyResponse(this);
        }
    }
}
