package org.bj.academylmsapi.model.teacher;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bj.academylmsapi.entity.Teacher;
import org.bj.academylmsapi.enums.Gender;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

import java.time.LocalDate;
import java.time.LocalDateTime;

// 강사 복수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class TeacherItem {
    private Long id;
    private String imgSrc;
    private String teacherName;
    private Gender gender;
    private LocalDate dateBirth;

    public TeacherItem(Builder builder) {
        this.id = builder.id;
        this.imgSrc = builder.imgSrc;
        this.teacherName = builder.teacherName;
        this.gender = builder.gender;
        this.dateBirth = builder.dateBirth;
    }

    public static class Builder implements CommonModelBuilder<TeacherItem> {
        private final Long id;
        private final String imgSrc;
        private final String teacherName;
        private final Gender gender;
        private final LocalDate dateBirth;
        public Builder(Teacher teacher) {
            this.id = teacher.getId();
            this.imgSrc = teacher.getImgSrc();
            this.teacherName = teacher.getTeacherName();
            this.gender = teacher.getGender();
            this.dateBirth = teacher.getDateBirth();
        }

        @Override
        public TeacherItem build() {
            return new TeacherItem(this);
        }
    }
}
