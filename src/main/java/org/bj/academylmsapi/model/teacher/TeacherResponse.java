package org.bj.academylmsapi.model.teacher;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.Teacher;
import org.bj.academylmsapi.enums.Gender;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

import java.time.LocalDate;
import java.time.LocalDateTime;

// 강사 단수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class TeacherResponse {
    private Long id;
    private String imgSrc;
    private String teacherName;
    private Gender gender;
    private LocalDate dateBirth;
    private String phoneNumber;
    private String address;
    private String email;
    private LocalDateTime dateJoin;
    private String identity;
    private String password;

    public TeacherResponse(Builder builder) {
        this.id = builder.id;
        this.imgSrc = builder.imgSrc;
        this.teacherName = builder.teacherName;
        this.gender = builder.gender;
        this.dateBirth = builder.dateBirth;
        this.phoneNumber = builder.phoneNumber;
        this.address = builder.address;
        this.email = builder.email;
        this.dateJoin = builder.dateJoin;
        this.identity = builder.identity;
        this.password = builder.password;
    }

    public static class Builder implements CommonModelBuilder<TeacherResponse> {
        private final Long id;
        private final String imgSrc;
        private final String teacherName;
        private final Gender gender;
        private final LocalDate dateBirth;
        private final String phoneNumber;
        private final String address;
        private final String email;
        private final LocalDateTime dateJoin;
        private final String identity;
        private final String password;

        public Builder(Teacher teacher) {
            this.id = teacher.getId();
            this.imgSrc = teacher.getImgSrc();
            this.teacherName = teacher.getTeacherName();
            this.gender = teacher.getGender();
            this.dateBirth = teacher.getDateBirth();
            this.phoneNumber = teacher.getPhoneNumber();
            this.address = teacher.getAddress();
            this.email = teacher.getEmail();
            this.dateJoin = teacher.getDateJoin();
            this.identity = teacher.getIdentity();
            this.password = teacher.getPassword();
        }

        @Override
        public TeacherResponse build() {
            return new TeacherResponse(this);
        }
    }
}
