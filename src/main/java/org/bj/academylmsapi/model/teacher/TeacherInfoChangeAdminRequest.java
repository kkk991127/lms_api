package org.bj.academylmsapi.model.teacher;

import lombok.Getter;
import lombok.Setter;
import org.bj.academylmsapi.enums.Gender;

import java.time.LocalDate;
import java.time.LocalDateTime;

// 강사 U model 관리자버전

@Getter
@Setter
public class TeacherInfoChangeAdminRequest {
    private String imgSrc;
    private String teacherName;
    private Gender gender;
    private LocalDate dateBirth;
    private String phoneNumber;
    private String address;
    private String email;
    private String identity;
    private String password;
}
