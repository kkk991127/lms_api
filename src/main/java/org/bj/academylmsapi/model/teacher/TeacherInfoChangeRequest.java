package org.bj.academylmsapi.model.teacher;

import lombok.Getter;
import lombok.Setter;

// 강사 email U model

@Getter
@Setter
public class TeacherInfoChangeRequest {
    private String imgSrc;
    private String teacherName;
    private String phoneNumber;
    private String address;
    private String email;
}
