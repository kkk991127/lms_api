package org.bj.academylmsapi.model.admin;

import lombok.Getter;
import lombok.Setter;
import org.bj.academylmsapi.entity.Admin;
import org.bj.academylmsapi.enums.Gender;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

import java.time.LocalDate;
import java.time.LocalDateTime;

// 관리자 단수 R model

@Getter
@Setter
public class AdminResponse {
    private Long id;
    private String adminName;
    private Gender gender;
    private LocalDate dateBirth;
    private String phoneNumber;
    private String homeNumber;
    private String idNumber;
    private String address;
    private String imgSrc;
    private LocalDateTime dateJoin;

    private AdminResponse(Builder builder) {
        this.id = builder.id;
        this.adminName = builder.adminName;
        this.gender = builder.gender;
        this.dateBirth = builder.dateBirth;
        this.phoneNumber = builder.phoneNumber;
        this.homeNumber = builder.homeNumber;
        this.idNumber = builder.idNumber;
        this.address = builder.address;
        this.imgSrc = builder.imgSrc;
        this.dateJoin = builder.dateJoin;
    }

    public static class Builder implements CommonModelBuilder<AdminResponse> {
        private final Long id;
        private final String adminName;
        private final Gender gender;
        private final LocalDate dateBirth;
        private final String phoneNumber;
        private final String homeNumber;
        private final String idNumber;
        private final String address;
        private final String imgSrc;
        private final LocalDateTime dateJoin;

        public Builder(Admin admin) {
            this.id = admin.getId();
            this.adminName = admin.getAdminName();
            this.gender = admin.getGender();
            this.dateBirth = admin.getDateBirth();
            this.phoneNumber = admin.getPhoneNumber();
            this.homeNumber = admin.getHomeNumber();
            this.idNumber = admin.getIdNumber();
            this.address = admin.getAddress();
            this.imgSrc = admin.getImgSrc();
            this.dateJoin = admin.getDateJoin();
        }

        @Override
        public AdminResponse build() {
            return new AdminResponse(this);
        }
    }
}
