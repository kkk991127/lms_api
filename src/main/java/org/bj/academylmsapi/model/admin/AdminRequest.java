package org.bj.academylmsapi.model.admin;

import lombok.Getter;
import lombok.Setter;
import org.bj.academylmsapi.enums.Gender;

import java.time.LocalDate;
import java.time.LocalDateTime;

// 관리자 C model

@Getter
@Setter
public class AdminRequest {
    private String adminName;
    private Gender gender;
    private LocalDate dateBirth;
    private String phoneNumber;
    private String homeNumber;
    private String idNumber;
    private String address;
    private String imgSrc;
    private LocalDateTime dateJoin;
}
