package org.bj.academylmsapi.model.admin;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.Admin;
import org.bj.academylmsapi.enums.Gender;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

import java.time.LocalDate;

// 관리자 복수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AdminItem {
    private Long id;
    private String adminName;
    private Gender gender;
    private LocalDate dateBirth;
    private String imgSrc;

    private AdminItem(Builder builder) {
        this.id = builder.id;
        this.adminName = builder.adminName;
        this.gender = builder.gender;
        this.dateBirth = builder.dateBirth;
        this.imgSrc = builder.imgSrc;
    }

    public static class Builder implements CommonModelBuilder<AdminItem> {
        private final Long id;
        private final String adminName;
        private final Gender gender;
        private final LocalDate dateBirth;
        private final String imgSrc;

        public Builder(Admin admin) {
            this.id = admin.getId();
            this.adminName = admin.getAdminName();
            this.gender = admin.getGender();
            this.dateBirth = admin.getDateBirth();
            this.imgSrc = admin.getImgSrc();
        }

        @Override
        public AdminItem build() {
            return new AdminItem(this);
        }
    }
}
