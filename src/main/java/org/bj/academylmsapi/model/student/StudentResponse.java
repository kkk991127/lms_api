package org.bj.academylmsapi.model.student;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.enums.Gender;
import org.bj.academylmsapi.enums.StudentStatus;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

import java.time.LocalDate;
import java.time.LocalDateTime;

// 수강생 단수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class StudentResponse {
    private Long id;
    private String subjectChoice;
    private String imgSrc;
    private String studentName;
    private Gender gender;
    private LocalDate dateBirth;
    private String phoneNumber;
    private String homeNumber;
    private String idNumber;
    private String address;
    private String email;
    private String finalEducation;
    private String major;
    private StudentStatus studentStatus;
    private LocalDate dateStatusChange;
    private String statusWhyAndDate;
    private LocalDate dateJoin;

    private StudentResponse(Builder builder) {
        this.id = builder.id;
        this.subjectChoice = builder.subjectChoice;
        this.imgSrc = builder.imgSrc;
        this.studentName = builder.studentName;
        this.gender = builder.gender;
        this.dateBirth = builder.dateBirth;
        this.phoneNumber = builder.phoneNumber;
        this.homeNumber = builder.homeNumber;
        this.idNumber = builder.idNumber;
        this.address = builder.address;
        this.email = builder.email;
        this.finalEducation = builder.finalEducation;
        this.major = builder.major;
        this.studentStatus = builder.studentStatus;
        this.dateStatusChange = builder.dateStatusChange;
        this.statusWhyAndDate = builder.statusWhyAndDate;
        this.dateJoin = builder.dateJoin;
    }

    public static class Builder implements CommonModelBuilder<StudentResponse> {
        private final Long id;
        private final String subjectChoice;
        private final String imgSrc;
        private final String studentName;
        private final Gender gender;
        private final LocalDate dateBirth;
        private final String phoneNumber;
        private final String homeNumber;
        private final String idNumber;
        private final String address;
        private final String email;
        private final String finalEducation;
        private final String major;
        private final StudentStatus studentStatus;
        private final LocalDate dateStatusChange;
        private final String statusWhyAndDate;
        private final LocalDate dateJoin;

        public Builder(Student student) {
            this.id = student.getId();
            this.subjectChoice = student.getSubjectChoice().getSubjectName();
            this.imgSrc = student.getImgSrc();
            this.studentName = student.getStudentName();
            this.gender = student.getGender();
            this.dateBirth = student.getDateBirth();
            this.phoneNumber = student.getPhoneNumber();
            this.homeNumber = student.getHomeNumber();
            this.idNumber = student.getIdNumber();
            this.address = student.getAddress();
            this.email = student.getEmail();
            this.finalEducation = student.getFinalEducation();
            this.major = student.getMajor();
            this.studentStatus = student.getStudentStatus();
            this.dateStatusChange = student.getDateStatusChange();
            this.statusWhyAndDate = student.getStatusWhyAndDate();
            this.dateJoin = student.getDateJoin();
        }


        @Override
        public StudentResponse build() {
            return new StudentResponse(this);
        }
    }
}
