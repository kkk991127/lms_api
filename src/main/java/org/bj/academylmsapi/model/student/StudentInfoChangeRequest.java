package org.bj.academylmsapi.model.student;

import lombok.Getter;
import lombok.Setter;

// 수강생 email U model

@Getter
@Setter
public class StudentInfoChangeRequest {
    private String imgSrc;
    private String phoneNumber;
    private String homeNumber;
    private String address;
    private String email;
    private String finalEducation;
    private String major;
    private String identity;
    private String password;
}
