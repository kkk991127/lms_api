package org.bj.academylmsapi.model.student;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.enums.Gender;
import org.bj.academylmsapi.enums.StudentStatus;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;
import java.time.LocalDate;

// 수강생 복수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class StudentItem {
    private Long id;
    private String imgSrc;
    private String studentName;
    private Gender gender;
    private LocalDate dateBirth;
    private StudentStatus studentStatus;
    private LocalDate dateStatusChange;
    private String statusWhyAndDate;

    private StudentItem(Builder builder) {
        this.id = builder.id;
        this.imgSrc = builder.imgSrc;
        this.studentName = builder.studentName;
        this.gender = builder.gender;
        this.dateBirth = builder.dateBirth;
        this.studentStatus = builder.studentStatus;
        this.dateStatusChange = builder.dateStatusChange;
        this.statusWhyAndDate = builder.statusWhyAndDate;
    }

    public static class Builder implements CommonModelBuilder<StudentItem> { // 이거만 쓰고 alt + enter 하면 @over부터 나옴
        private final Long id;
        private final String imgSrc;
        private final String studentName;
        private final Gender gender;
        private final LocalDate dateBirth;
        private final StudentStatus studentStatus;
        private final LocalDate dateStatusChange;
        private final String statusWhyAndDate;

        public Builder(Student student) {
            this.id = student.getId();
            this.imgSrc = student.getImgSrc();
            this.studentName = student.getStudentName();
            this.gender = student.getGender();
            this.dateBirth = student.getDateBirth();
            this.studentStatus = student.getStudentStatus();
            this.dateStatusChange = student.getDateStatusChange();
            this.statusWhyAndDate = student.getStatusWhyAndDate();
        }

        @Override
        public StudentItem build() {
            return new StudentItem(this);
        }
    }
}
