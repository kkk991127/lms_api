package org.bj.academylmsapi.model.student;

import lombok.Getter;
import lombok.Setter;
import org.bj.academylmsapi.entity.SubjectChoice;
import org.bj.academylmsapi.enums.Gender;
import org.bj.academylmsapi.enums.StudentStatus;

import java.time.LocalDate;
import java.time.LocalDateTime;

// 수강생 C model

@Getter
@Setter
public class StudentRequest {
    private Long subjectChoiceId;
    private String imgSrc;
    private String studentName;
    private Gender gender;
    private LocalDate dateBirth;
    private String phoneNumber;
    private String homeNumber;
    private String idNumber;
    private String address;
    private String email;
    private String finalEducation;
    private String major;
    private StudentStatus studentStatus;
    private LocalDate dateStatusChange;
    private String statusWhyAndDate;
    private String identity;
    private String password;
    private LocalDateTime dateJoin;
}
