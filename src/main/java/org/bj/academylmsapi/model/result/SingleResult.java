package org.bj.academylmsapi.model.result;

import lombok.Getter;
import lombok.Setter;

// 단수 반응 model

@Getter
@Setter
public class SingleResult<T> extends CommonResult{
    private String msg;
    private Integer code;
    private T data;
}
