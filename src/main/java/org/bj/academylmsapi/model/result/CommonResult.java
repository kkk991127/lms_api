package org.bj.academylmsapi.model.result;

import lombok.Getter;
import lombok.Setter;

// 그냥 반응 model

@Getter
@Setter
public class CommonResult {
    private String msg;
    private Integer code;
    private Boolean isSuccess;

}
