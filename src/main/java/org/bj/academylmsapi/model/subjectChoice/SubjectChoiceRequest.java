package org.bj.academylmsapi.model.subjectChoice;

import lombok.Getter;
import lombok.Setter;
import org.bj.academylmsapi.enums.TrainingType;

import java.time.LocalDate;

// 과정 C model

@Getter
@Setter
public class SubjectChoiceRequest {
    // 강사id FK
    private Long teacherId;
    // 과정명
    private String subjectName;
    // 교육 시작일
    private LocalDate dateStart;
    // 교육 종료일
    private LocalDate dateEnd;
    // 훈련 기간
    private Long educationPeriod;
    // 훈련유형
    private TrainingType trainingType;
    // 회차
    private Short classTurn;
    // 재적인원
    private Short registerHuman;
    // 총 인원수
    private Short totalHuman;
    // ncs 수준
    private Short ncsLevel;
}
