package org.bj.academylmsapi.model.subjectChoice;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import org.bj.academylmsapi.entity.SubjectChoice;
import org.bj.academylmsapi.enums.TrainingType;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

// 과정 단수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class SubjectChoiceResponse {
    private Long id;
    private String teacherName;
    private String subjectName;
    private LocalDate dateStart;
    private LocalDate dateEnd;
    private Long educationPeriod;
    private TrainingType trainingType;
    private Short classTurn;
    private Short registerHuman;
    private Short totalHuman;
    private Short ncsLevel;

    private SubjectChoiceResponse(Builder builder) {
        this.id = builder.id;
        this.teacherName = builder.teacherName;
        this.subjectName = builder.subjectName;
        this.dateStart = builder.dateStart;
        this.dateEnd = builder.dateEnd;
        this.educationPeriod = builder.educationPeriod;
        this.trainingType = builder.trainingType;
        this.classTurn = builder.classTurn;
        this.registerHuman = builder.registerHuman;
        this.totalHuman = builder.totalHuman;
        this.ncsLevel = builder.ncsLevel;
    }

    public static class Builder implements CommonModelBuilder<SubjectChoiceResponse>{
        private final Long id;
        private final String teacherName;
        private final String subjectName;
        private final LocalDate dateStart;
        private final LocalDate dateEnd;
        private final Long educationPeriod;
        private final TrainingType trainingType;
        private final Short classTurn;
        private final Short registerHuman;
        private final Short totalHuman;
        private final Short ncsLevel;

        public Builder(SubjectChoice subjectChoice) {
            this.id = subjectChoice.getId();
            this.teacherName = subjectChoice.getTeacher().getTeacherName();
            this.subjectName = subjectChoice.getSubjectName();
            this.dateStart = subjectChoice.getDateStart();
            this.dateEnd = subjectChoice.getDateEnd();
            this.educationPeriod = ChronoUnit.DAYS.between(subjectChoice.getDateStart(),subjectChoice.getDateEnd());
            this.trainingType = subjectChoice.getTrainingType();
            this.classTurn = subjectChoice.getClassTurn();
            this.registerHuman = subjectChoice.getRegisterHuman();
            this.totalHuman = subjectChoice.getTotalHuman();
            this.ncsLevel = subjectChoice.getNcsLevel();
        }
        @Override
        public SubjectChoiceResponse build() {
            return new SubjectChoiceResponse(this);
        }
    }

}
