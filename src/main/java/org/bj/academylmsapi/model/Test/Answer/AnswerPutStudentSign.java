package org.bj.academylmsapi.model.Test.Answer;

import lombok.Getter;
import lombok.Setter;
import org.bj.academylmsapi.enums.TestStatus;

@Setter
@Getter
public class AnswerPutStudentSign {
    private String studentSign;
    private TestStatus testStatus;
}
