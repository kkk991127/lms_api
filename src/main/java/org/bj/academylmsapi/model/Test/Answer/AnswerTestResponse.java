package org.bj.academylmsapi.model.Test.Answer;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.testManagement.AnswerTest;
import org.bj.academylmsapi.enums.TestStatus;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AnswerTestResponse {
    private String makeTest;
    private String student;
    private String[] testAnswer;
    private String addFile;
    private TestStatus testStatus;

    private AnswerTestResponse(Builder builder){
        this.makeTest= builder.makeTest;
        this.student= builder.student;
        this.testAnswer= builder.testAnswer;
        this.addFile= builder.addFile;
        this.testStatus = builder.testStatus;
    }

    public static class Builder implements CommonModelBuilder<AnswerTestResponse>{
        private final String makeTest;
        private final String student;
        private final String[] testAnswer;
        private final String addFile;
        private final TestStatus testStatus;

        public Builder(AnswerTest answerTest){
            this.makeTest = answerTest.getMakeTest().getSubjectStatus().getTestName();
            this.student = answerTest.getStudent().getStudentName();
            this.testAnswer = answerTest.getTestAnswer().split("@");
            this.addFile = answerTest.getAddFile();
            this.testStatus = answerTest.getTestStatus();
        }

        @Override
        public AnswerTestResponse build() {
            return new AnswerTestResponse(this);
        }
    }
}
