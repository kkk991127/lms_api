package org.bj.academylmsapi.model.Test.SubjectStatus;

import lombok.Getter;
import lombok.Setter;

import org.bj.academylmsapi.enums.TestType;

import java.time.LocalDate;
import java.time.LocalDateTime;


@Getter
@Setter
public class SubjectStatusRequest {
    private Long subjectId;
    private String abilityUnitName;
    private String abilityUnitFactorName;
    private String NCS;
    private Long teacherId; // 동명 2인 일 수 있으므로
    private String testName;
    private LocalDate dateTest;
    private Short testTime;
    private TestType testType;
    private Short level;
    private Short score;
    private Short problemNum;
}
