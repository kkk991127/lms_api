package org.bj.academylmsapi.model.Test.MakeTest;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.testManagement.MakeTest;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

import java.util.LinkedList;
import java.util.List;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MakeTestResponse {
    private  String[] testProblem;
    private List<String[]> testSelect;

    private MakeTestResponse(Builder builder){
        this.testProblem = builder.testProblem;
        this.testSelect = builder.testSelect;
    }

    public static class Builder implements CommonModelBuilder<MakeTestResponse>{
        private  final String[] testProblem;
        private  final List<String[]> testSelect;

        public Builder(MakeTest makeTest){
            this.testProblem=makeTest.getTestProblem().split("@");
            String[] strings =  makeTest.getTestSelect().split("@");
            List<String[]> strings2 = new LinkedList<>();
            for (int i = 0; i < strings.length; i++) strings2.add( strings[i].split("#"));
            this.testSelect= strings2;
        }

        @Override
        public MakeTestResponse build() {
            return new  MakeTestResponse(this);
        }
    }
}
