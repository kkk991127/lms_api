package org.bj.academylmsapi.model.Test.MakeTest;


import lombok.Getter;
import lombok.Setter;


import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class MakeTestRequest {
    private Long subjectStatusId;
    private String testProblem;
    private String testSelect;
}
