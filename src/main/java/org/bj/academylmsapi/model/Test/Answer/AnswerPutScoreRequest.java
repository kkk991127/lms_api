package org.bj.academylmsapi.model.Test.Answer;

import lombok.Getter;
import lombok.Setter;
import org.bj.academylmsapi.enums.StudentScore;

@Getter
@Setter
public class AnswerPutScoreRequest {

    private String teacherComment;
    private Short studentScore;
    private Short achieveLevel;
}
