package org.bj.academylmsapi.model.Test.Answer;

import lombok.Getter;
import lombok.Setter;
import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.entity.testManagement.MakeTest;
import org.bj.academylmsapi.enums.StudentScore;
import org.bj.academylmsapi.enums.TestStatus;

@Getter
@Setter
public class AnswerTestRequest {
    private Long makeTest;
    private Long student;
    private String testAnswer;
    private TestStatus testStatus;
    private String addFile;

}
