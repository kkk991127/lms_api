package org.bj.academylmsapi.model.Test.SubjectStatus;


import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.testManagement.SubjectStatus;
import org.bj.academylmsapi.enums.TestType;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED )
public class SubjectStatusItem {
    private Long id;
    private String subjectName;
    private String abilityUnitName;
    private String abilityUnitFactorName;
    private String NCS;
    private String teacherName; // 동명 2인 일 수 있으 므로
    private String testName;
    private LocalDate dateTest;
    private Short testTime;
    private TestType testType;
    private String isSetTest; // 3항 사용을 위해 String type 으로 변환
    private Short level;
    private Short score;
    private Short problemNum;

    private SubjectStatusItem(Builder builder){
        this.id = builder.id;
        this.subjectName= builder.subjectName;
        this.abilityUnitName=  builder.abilityUnitName;
        this.abilityUnitFactorName= builder.abilityUnitFactorName;
        this.NCS= builder.NCS;
        this.teacherName= builder.teacherName;
        this.testName= builder.testName;
        this.dateTest= builder.dateTest;
        this.testTime= builder.testTime;
        this.testType= builder.testType;
        this.isSetTest= builder.isSetTest ;
        this.level= builder.level;
        this.score= builder.score;
        this.problemNum = builder.problemNum;
    }

    public static class Builder implements CommonModelBuilder<SubjectStatusItem>{
        private final Long id;
        private final String subjectName;
        private final String abilityUnitName;
        private final String abilityUnitFactorName;
        private final String NCS;
        private final String teacherName;
        private final String testName;
        private final LocalDate dateTest;
        private final Short testTime;
        private final TestType testType;
        private final String isSetTest;
        private final Short level;
        private final Short score;
        private final Short problemNum;

        public Builder(SubjectStatus subjectStatus){
            this.id = subjectStatus.getId();
            this.subjectName= subjectStatus.getSubjectId().getSubjectName();
            this.abilityUnitName= subjectStatus.getAbilityUnitName();
            this.abilityUnitFactorName= subjectStatus.getAbilityUnitFactorName();
            this.NCS= subjectStatus.getNCS();
            this.teacherName= subjectStatus.getTeacherId().getTeacherName();
            this.testName= subjectStatus.getTestName();
            this.dateTest= subjectStatus.getDateTest();
            this.testTime= subjectStatus.getTestTime();
            this.testType= subjectStatus.getTestType();
            this.isSetTest = subjectStatus.getIsSetTest() ? "출제 완료" : "미출제";
            this.level= subjectStatus.getLevel();
            this.score= subjectStatus.getScore();
            this.problemNum = subjectStatus.getProblemNum();
        }

        @Override
        public SubjectStatusItem build() {
            return new SubjectStatusItem(this);
        }
    }
}
