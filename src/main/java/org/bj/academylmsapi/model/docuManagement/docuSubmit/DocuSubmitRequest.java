package org.bj.academylmsapi.model.docuManagement.docuSubmit;

import lombok.Getter;
import lombok.Setter;

// 서류관리 - 서류제출 C model

@Getter
@Setter
public class DocuSubmitRequest {
    private Long signRegisterId;
    private Long studentId;
    private String signFile;
    private String addFile;
    private Boolean isSignClear;
}
