package org.bj.academylmsapi.model.docuManagement.signRegister;

import lombok.Getter;
import lombok.Setter;

// 서류관리 - 서류등록 C model

@Getter
@Setter
public class SignRegisterRequest {
    private Long subjectChoiceId;
    private String boardTitle;
    private String boardWriter;
    private String boardContent;
    private String addFile;
}
