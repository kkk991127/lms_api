package org.bj.academylmsapi.model.docuManagement.signRegister;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bj.academylmsapi.entity.docuManagement.SignRegister;
import org.bj.academylmsapi.interfaces.CommonModelBuilder;

import java.time.LocalDateTime;

// 서류관리 - 서류등록 단수 R model

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class SignRegisterResponse {
    private Long id;
    private Long subjectChoiceId;
    private String boardTitle;
    private String boardWriter;
    private String boardContent;
    private String addFile;
    private LocalDateTime dateCreate;

    private SignRegisterResponse(Builder builder) {
        this.id = builder.id;
        this.subjectChoiceId = builder.subjectChoiceId;
        this.boardTitle = builder.boardTitle;
        this.boardWriter = builder.boardWriter;
        this.boardContent = builder.boardContent;
        this.addFile = builder.addFile;
        this.dateCreate = builder.dateCreate;
    }

    public static class Builder implements CommonModelBuilder<SignRegisterResponse> {
        private final Long id;
        private final Long subjectChoiceId;
        private final String boardTitle;
        private final String boardWriter;
        private final String boardContent;
        private final String addFile;
        private final LocalDateTime dateCreate;

        public Builder(SignRegister signRegister) {
            this.id = signRegister.getId();
            this.subjectChoiceId = signRegister.getSubjectChoice().getId();
            this.boardTitle = signRegister.getBoardTitle();
            this.boardWriter = signRegister.getBoardWriter();
            this.boardContent = signRegister.getBoardContent();
            this.addFile = signRegister.getAddFile();
            this.dateCreate = signRegister.getDateCreate();
        }

        @Override
        public SignRegisterResponse build() {
            return new SignRegisterResponse(this);
        }
    }
}
