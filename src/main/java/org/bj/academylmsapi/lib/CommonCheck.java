package org.bj.academylmsapi.lib;

public class CommonCheck {
    /*
    * 정규식 이걸 왜 써야 할 까 ?
    * 사람들이 입력하는 아이디 , 킹왕짱명돈 , "404" 이런거 DB에 등록해주면
    * 안되니깐 그런데 이거 안된다 ~~ 하는 것 아이디는 string 이니까 if문으로
    * 처리 할 수 있나 ? 그래서 정규식 한다.*/

    public static boolean checkUsername(String username) {
        String pattern = "^[a-zA-Z]{1}[a-zA-Z0-9]{4,19}$";
        return username.matches(pattern);
    }
}
