package org.bj.academylmsapi.repository;

import org.bj.academylmsapi.entity.SubjectChoice;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

// 과정 repository

public interface SubjectChoiceRepository extends JpaRepository<SubjectChoice, Long> {
}
