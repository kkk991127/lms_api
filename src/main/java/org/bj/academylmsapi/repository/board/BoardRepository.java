package org.bj.academylmsapi.repository.board;

import org.bj.academylmsapi.entity.board.Board;
import org.springframework.data.jpa.repository.JpaRepository;

// 게시판 - 게시판 repository

public interface BoardRepository extends JpaRepository<Board, Long> {
}
