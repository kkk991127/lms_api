package org.bj.academylmsapi.repository;

import org.bj.academylmsapi.entity.Academy;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

// 학원 repository

public interface AcademyRepository extends JpaRepository<Academy, Long> {

    // 학원 복수 R 등록순 정렬
    List<Academy> findAllByOrderByIdAsc();

    Optional<Academy> findByUsername (String Username);
}
