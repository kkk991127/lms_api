package org.bj.academylmsapi.repository;

import org.bj.academylmsapi.entity.SubjectChoice;
import org.bj.academylmsapi.entity.Teacher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

// 강사 repository

public interface TeacherRepository extends JpaRepository<Teacher, Long> {
}
