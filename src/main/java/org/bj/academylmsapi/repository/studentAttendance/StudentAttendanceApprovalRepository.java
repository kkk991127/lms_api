package org.bj.academylmsapi.repository.studentAttendance;

import org.bj.academylmsapi.entity.studentAttendance.StudentAttendanceApproval;
import org.springframework.data.jpa.repository.JpaRepository;

// 수강생 출결 승인 repository

public interface StudentAttendanceApprovalRepository extends JpaRepository<StudentAttendanceApproval, Long> {
}
