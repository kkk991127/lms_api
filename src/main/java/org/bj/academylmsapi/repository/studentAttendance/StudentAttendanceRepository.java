package org.bj.academylmsapi.repository.studentAttendance;

import org.bj.academylmsapi.entity.studentAttendance.StudentAttendance;
import org.springframework.data.jpa.repository.JpaRepository;

// 수강생 출결 repository

public interface StudentAttendanceRepository extends JpaRepository<StudentAttendance, Long> {
}
