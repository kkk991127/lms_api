package org.bj.academylmsapi.repository.studentAttendance;

import org.bj.academylmsapi.entity.studentAttendance.StudentAttendanceHistory;
import org.springframework.data.jpa.repository.JpaRepository;

// 수강생 출결 내역 repository

public interface StudentAttendanceHistoryRepository extends JpaRepository<StudentAttendanceHistory, Long> {
}
