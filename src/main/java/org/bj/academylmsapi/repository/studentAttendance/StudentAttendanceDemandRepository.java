package org.bj.academylmsapi.repository.studentAttendance;

import org.bj.academylmsapi.entity.studentAttendance.StudentAttendanceDemand;
import org.springframework.data.jpa.repository.JpaRepository;

// 수강생 출결 신청 repository

public interface StudentAttendanceDemandRepository extends JpaRepository<StudentAttendanceDemand, Long> {
}
