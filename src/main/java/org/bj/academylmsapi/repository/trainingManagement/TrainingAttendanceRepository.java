package org.bj.academylmsapi.repository.trainingManagement;

import org.bj.academylmsapi.entity.trainingManagement.TrainingAttendance;
import org.springframework.data.jpa.repository.JpaRepository;

// 훈련관리 - 훈련일지 출결

public interface TrainingAttendanceRepository extends JpaRepository<TrainingAttendance, Long> {
}
