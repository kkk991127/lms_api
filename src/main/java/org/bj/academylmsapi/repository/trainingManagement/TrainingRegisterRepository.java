package org.bj.academylmsapi.repository.trainingManagement;

import org.bj.academylmsapi.entity.trainingManagement.TrainingRegister;
import org.springframework.data.jpa.repository.JpaRepository;

// 훈련관리 - 훈련일지 등록

public interface TrainingRegisterRepository extends JpaRepository<TrainingRegister, Long> {
}
