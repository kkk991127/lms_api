package org.bj.academylmsapi.repository.studentManagement;

import org.bj.academylmsapi.entity.studentManagement.AdviceManagement;
import org.springframework.data.jpa.repository.JpaRepository;

//
public interface AdviceManagementRepository extends JpaRepository<AdviceManagement, Long> {
}
