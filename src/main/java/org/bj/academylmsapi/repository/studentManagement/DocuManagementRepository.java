package org.bj.academylmsapi.repository.studentManagement;

import org.bj.academylmsapi.entity.studentManagement.DocuManagement;
import org.springframework.data.jpa.repository.JpaRepository;

// 학생관리 - 서류관리 repository

public interface DocuManagementRepository extends JpaRepository<DocuManagement, Long> {
}
