package org.bj.academylmsapi.repository.studentManagement;

import org.bj.academylmsapi.entity.studentManagement.Career;
import org.bj.academylmsapi.entity.studentManagement.DocuManagement;
import org.springframework.data.jpa.repository.JpaRepository;

// 학생관리 - 취업관리 - 경력사항 repository

public interface CareerRepository extends JpaRepository<Career, Long> {
}
