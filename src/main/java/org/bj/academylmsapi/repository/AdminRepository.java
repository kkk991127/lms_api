package org.bj.academylmsapi.repository;

import org.bj.academylmsapi.entity.Admin;
import org.bj.academylmsapi.entity.SubjectChoice;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

// 관리자 repository

public interface AdminRepository extends JpaRepository<Admin, Long> {
    // 관리자 복수 R 등록순 정렬
    List<Admin> findAllByOrderByIdAsc();
}
