package org.bj.academylmsapi.repository;

import org.bj.academylmsapi.entity.Student;
import org.bj.academylmsapi.entity.SubjectChoice;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

// 수강생 repository

public interface StudentRepository extends JpaRepository<Student, Long> {

    // 수강생 찾기
    Student findByStudentName(String name);

    // 수강생 복수 R 등록순 정렬
    List<Student> findAllByOrderByIdAsc();
}
