package org.bj.academylmsapi.repository;

import org.bj.academylmsapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MemberRepository extends JpaRepository<Member,Long> {

    Optional<Member> findByUsername (String Username);


}
