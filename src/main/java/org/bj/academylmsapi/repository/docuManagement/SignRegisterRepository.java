package org.bj.academylmsapi.repository.docuManagement;

import org.bj.academylmsapi.entity.docuManagement.SignRegister;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SignRegisterRepository extends JpaRepository<SignRegister, Long> {
}
