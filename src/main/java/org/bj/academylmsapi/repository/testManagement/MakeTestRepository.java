package org.bj.academylmsapi.repository.testManagement;

import org.bj.academylmsapi.entity.testManagement.MakeTest;
import org.springframework.data.jpa.repository.JpaRepository;

// 평가관리 - 시험출제

public interface MakeTestRepository extends JpaRepository<MakeTest, Long> {
}
