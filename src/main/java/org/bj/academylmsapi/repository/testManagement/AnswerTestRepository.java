package org.bj.academylmsapi.repository.testManagement;

import org.bj.academylmsapi.entity.testManagement.AnswerTest;
import org.springframework.data.jpa.repository.JpaRepository;

// 평가관리 - 시험채점

public interface AnswerTestRepository extends JpaRepository<AnswerTest, Long> {
}
