package org.bj.academylmsapi.configure;

// 핸들러 = 조종 이런 상황이 오면 조정해서 이렇게 해라 상황을 조정해라
// 컴포넌트 = 기능 묶음 계속 재사용 하는 것


import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import java.io.IOException;

//회사마다 보안방침이 다 다르니 , 회사에 따른 보안 방침 다 하나씩 구현 해야함.
@Component
public class CustomAccessDeniedHandler implements AccessDeniedHandler {

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        response.sendRedirect("/exception/access-denied");
    }
}
