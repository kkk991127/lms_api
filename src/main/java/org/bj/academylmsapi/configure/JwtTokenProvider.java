package org.bj.academylmsapi.configure;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import jakarta.annotation.PostConstruct;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import java.util.Base64;
import java.util.Date;

@Component
@RequiredArgsConstructor
public class JwtTokenProvider {
    private final UserDetailsService userDetailsService;
    @Value("$spring.jwt.secret")
    private String secretKey;

    //포스트 코코볼 -> 먼저 construct -> 구조  먼저 구조를 만들겠다.
    @PostConstruct
    protected void init(){
        secretKey = Base64.getEncoder().encodeToString(secretKey.getBytes()); //값을 바이트를 가져와서 인코딩 하겠다.
    } // 옛날 암호화 기법
    //토큰 생성
    //claim이라는 단어 뜻 찾아보기
    // 보안 토큰 주장 = "나에요"

    public String createToken(String username, String role, String type){
        Claims claims = Jwts.claims().setSubject(username);
        claims.put("role",role); // role = 역활
        Date now = new Date();
        // 토큰 유효시간
        // 1000 밀리세컨드 = 1초
        // 기본으로 10ㅅ기간 유효하게 설정해줌 왜냐 아침에 출근해서 로그인 하고 점심먹고 퇴근하면 대충 10시간이니깐
        // 앱용 토큰 같은 경우 유효시간 1년으로 설정해줌 앱에서 아침마다 로그인 하려고 하면 짜증나니깐
        long tokenValidMillisecond = 1000L * 60 * 60 *10;
        if (type.equals("APP")) tokenValidMillisecond = 1000L * 60 * 60 * 24 *365;
        //토큰 생성해서 리턴.
        //jwt 사이트 참고
        //유효시간도 넣어줌 .생성 요청한 시간 ~ 현재 + 위에서 설정된 유효 초 만큼.
        return Jwts.builder() // Jwt - 클래스 , 빌더의 역활 = 쌓는 것
                .setClaims(claims) // 나야
                .setIssuedAt(now) // 언제 발급 됬어 ?
                .setExpiration(new Date(now.getTime() + tokenValidMillisecond)) // 만료일 ( 언제부터 언제까지 유효한 토큰입니다 )
                .signWith(SignatureAlgorithm.HS256,secretKey) // 서명
                .compact(); // 토큰이 작으니깐 컴팩트하게 만든다.
    }

    //토큰을 분석하여 인증정보를 가져옴.
    public Authentication getAuthentication(String token){
        UserDetails userDetails = userDetailsService.loadUserByUsername(getUsername(token));
        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    } // Authentication - 일회용 출입증이라고 생각하자 어디에 접근 할 수 있고 어디소속이고 , 등등..


    // 토큰을 파싱하여 username을 가져옴.
    // 토큰 생성시 username은 subject에 넣은 것 꼭 확인 .
    // jwt 사이트 보면서 코드 이해하기.

    public String getUsername(String token) {
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().getSubject();
    }

    //리졸브(resolve)라는 단어도 많이씀 - 결정 ,해결
    public String resolveToken(HttpServletRequest request){
        //rest api - header 인증 방식에서 Bearer 를 언제 사용하는지 보기.
        return  request.getHeader(HttpHeaders.AUTHORIZATION);
    } // 성공한 토큰을 가져오겠다.


    //유효시간을 검사하여 유효시간이 지났으면 false를 줌.
    public boolean validateToken(String jwtToken){
        try {
            Jws<Claims> claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(jwtToken);
            return !claims.getBody().getExpiration().before(new Date());
        } catch (Exception e){
            return false;
        }
    }
}
