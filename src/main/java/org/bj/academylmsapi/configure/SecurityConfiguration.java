package org.bj.academylmsapi.configure;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.List;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity
@RequiredArgsConstructor
public class SecurityConfiguration {
    private final JwtTokenProvider jwtTokenProvider;
    private final CustomAuthenticationEntryPoint entryPoint;
    private final CustomAccessDeniedHandler accessDenied;
    //시작할때 중간에 등록안되니깐 처음부터 Bean으로 등록
    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration config = new CorsConfiguration();

        config.setAllowCredentials(true);
        config.setAllowedOrigins(List.of("*")); //origin
        config.setAllowedMethods(List.of("GET", "POST" ,"PUT", "DELETE", "PATCH" ,"OPTIONS"));
        // mapping , api 호출할 때 preflight = 옵션  ,
        config.setAllowedHeaders(List.of("*"));
        // 모든 헤더 요청 받아줌
        config.setExposedHeaders(List.of("*"));
        //exposed- 노출된
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**",config);
        // mapping 주소에 대해서 위에 있는 설정 들을 다 먹겠다.
        return source;
    }

    /*
     flutter 에서 header에 token 넣는법
     string? token = await TokenaLib.getToken();
     Dio dio = Dio();
     dio.options.headers['Authorization'] = 'Bearer ' + token!; < -- 한줄 추가

     vue js 에서 axios에 header 넣는법
     https://velog.io/@ch9eri/Axios-headers-Authorization
     노션에 링크 추가함 !

    * */
    @Bean // 얘한테 !
    protected SecurityFilterChain filterChain(HttpSecurity http) throws  Exception{
        http.csrf(AbstractHttpConfigurer::disable) //csrf를 비활성화 해라 어느 상황에서 csrf가 쓰일까 ?
                .formLogin(AbstractHttpConfigurer::disable)
                .httpBasic(AbstractHttpConfigurer::disable)
                .cors(corsConfig -> corsConfig.configurationSource(corsConfigurationSource())) //cors 설정
                .sessionManagement((sessionManagement) -> //위에 3개 때문에 staless가 된 것임. 연관해서 생각
                        sessionManagement.sessionCreationPolicy(SessionCreationPolicy.STATELESS)) // 세션 안쓴다.
                .authorizeHttpRequests((authorizeRequests) -> //인증 = 나라는 걸 확인 받는것  인가 = 인증 결과를 허가 하는 것
                        // 데이터 요청에 대해서 허가를 해준다.
                        authorizeRequests // 인가 ( 허가를 해줌 )

                                .requestMatchers("/v3/**", "/swagger-ui/**").permitAll() // spring boot 버전 3
                                .requestMatchers(HttpMethod.GET, "/exception/**").permitAll() // 모든걸 뚫음
                                .requestMatchers("/v1/member/login/**").permitAll() // matcher - 주소 매칭 ,
                                .requestMatchers("/v1/auth-test/test-admin").hasAnyRole("MEMBER","ADMIN") // 이 주소는  그외의 공간이 아니기 때문에

                                .anyRequest().hasRole("ADMIN")
                );

            http.exceptionHandling(handler -> handler.accessDeniedHandler(accessDenied)); // 핸들러 한테 에러를 넘김 .
            http.exceptionHandling(handler -> handler.authenticationEntryPoint(entryPoint)); // 핸들러 한테 넘김
            http.addFilterBefore(new JwtTokenFilter(jwtTokenProvider), UsernamePasswordAuthenticationFilter.class);
            //이거 수행하기전에 토큰 프로바이더한테 .class /  usernamepassword의 필터의 자리

            return http.build(); //빌드해서 넘겨줌

    }
}



