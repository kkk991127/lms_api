package org.bj.academylmsapi.configure;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import java.io.IOException;

@Component
@RequiredArgsConstructor
// 시작할 때 설정하는 값.
public class JwtTokenFilter extends GenericFilterBean {
    private final JwtTokenProvider jwtTokenProvider;
    @Override
    // 필터 단계 설정.
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        String tokenFull = jwtTokenProvider.resolveToken((HttpServletRequest) request);
        String token ="";
        if (tokenFull == null || !tokenFull.startsWith("Bearer ")){
            chain.doFilter(request,response);
            return;
        } // 받아온 resolve 값이 없거나 Bearer 띄어쓰기 값이 아니라면,
        // 필터링 할 필요도 없으니 return 을 만나면 끝나버림 .
        token = tokenFull.split(" ")[1].trim(); // trim 꺼라
        if (!jwtTokenProvider.validateToken(token)){
        chain.doFilter(request, response);
        return;
        }
        // 무사히 통과했으면 일회용 출입증을 받는다 .
        //context - 환경 holder - 고정시켜놓은
        Authentication authentication = jwtTokenProvider.getAuthentication(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(request, response);
    }
}
